# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190411182112) do

  create_table "account_connections", id: false, force: :cascade do |t|
    t.integer "account_id",           limit: 4, null: false
    t.integer "connected_account_id", limit: 4, null: false
  end

  add_index "account_connections", ["account_id", "connected_account_id"], name: "index_account_connections_on_account_id_and_connected_account_id", unique: true, using: :btree

  create_table "accounts", force: :cascade do |t|
    t.string   "name",                limit: 255
    t.string   "code",                limit: 255
    t.text     "settings",            limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.boolean  "default",                           default: false
    t.boolean  "is_comparative",                    default: false
    t.boolean  "requires_unique_ref",               default: false
  end

  create_table "authentications", force: :cascade do |t|
    t.integer  "account_id",  limit: 4,   null: false
    t.integer  "customer_id", limit: 4,   null: false
    t.string   "provider",    limit: 255, null: false
    t.string   "uid",         limit: 255, null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "authentications", ["account_id", "customer_id", "provider", "uid"], name: "index_authentications_account_customer_uid_uniq", unique: true, using: :btree
  add_index "authentications", ["account_id"], name: "index_authentications_on_account_id", using: :btree
  add_index "authentications", ["customer_id"], name: "index_authentications_on_customer_id", using: :btree

  create_table "candidate_question_answers", force: :cascade do |t|
    t.integer  "candidate_id", limit: 4
    t.integer  "question_id",  limit: 4
    t.integer  "answer",       limit: 4
    t.integer  "account_id",   limit: 4
    t.string   "source",       limit: 255
    t.text     "source_desc",  limit: 65535
    t.text     "source_link",  limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "candidate_question_answers", ["account_id"], name: "index_candidate_question_answers_on_account_id", using: :btree
  add_index "candidate_question_answers", ["candidate_id"], name: "index_candidate_question_answers_on_candidate_id", using: :btree
  add_index "candidate_question_answers", ["question_id"], name: "index_candidate_question_answers_on_question_id", using: :btree

  create_table "candidates", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "image",       limit: 255
    t.text     "description", limit: 65535
    t.text     "params",      limit: 65535
    t.text     "answers",     limit: 65535
    t.decimal  "coherence",                 precision: 2, scale: 2
    t.string   "code",        limit: 255
    t.integer  "account_id",  limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.string   "party",       limit: 255
    t.integer  "state_id",    limit: 4
    t.integer  "district_id", limit: 4
  end

  add_index "candidates", ["account_id"], name: "index_candidates_on_account_id", using: :btree
  add_index "candidates", ["deleted_at"], name: "index_candidates_on_deleted_at", using: :btree
  add_index "candidates", ["district_id"], name: "index_candidates_on_district_id", using: :btree
  add_index "candidates", ["name"], name: "index_candidates_on_name", using: :btree
  add_index "candidates", ["state_id"], name: "index_candidates_on_state_id", using: :btree

  create_table "customers", force: :cascade do |t|
    t.integer  "account_id",             limit: 4,                   null: false
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "full_name",              limit: 255
    t.string   "image",                  limit: 255
    t.string   "zip",                    limit: 255
    t.string   "gender",                 limit: 255
    t.string   "year_of_birth",          limit: 255
    t.string   "ethnicity",              limit: 255
    t.string   "education",              limit: 255
    t.boolean  "private",                            default: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  add_index "customers", ["account_id", "email", "deleted_at"], name: "index_customers_account_email", unique: true, using: :btree
  add_index "customers", ["account_id"], name: "index_customers_on_account_id", using: :btree
  add_index "customers", ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true, using: :btree

  create_table "districts", force: :cascade do |t|
    t.integer  "account_id", limit: 4,   null: false
    t.integer  "state_id",   limit: 4,   null: false
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "districts", ["account_id"], name: "index_districts_on_account_id", using: :btree
  add_index "districts", ["code"], name: "index_districts_on_code", using: :btree
  add_index "districts", ["deleted_at"], name: "index_districts_on_deleted_at", using: :btree
  add_index "districts", ["state_id"], name: "index_districts_on_state_id", using: :btree

  create_table "extra_questions", force: :cascade do |t|
    t.integer  "account_id", limit: 4
    t.string   "name",       limit: 255
    t.integer  "position",   limit: 4
    t.text     "options",    limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "extra_questions", ["account_id"], name: "index_extra_questions_on_account_id", using: :btree
  add_index "extra_questions", ["deleted_at"], name: "index_extra_questions_on_deleted_at", using: :btree

  create_table "invites", force: :cascade do |t|
    t.integer  "account_id", limit: 4
    t.string   "token",      limit: 36, null: false
    t.datetime "deleted_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "languages", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.string   "code",             limit: 255
    t.integer  "account_id",       limit: 4,   null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "default_language"
  end

  add_index "languages", ["account_id"], name: "index_languages_on_account_id", using: :btree
  add_index "languages", ["code"], name: "index_languages_on_code", using: :btree
  add_index "languages", ["deleted_at"], name: "index_languages_on_deleted_at", using: :btree

  create_table "pages", force: :cascade do |t|
    t.integer  "account_id",  limit: 4
    t.integer  "language_id", limit: 4
    t.string   "kind",        limit: 255,      default: "page", null: false
    t.string   "name",        limit: 255,                       null: false
    t.string   "title",       limit: 255
    t.string   "description", limit: 255
    t.string   "path",        limit: 255,                       null: false
    t.text     "content",     limit: 16777215
    t.integer  "position",    limit: 4
    t.boolean  "hidden",                       default: false
    t.datetime "deleted_at"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  add_index "pages", ["account_id", "path", "deleted_at"], name: "index_pages_on_account_id_and_path_and_deleted_at", unique: true, using: :btree
  add_index "pages", ["account_id"], name: "index_pages_on_account_id", using: :btree
  add_index "pages", ["language_id"], name: "index_pages_on_language_id", using: :btree

  create_table "parameters", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "account_id", limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "parameters", ["account_id"], name: "index_parameters_on_account_id", using: :btree
  add_index "parameters", ["deleted_at"], name: "index_parameters_on_deleted_at", using: :btree
  add_index "parameters", ["name"], name: "index_parameters_on_name", using: :btree

  create_table "questions", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "description", limit: 255
    t.integer  "position",    limit: 4
    t.string   "params",      limit: 255
    t.integer  "eid",         limit: 4
    t.integer  "account_id",  limit: 4
    t.datetime "deleted_at"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "questions", ["account_id"], name: "index_questions_on_account_id", using: :btree
  add_index "questions", ["deleted_at"], name: "index_questions_on_deleted_at", using: :btree
  add_index "questions", ["name"], name: "index_questions_on_name", using: :btree

  create_table "states", force: :cascade do |t|
    t.integer  "account_id", limit: 4,   null: false
    t.string   "code",       limit: 255
    t.string   "name",       limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "states", ["account_id"], name: "index_states_on_account_id", using: :btree
  add_index "states", ["code"], name: "index_states_on_code", using: :btree
  add_index "states", ["deleted_at"], name: "index_states_on_deleted_at", using: :btree

  create_table "submissions", force: :cascade do |t|
    t.integer  "account_id",             limit: 4,     null: false
    t.string   "client_hash",            limit: 255,   null: false
    t.string   "language_code",          limit: 255
    t.string   "publisher",              limit: 255
    t.text     "answers",                limit: 65535
    t.text     "extra_questions",        limit: 65535
    t.string   "remote_ip",              limit: 255
    t.text     "user_agent",             limit: 65535
    t.integer  "seconds_to_result",      limit: 4
    t.integer  "start_to_result",        limit: 4
    t.datetime "open_page_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "full_name",              limit: 255
    t.string   "email",                  limit: 255
    t.string   "zip",                    limit: 255
    t.string   "facebook",               limit: 255
    t.string   "twitter",                limit: 255
    t.datetime "left_page_at"
    t.string   "gender",                 limit: 255
    t.string   "year_of_birth",          limit: 255
    t.string   "ethnicity",              limit: 255
    t.string   "education",              limit: 255
    t.string   "phone",                  limit: 255
    t.text     "location_data",          limit: 65535
    t.integer  "state_id",               limit: 4
    t.integer  "district_id",            limit: 4
    t.integer  "customer_id",            limit: 4
    t.string   "title",                  limit: 255
    t.string   "external_ref",           limit: 255
    t.integer  "comparision_account_id", limit: 4
  end

  add_index "submissions", ["account_id"], name: "index_submissions_on_account_id", using: :btree
  add_index "submissions", ["client_hash"], name: "index_submissions_on_client_hash", using: :btree
  add_index "submissions", ["comparision_account_id"], name: "fk_rails_2b49de6120", using: :btree
  add_index "submissions", ["customer_id"], name: "index_submissions_on_customer_id", using: :btree
  add_index "submissions", ["district_id"], name: "index_submissions_on_district_id", using: :btree
  add_index "submissions", ["external_ref"], name: "index_submissions_on_external_ref", using: :btree
  add_index "submissions", ["state_id"], name: "index_submissions_on_state_id", using: :btree

  create_table "translations", force: :cascade do |t|
    t.integer  "language_id", limit: 4,     null: false
    t.integer  "account_id",  limit: 4,     null: false
    t.text     "content",     limit: 65535, null: false
    t.integer  "model_id",    limit: 4,     null: false
    t.string   "model_type",  limit: 255,   null: false
    t.string   "model_field", limit: 255,   null: false
    t.datetime "deleted_at"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "translations", ["account_id"], name: "index_translations_on_account_id", using: :btree
  add_index "translations", ["deleted_at"], name: "index_translations_on_deleted_at", using: :btree
  add_index "translations", ["language_id", "model_id", "model_type", "model_field"], name: "idx_language_model", using: :btree
  add_index "translations", ["language_id"], name: "index_translations_on_language_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "deleted_at"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email", "deleted_at"], name: "index_users_on_email_and_deleted_at", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "authentications", "accounts", name: "authentications_account_id_fk"
  add_foreign_key "authentications", "customers", name: "authentications_customer_id_fk"
  add_foreign_key "candidate_question_answers", "accounts", name: "candidate_question_answers_account_id_fk"
  add_foreign_key "candidate_question_answers", "candidates", name: "candidate_question_answers_candidate_id_fk"
  add_foreign_key "candidate_question_answers", "questions", name: "candidate_question_answers_question_id_fk"
  add_foreign_key "candidates", "accounts", name: "candidates_account_id_fk"
  add_foreign_key "candidates", "districts", name: "candidates_district_id_fk"
  add_foreign_key "candidates", "states", name: "candidates_state_id_fk"
  add_foreign_key "customers", "accounts", name: "customers_account_id_fk"
  add_foreign_key "districts", "accounts", name: "districts_account_id_fk"
  add_foreign_key "districts", "states", name: "districts_state_id_fk"
  add_foreign_key "extra_questions", "accounts", name: "extra_questions_account_id_fk"
  add_foreign_key "languages", "accounts", name: "languages_account_id_fk"
  add_foreign_key "pages", "accounts", name: "pages_account_id_fk"
  add_foreign_key "pages", "languages", name: "pages_language_id_fk"
  add_foreign_key "parameters", "accounts", name: "parameters_account_id_fk"
  add_foreign_key "questions", "accounts", name: "questions_account_id_fk"
  add_foreign_key "states", "accounts", name: "states_account_id_fk"
  add_foreign_key "submissions", "accounts", column: "comparision_account_id"
  add_foreign_key "submissions", "accounts", name: "submissions_account_id_fk"
  add_foreign_key "submissions", "customers", name: "submissions_customer_id_fk"
  add_foreign_key "submissions", "districts", name: "submissions_district_id_fk"
  add_foreign_key "submissions", "states", name: "submissions_state_id_fk"
  add_foreign_key "translations", "accounts", name: "translations_account_id_fk"
  add_foreign_key "translations", "languages", name: "translations_language_id_fk"
end
