# In order to execute the script in production
# SSH to remote server
# cd to Rails working directory, e.g. /var/rails/societly/current
# run the script via [rails runner](https://guides.rubyonrails.org/command_line.html#rails-runner)
#
# RAILS_ENV=production ./bin/rails runner db/scripts/export_candidates_by_countries.rb
#
# Note that the file is generated into the same working directory. You can and should move it away from there.

master_code = 'survey'

master_account = Account.find_by(code: master_code)
accounts = master_account.accounts

CSV.open("candidates_#{master_account.name.downcase}-#{Date.today.to_s(:db)}.csv", 'wb') do |csv|
  accounts.each do |account|
    headers = %w[Account Candidate Party]
    questions = account.questions.order(:position)
    headers |= questions.map(&:name)

    csv << headers
    account.candidates.includes(:candidate_question_answers).each do |candidate|
      row = [account.name, candidate.name, candidate.party]
      questions.each do |question|
        answer = candidate.candidate_question_answers.find {|a| a.question_id == question.id }
        row << answer.try(:answer)
      end
      csv << row
    end
    csv << Array.new(questions.size + 3)
  end
end
