require 'CSV'

master_code = 'survey'
invitations_for_account = 2_000

host = 'http://localhost:3000/'

master = Account.find_by(code: master_code)

accounts = master.accounts

accounts.each do |account|
  lang = account.languages.order(:default_language).last
  base_url = "#{host}/#{master_code}/#{account.code}/default/#{lang.code}"
  CSV.open("invitations_#{account.name.downcase}.csv", "wb") do |csv|
    csv << ["url"]

    invitations_for_account.times do
      token = Invite.create(account: account).token
      csv << ["#{base_url}/?cref=#{token}"]
    end
  end
end
