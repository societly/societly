# Requierments for the script: https://jira.lab.mobi/browse/SOC-99
#
## The client has created a "euandi2019_master" account, that contains all the statements and base translations in english (account settings)
## We need to duplicate this master account into the 29 country list:
## All EU countries
## UK
## Belgium needs to have 2 account "Flemish" and "Wallonia"

countries = {
  'AT' => { name: 'Austria' },
  'flemish' => { name: 'Flemish' },
  'wallonia' => { name: 'Wallonia' },
  'BG' => { name: 'Bulgaria' },
  'HR' => { name: 'Croatia' },
  'CY' => { name: 'Cyprus' },
  'CZ' => { name: 'Czechia' },
  'DK' => { name: 'Denmark' },
  'EE' => { name: 'Estonia' },
  'FI' => { name: 'Finland' },
  'FR' => { name: 'France' },
  'DE' => { name: 'Germany' },
  'GR' => { name: 'Greece' },
  'HU' => { name: 'Hungary' },
  'IE' => { name: 'Ireland' },
  'IT' => { name: 'Italy' },
  'LV' => { name: 'Latvia' },
  'LT' => { name: 'Lithuania' },
  'LU' => { name: 'Luxembourg' },
  'MT' => { name: 'Malta' },
  'NL' => { name: 'Netherlands' },
  'PL' => { name: 'Poland' },
  'PT' => { name: 'Portugal' },
  'RO' => { name: 'Romania' },
  'SK' => { name: 'Slovakia' },
  'SI' => { name: 'Slovenia' },
  'SP' => { name: 'Spain' },
  'SE' => { name: 'Sweden' },
  'GB' => { name: 'United Kingdom' }
}

master = Account.find_by(code: 'euandi2019')
languages = master.languages

altered_accounts = []

ActiveRecord::Base.transaction do
  countries.each do |key, data|
    code = key
    name = data[:name]
    params_map = HashWithIndifferentAccess.new

    existing_account = Account.find_by(code: code)

    if existing_account.present?
      altered_accounts << existing_account.code
      existing_account.parameters.destroy_all
      existing_account.questions.destroy_all
      existing_account.pages.destroy_all
      existing_account.languages.destroy_all
      existing_account.translation_keys.destroy_all
    end

    account = Account.where(code: code).first_or_create
    account.name = name
    account.save!

    # languages
    languages.each do |lang|
      Language.where(account: account, code: lang.code).first_or_create do |l|
        l.name = lang.name
      end
    end
    account.reload
    language = account.languages.first

    # parameters
    master.parameters.each do |param|
      p = account.parameters.build
      p.attributes = { name: { 'EN' => param.name } }
      p.save!
      params_map[param.id] = p.id
    end
    account.reload

    # questions
    master.questions.each do |question|
      q = account.questions.build
      q.attributes = {
        name: { 'EN' => question.name },
        description: { 'EN' => question.description },
        position: question.position
      }
      q.params = question.params.each_with_object({}) do |(k, v), memo|
        memo[params_map[k.to_i] || k] = v
        memo
      end
      q.save!
    end

    # pages
    master.pages.each do |page|
      p = account.pages.build
      p.attributes = {
        language: language,
        kind: page.kind,
        name: page.name,
        title: page.title,
        description: page.description,
        path: page.path,
        content: page.content,
        hidden: page.hidden
      }
      p.save!
    end

    # translation_keys
    master.translation_keys.each do |tr_key|
      tk = account.translation_keys.build
      tk.account = account
      tk.language_id = language.id
      tk.content = tr_key.content
      tk.model_field = tr_key.model_field
      tk.save!
    end
  end
end

puts "Updated following accounts: #{altered_accounts.join(', ')}"
