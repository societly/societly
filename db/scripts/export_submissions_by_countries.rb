# In order to execute the script in production
# SSH to remote server
# cd to Rails working directory, e.g. /var/rails/societly/current
# run the script via [rails runner](https://guides.rubyonrails.org/command_line.html#rails-runner)
#
# RAILS_ENV=production ./bin/rails runner db/scripts/export_submissions_by_countries.rb
#
# Note that the files are generated into the Rails /tmp. You can and should move those away from there.

# require 'CSV'
require 'fileutils'

include Admin::SubmissionsHelper

def lt(time)
  time.strftime('%Y-%m-%d %H:%M') if time
end

headers_template = [
  'id',
  'Publisher',
  'Language',
  'Submitted',
  'External ref',
  'Start to result',
  'Seconds to result',
  'Open page at',
  'Left page at',
  'Name',
  'Gender',
  'Year of birth',
  'Race/ethnicity',
  'ZIP code',
  'State',
  'District',
  'Education',
  'Email',
  'Cell phone',
  'Facebook',
  'Twitter',
  'Customer id',
  'Title',
]

master_code = 'survey'

master_account = Account.find_by(code: master_code)
accounts = master_account.accounts

collection = "societly-submissions-#{Time.now.strftime("%Y%m%d%H%M")}"
directory = Rails.root.join('tmp', collection)
FileUtils.mkdir_p directory

accounts.each do |account|
  header = headers_template.clone

  submissions = account.submissions.includes(:state, :district).order('id DESC')
  questions = account.questions.sorted
  extra_questions = account.extra_questions.sorted
  candidates = account.candidates.by_level('country').includes(:candidate_question_answers).order(:name)
  # language = account.languages.default_language.first
  filename = File.join(directory, "societly-submissions-#{account.name.downcase}-#{Time.now.strftime("%Y%m%d%H%M")}.csv")

  questions.each do |q|
    header << "S ID #{q.id} - answer"
    header << "S ID #{q.id} - balance"
  end

  extra_questions.each do |q|
    header << "EQ ID #{q.id}"
  end

  candidates.each do |c|
    header << c.name
  end

  CSV.open(filename, 'wb') do |csv|
    csv << header
    # data rows
    submissions.find_each do |submission|
      row = [
        submission.id,
        submission.publisher.try(:capitalize),
        submission.language_code,
        lt(submission.created_at),
        submission.external_ref
      ]

      row += [
        submission.start_to_result,
        submission.seconds_to_result,
        lt(submission.open_page_at),
        lt(submission.left_page_at),
        submission.full_name,
        submission.gender,
        submission.year_of_birth,
        submission.ethnicity,
        submission.zip,
        submission.state.try(:name),
        submission.district.try(:name),
        submission.education,
        submission.email,
        submission.phone,
        submission.facebook,
        submission.twitter,
        submission.customer_id,
        submission.db_title
      ]

      answers = submission.answers.each_with_object({}) {|answer, h| h[answer['question_id']] = answer }

      questions.each do |q|
        if answers[q.id]
          row << answers[q.id]['answer']
          row << answers[q.id]['balance']
        else
          row += Array.new(2)
        end
      end

      extra_questions.each do |q|
        answer = (submission.extra_questions || {})[q.id.to_s]
        if answer.present?
          row << (q.option_value_for(answer, @language).presence || answer)
        else
          row << ''
        end
      end

      candidates.each do |c|
        row << candidate_distance(c, answers)
      end

      csv << row
    end
  end
  puts "Generated #{filename} for #{account.name}"
end
