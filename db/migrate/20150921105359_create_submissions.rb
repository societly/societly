class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.integer :account_id, null: false
      t.string :client_hash, null: false
      t.string :language_code, length: 5
      t.string :publisher
      t.text :answers
      t.text :party_preferences
      t.string :remote_ip
      t.text :user_agent
      t.integer :seconds_to_result
      t.integer :start_to_result
      t.datetime :open_page_at

      t.timestamps null: false
      
      t.index :account_id
      t.index :client_hash
    end
  end
end
