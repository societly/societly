class AddLocationDataToSubmission < ActiveRecord::Migration
  def change
    add_column :submissions, :location_data, :text
  end
end
