class CreateStates < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.references :account, index: true, null: false
      t.string :code, index: true
      t.string :name

      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end
  end
end
