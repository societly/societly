class AddUserFieldsToSubmission < ActiveRecord::Migration
  def change
    add_column :submissions, :full_name, :string
    add_column :submissions, :email, :string
    add_column :submissions, :zip, :string
    add_column :submissions, :facebook, :string
    add_column :submissions, :twitter, :string
  end
end
