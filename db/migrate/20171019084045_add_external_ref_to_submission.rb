class AddExternalRefToSubmission < ActiveRecord::Migration
  def change
    add_column :submissions, :external_ref, :string
  end
end
