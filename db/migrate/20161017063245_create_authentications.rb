class CreateAuthentications < ActiveRecord::Migration
  def change
    create_table :authentications do |t|
      t.references :account, index: true, null: false
      t.references :customer, index: true, null: false
      t.string :provider, null: false
      t.string :uid, null: false

      t.timestamps null: false
    end

    add_index :authentications, [:account_id, :customer_id, :provider, :uid], unique: true, name: 'index_authentications_account_customer_uid_uniq'
  end
end
