class AddIsComparativeToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :is_comparative, :boolean, default: false
  end
end
