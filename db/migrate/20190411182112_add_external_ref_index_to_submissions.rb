class AddExternalRefIndexToSubmissions < ActiveRecord::Migration
  def change
    add_index :submissions, :external_ref
  end
end
