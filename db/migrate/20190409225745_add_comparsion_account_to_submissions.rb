class AddComparsionAccountToSubmissions < ActiveRecord::Migration
  def change
    add_column :submissions, :comparision_account_id, :integer, default: nil
    add_foreign_key :submissions, :accounts, column: :comparision_account_id
  end
end
