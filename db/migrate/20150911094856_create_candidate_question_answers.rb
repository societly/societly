class CreateCandidateQuestionAnswers < ActiveRecord::Migration
  def change
    create_table :candidate_question_answers do |t|
      t.integer :candidate_id
      t.integer :question_id
      t.integer :answer
      t.integer :account_id
      t.string :source
      t.text :source_desc
      t.text :source_link
      t.datetime :deleted_at

      t.timestamps null: false
      
      t.index :account_id
      t.index :candidate_id
      t.index :question_id
    end
  end
end
