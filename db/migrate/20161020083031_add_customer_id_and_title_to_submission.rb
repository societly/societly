class AddCustomerIdAndTitleToSubmission < ActiveRecord::Migration
  def change
    add_reference :submissions, :customer, index: true
    add_column :submissions, :title, :string
  end
end
