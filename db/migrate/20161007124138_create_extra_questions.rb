class CreateExtraQuestions < ActiveRecord::Migration
  def change
    create_table :extra_questions do |t|
      t.references :account, index: true
      t.string :name
      t.integer :position
      t.text :options

      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end
  end
end
