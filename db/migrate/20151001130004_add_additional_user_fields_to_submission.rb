class AddAdditionalUserFieldsToSubmission < ActiveRecord::Migration
  def change
    add_column :submissions, :gender, :string
    add_column :submissions, :year_of_birth, :string
    add_column :submissions, :ethnicity, :string
    add_column :submissions, :education, :string
    add_column :submissions, :phone, :string
  end
end
