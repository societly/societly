class AddLeftPageAtToSubmission < ActiveRecord::Migration
  def change
    add_column :submissions, :left_page_at, :datetime
  end
end
