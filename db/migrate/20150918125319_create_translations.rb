class CreateTranslations < ActiveRecord::Migration
  def change
    create_table :translations do |t|
      t.integer :language_id, null: false
      t.integer :account_id, null: false
      t.text :content, null: false
      t.integer :model_id, null: false
      t.string :model_type, null: false
      t.string :model_field, null: false

      t.datetime :deleted_at
      t.timestamps null: false
      
      t.index [:language_id, :model_id, :model_type, :model_field], name: :idx_language_model
      t.index :language_id
      t.index :account_id
      t.index :deleted_at
    end
  end
end
