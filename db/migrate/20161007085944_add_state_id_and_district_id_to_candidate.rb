class AddStateIdAndDistrictIdToCandidate < ActiveRecord::Migration
  def change
    add_reference :candidates, :state, index: true
    add_reference :candidates, :district, index: true
  end
end
