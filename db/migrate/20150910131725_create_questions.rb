class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :name
      t.string :description
      t.integer :position
      t.string :params
      t.integer :eid

      t.integer :account_id
      t.datetime :deleted_at
      t.timestamps null: false
      
      t.index(:name)
      t.index(:account_id)
      t.index(:deleted_at)
    end
  end
end
