class CreateLanguages < ActiveRecord::Migration
  def change
    create_table :languages do |t|
      t.string :name
      t.string :code, length: 5
      t.integer :account_id, null: false
      t.datetime :deleted_at

      t.timestamps null: false
      
      t.index :code
      t.index :account_id
      t.index :deleted_at
    end
  end
end
