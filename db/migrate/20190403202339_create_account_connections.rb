class CreateAccountConnections < ActiveRecord::Migration
  def change
    create_table 'account_connections', force: true, id: false do |t|
      t.integer 'account_id', null: false
      t.integer 'connected_account_id', null: false
    end
    add_index :account_connections, [:account_id, :connected_account_id], unique: true
  end
end
