class CreateParameters < ActiveRecord::Migration
  def change
    create_table :parameters do |t|
      t.string :name
      t.integer :account_id
      t.datetime :deleted_at

      t.timestamps null: false
      
      t.index(:name)
      t.index(:account_id)
      t.index(:deleted_at)
    end
  end
end
