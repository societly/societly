class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.references :account
      t.string :token, limit: 36, null: false, unique: true
      t.datetime :deleted_at
      t.timestamps null: false
    end
  end
end
