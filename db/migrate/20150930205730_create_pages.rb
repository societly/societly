class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.references :account, index: true
      t.references :language, index: true
      t.string :kind, null: false, default: 'page'
      t.string :name, null: false
      t.string :title
      t.string :description
      t.string :path, null: false
      t.text :content, limit: 2.megabytes
      t.integer :position
      t.boolean :hidden, default: false
      t.datetime :deleted_at

      t.timestamps null: false
    end

    add_index :pages, [:account_id, :path, :deleted_at], unique: true
  end
end
