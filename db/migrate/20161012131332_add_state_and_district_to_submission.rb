class AddStateAndDistrictToSubmission < ActiveRecord::Migration
  def change
    add_column :submissions, :state_id, :integer
    add_column :submissions, :district_id, :integer
    add_index :submissions, :state_id
    add_index :submissions, :district_id
  end
end
