class RenamePartyPreferencesToExtraQuestionForSubmission < ActiveRecord::Migration
  def change
    rename_column(:submissions, :party_preferences, :extra_questions)
  end
end
