class AddUniqueRefRequirementToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :requires_unique_ref, :boolean, default: false
  end
end
