class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
      t.string :name
      t.string :image
      t.text :description
      t.text :params
      t.text :answers
      t.decimal :coherence, precision: 2, scale: 2
      t.string :code

      t.integer :account_id
      t.datetime :deleted_at
      t.timestamps null: false
      
      t.index(:name)
      t.index(:account_id)
      t.index(:deleted_at)
    end
  end
end
