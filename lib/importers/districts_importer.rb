# Import States data for accounts
#
# Usage:
#  file = Rails.root.join('tmp', 'national_cd113.csv')
#  i = DistrictsImporter.new(account_id: 1, file: file)
#  i.import!
#
# Source for US 113th Congressional District Codes: https://www.census.gov/geo/reference/codes/cd113.html (should be converted to tab separated CSV)
#
# Expected format is TAB separated CSV file with 4 columns:
#   STATE STATEFP CD113FP NAMELSAD
#   AL  01  1 Congressional District 1
#
require 'csv'

class DistrictsImporter

  # Options:
  #
  #   :account_id - target account (required)
  #   :file - target CSV file full path (required)
  #   :csv_options - optional has for CSV importer
  #     :col_sep - (default "\t")
  #     :headers - (default true)
  def initialize(options = {})
    @account = Account.find(options[:account_id])
    @language_code = @account.languages.default_language.first.try(:code)
    @file = options[:file]
    @total = 0
    @imported = 0
    @csv_options = {col_sep: "\t", headers: true}.merge(options.fetch(:csv_options, {}))
    raise "File '#{@file}' not found!" unless File.exists?(@file)
  end

  def import!
    states = @account.states.to_a

    CSV.foreach(@file, @csv_options) do |row|
      district_code = row[2].to_s.strip.downcase
      # Skip not defined districts
      next if district_code == 'zz' || district_code.blank?

      @total += 1

      state_code = row[0].to_s.strip
      state = states.detect { |e| e.code == state_code }

      if state
        # Rewrite code for delegate districts
        district_code = district_code == '98' ? 0 : district_code.to_i
        code = "#{state_code}-#{'%.2d' % district_code }"

        district = state.districts.find_or_initialize_by(code: code)
        if district.new_record?
          district.account = @account
          district.name = {@language_code => row[3].to_s.strip}
          if district.save
            @imported += 1
          else
            puts "ERROR: Cant save district '#{code}'. #{state.errors.full_messages}"
          end
        else
          puts "SKIPING: District exists. #{row.inspect}"
        end
      else
        puts "SKIPING: State with code '#{state_code}' doesn't exist. #{row.inspect}"
      end
    end

    puts "\nTotal: #{@total}; Imported: #{@imported}"
  end
end
