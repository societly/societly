# Import States data for accounts
#
# Usage:
#  file = Rails.root.join('tmp', 'us_states_with_sovereignty.csv')
#  i = StatesImporter.new(account_id: 1, file: file)
#  i.import!
#
# Source for US: https://www.census.gov/geo/reference/ansi_statetables.html
# * FIPS Codes for the States and the District of Columbia
# * FIPS State Codes for the Outlying Areas of the United States and the Freely Associated States (with status 1 only)
#
# Expected format is TAB separated CSV file with 3 columns:
#   "Name", "FIPS State Numeric Code ", "Official USPS Code"
#   "Alabama", "01", "AL"
#
require 'csv'

class StatesImporter

  # Options:
  #
  #   :account_id - target account (required)
  #   :file - target CSV file full path (required)
  #   :csv_options - optional has for CSV importer
  #     :col_sep - (default "\t")
  #     :headers - (default true)
  def initialize(options = {})
    @account = Account.find(options[:account_id])
    @language_code = @account.languages.default_language.first.try(:code)
    @file = options[:file]
    @total = 0
    @imported = 0
    @csv_options = {col_sep: "\t", headers: true}.merge(options.fetch(:csv_options, {}))
    raise "File '#{@file}' not found!" unless File.exists?(@file)
  end

  def import!
    CSV.foreach(@file, @csv_options) do |row|
      @total += 1

      state = @account.states.find_or_initialize_by(code: row[2].to_s.strip)
      if state.new_record?
        state.name = {@language_code => row[0].to_s.strip}
        if state.save
          @imported += 1
        else
          puts "ERROR: Cant save state '#{row[2]}'. #{state.errors.full_messages}"
        end
      else
        puts "SKIPING: State exists. #{row.inspect}"
      end
    end

    puts "\nTotal: #{@total}; Imported: #{@imported}"
  end
end
