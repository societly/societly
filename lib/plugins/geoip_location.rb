# GeoIP detection using MaxMind GeoIP City database.
class GeoipLocation
  # Connect to GeoIp database
  def self.database
    @@database ||= MaxMindDB.new(Rails.root.join('tmp/geoip/GeoLite2-City.mmdb'))
  end

  # Get City level GeoIP data by ip
  # Example output:
  # #<MaxMindDB::Result:0x007fbe2b199f88
  #  @raw=
  #   {"city"=>
  #     {"geoname_id"=>5400075,
  #      "names"=>
  #       {"de"=>"Sunnyvale", "en"=>"Sunnyvale", "es"=>"Sunnyvale", "ja"=>"サニーベール", "pt-BR"=>"Sunnyvale", "ru"=>"Саннивейл", "zh-CN"=>"森尼韦尔"}},
  #    "continent"=>
  #     {"code"=>"NA",
  #      "geoname_id"=>6255149,
  #      "names"=>
  #       {"de"=>"Nordamerika", "en"=>"North America", "es"=>"Norteamérica", "fr"=>"Amérique du Nord", "ja"=>"北アメリカ", "pt-BR"=>"América do Norte", "ru"=>"Северная Америка", "zh-CN"=>"北美洲"}},
  #    "country"=>
  #     {"geoname_id"=>6252001,
  #      "iso_code"=>"US",
  #      "names"=>
  #       {"de"=>"USA", "en"=>"United States", "es"=>"Estados Unidos", "fr"=>"États-Unis", "ja"=>"アメリカ合衆国", "pt-BR"=>"Estados Unidos", "ru"=>"США", "zh-CN"=>"美国"}},
  #    "location"=>
  #     {"latitude"=>37.4249,
  #      "longitude"=>-122.0074,
  #      "metro_code"=>807,
  #      "time_zone"=>"America/Los_Angeles"},
  #    "postal"=>{"code"=>"94089"},
  #    "registered_country"=>
  #     {"geoname_id"=>6252001,
  #      "iso_code"=>"US",
  #      "names"=>
  #       {"de"=>"USA", "en"=>"United States", "es"=>"Estados Unidos", "fr"=>"États-Unis", "ja"=>"アメリカ合衆国", "pt-BR"=>"Estados Unidos", "ru"=>"США", "zh-CN"=>"美国"}},
  #    "subdivisions"=>
  #     [{"geoname_id"=>5332921,
  #       "iso_code"=>"CA",
  #       "names"=>
  #        {"de"=>"Kalifornien", "en"=>"California", "es"=>"California", "fr"=>"Californie", "ja"=>"カリフォルニア州", "pt-BR"=>"Califórnia", "ru"=>"Калифорния", "zh-CN"=>"加利福尼亚州"}}]}>
  def self.find_by_ip(ip)
    GeoipLocation.database.lookup(ip)
  rescue Exception => e
    Rails.logger.error "ERROR: GeoIP database is missing! #{e.inspect}"
    nil
  end
end
