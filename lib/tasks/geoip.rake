namespace :geoip do
  desc 'Update GeoIP (GeoLite2) database file'
  task update_geoip: :environment do
    uri = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz'

    $stderr.puts "Obtaining GeoIP database..."

    # Create directory where to put the GeoIP database in
    sh %{mkdir -p #{Rails.root}/tmp/geoip}

    # Download GeoIP database and unzip to
    sh %{cd #{Rails.root}/tmp/geoip && curl -s -O #{uri}}

    # Unzip the database file, force overwrite if necessary
    sh %{cd #{Rails.root}/tmp/geoip && gunzip -f GeoLite2-City.mmdb.gz}
  end
end
