require 'capistrano/rails/migrations'

# set :branch, 'feature/eu2019-ui'
set :stage, :production
set :keep_releases, 10
set :rails_env, 'production'
set :migration_role, 'migrator'

set :linked_files, %w{.rbenv-vars}

set :delayed_job_bin_path, 'bin'

role :app, %w{ubuntu@35.158.255.124}
role :web, %w{ubuntu@35.158.255.124}
role :db, %w{ubuntu@35.158.255.124}
role :migrator, %w{ubuntu@35.158.255.124}
