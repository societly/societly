require 'capistrano/rails/migrations'

set :stage, :production
set :keep_releases, 10
set :rails_env, 'production'
set :migration_role, 'migrator'

set :linked_files, %w{.rbenv-vars}

set :delayed_job_bin_path, 'bin'

role :app, %w{ubuntu@52.3.135.54}
role :web, %w{ubuntu@52.3.135.54}
role :db, %w{ubuntu@52.3.135.54}
role :migrator, %w{ubuntu@52.3.135.54}
