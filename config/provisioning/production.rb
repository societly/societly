$LOAD_PATH.unshift *Dir.glob(File.join(File.dirname(__FILE__), 'packages', '*'))

%w(essential git apache passenger mysql_client rbenv ruby rails exim4 societly_app logrotate imagemagick).each { |lib| require lib }

policy :base, roles: :production do
  requires :git
  requires :exim4
end

policy :database, roles: :production do
  requires :mysql_client
end

policy :apps, roles: :production do
  requires :ruby, ruby_version: '2.2.3'
  requires :rails, user: 'ubuntu'
  requires :passenger
  requires :imagemagick
  requires :rbenv_vars
  requires :societly_app, {
    app_env: 'production', server_admin: 'priit@voog.com', server_name: 'societly.com', user: 'ubuntu',
    server_aliases: %w(app.societly.com), passenger_max_pool_size: 10,
    ssl: false, ruby_version: '2.2.3'
  }
  requires :logrotate, application: 'societly', log_files: '/var/rails/societly/shared/log/*.log', postrotate: 'touch /var/rails/societly/current/tmp/restart.txt'
end

policy :web, roles: :production do
  requires :apache
end

deployment do
  delivery :capistrano do
    role :production, '52.3.135.54'

    set :user, 'ubuntu'
    set :run_method, :run

    set :default_environment, {
      'PATH' => "$HOME/.rbenv/shims:$HOME/.rbenv/bin:$PATH"
    }
  end
end
