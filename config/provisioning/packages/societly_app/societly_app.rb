package :societly_app do
  defaults port: 80, ssl: false, ruby_version: '2.2.3', server_aliases: [], passenger_max_pool_size: 5
  
  template_search_path 'config/provisioning/packages/societly_app'
  
  config_file = '/etc/apache2/sites-available/100-societly.conf'
  config_template = 'societly.conf'
  
  config = render(config_template)
  
  file config_file, content: config, owner: 'root', mode: 644, sudo: true do
    post :install do
      runner "chown root: #{config_file}", sudo: true
      runner "chmod 644 #{config_file}", sudo: true
      runner "a2ensite 100-societly", sudo: true
      runner "service apache2 reload", sudo: true
    end
  end
  
  verify do
    md5_of_file(config_file, Digest::MD5.hexdigest(config))
  end
end
