package :git do
  apt 'git-core', sudo: true
  
  verify do
    has_executable '/usr/bin/git'
  end
end
