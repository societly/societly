package :rails do
  description 'Install prerequisites for Rails apps'
  
  requires :libxml
  requires :rails_base_directory, user: opts[:user]
end

package :libxml do
  apt %w(libxslt-dev libxml2-dev), sudo: true
end

package :rails_base_directory do
  runner "mkdir -p /var/rails", sudo: true
  runner "chown #{opts[:user]}: /var/rails", sudo: true
  
  verify do
    has_directory '/var/rails'
  end
end
