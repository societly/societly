package :rbenv do
  description 'rbenv Ruby version manager'
  requires :git

  runner 'git clone https://github.com/sstephenson/rbenv.git ~/.rbenv'
  
  verify do
    has_directory '~/.rbenv'
  end
end

package :rbenv_init do
  requires :rbenv
  
  push_text 'export PATH="$HOME/.rbenv/bin:$PATH"', '~/.bashrc'
  push_text 'eval "$(rbenv init -)"', '~/.bashrc'
  
  verify do
    file_contains '~/.bashrc', 'rbenv init'
  end
end

package :ruby_build do
  description 'rbenv plugin that installs rubies'
  requires :rbenv
  
  runner 'git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build'
  
  verify do
    has_directory '~/.rbenv/plugins/ruby-build'
  end
end

package :rbenv_vars do
  description 'rbenv plugin that sets application variables'
  requires :rbenv
  
  runner 'git clone https://github.com/sstephenson/rbenv-vars.git ~/.rbenv/plugins/rbenv-vars'
  
  verify do
    has_directory '~/.rbenv/plugins/rbenv-vars'
  end
end
