package :ruby do
  description 'Ruby virtual machine'
  
  requires :build_essential
  requires :ruby_build
  requires :rbenv_init
  requires :ruby_core, ruby_version: opts[:ruby_version]
  requires :gemconfig
end

package :gemconfig do
  config_file = '.gemrc'
  config_template = File.join(File.dirname(__FILE__), 'ruby', 'gemrc')
  
  file config_file, content: File.read(config_template), mode: 644
  
  verify do
    matches_local config_template, config_file
  end
end

package :ruby_core do
  defaults ruby_version: '2.2.3'
  
  runner "rbenv install #{opts[:ruby_version]}"
  runner "rbenv local #{opts[:ruby_version]} && gem install bundler && rbenv rehash"
  
  verify do
    has_directory "~/.rbenv/versions/#{opts[:ruby_version]}"
  end
end
