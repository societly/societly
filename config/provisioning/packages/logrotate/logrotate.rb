package :logrotate do
  description 'Set up lograte configuration file'
  
  application = opts[:application]
  
  template_search_path 'config/provisioning/packages/logrotate'
  
  config_file = "/etc/logrotate.d/#{application}"
  config_template = 'logrotate.conf.erb'
  
  config = render(config_template)
  
  file config_file, content: config, mode: 644, sudo: true do
    post :install do
      runner "chown root: #{config_file}", sudo: true
    end
  end
  
  verify do
    md5_of_file(config_file, Digest::MD5.hexdigest(config))
  end
end
