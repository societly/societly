package :build_essential do
  description 'Build tools'
  
  apt %w(autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev), sudo: true
end
