package :passenger do
  description 'Phusion Passenger'
  
  requires :apache, :passenger_apt, :passenger_core, :passenger_apache_module
end

package :passenger_apt do
  description 'Set up APT repository for Phusion Passenger'
  
  apt_file = '/etc/apt/sources.list.d/passenger.list'
  
  file apt_file, content: 'deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main', mode: 600, sudo: true do
    pre :install do
      apt 'apt-transport-https ca-certificates', sudo: true
      runner 'apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 561F9B9CAC40B2F7', sudo: true
    end
    
    post :install do
      runner "chown root: #{apt_file}", sudo: true
      runner 'apt-get update', sudo: true
    end
  end
  
  verify do
    has_file apt_file
  end
end

package :passenger_core do
  requires :passenger_apt
  apt 'libapache2-mod-passenger', sudo: true
end

package :passenger_apache_module do
  runner 'a2enmod passenger', sudo: true
  runner 'service apache2 restart', sudo: true

  verify do
    has_file '/etc/apache2/mods-enabled/passenger.load'
  end
end
