package :exim4 do
  description 'Exim4 MTA'
  
  requires :exim4_core, :exim4_conf
end

package :exim4_core do
  apt 'exim4-daemon-light mailutils', sudo: true
end

package :exim4_conf do
  template_search_path 'config/provisioning/packages/exim4/exim4'
  
  config_file = '/etc/exim4/update-exim4.conf.conf'
  config_template = 'update-exim4.conf.conf'
  
  config = render(config_template)
  
  file config_file, content: config, sudo: true do
    post :install do
      runner "chown root: #{config_file}", sudo: true
      runner "chmod 644 #{config_file}", sudo: true
      runner "service exim4 reload", sudo: true
    end
  end
  
  verify do
    md5_of_file(config_file, Digest::MD5.hexdigest(config))
  end
end
