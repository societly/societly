package :apache do
  description 'Apache2 web server.'
  
  requires :apache_core, :apache_config
end

package :apache_core do
  description 'Apache web server core'
  
  apt 'apache2 apache2-utils', sudo: true
  
  verify do
    has_directory '/etc/apache2'
  end
end

package :apache_config do
  runner 'a2dissite 000-default', sudo: true do
    post :install do
      runner 'service apache2 reload', sudo: true
    end
  end
  
  runner 'a2dismod autoindex', sudo: true do
    post :install do
      runner 'service apache2 reload', sudo: true
    end
  end
  
  runner 'a2enmod headers rewrite expires', sudo: true do
    post :install do
      runner 'service apache2 reload', sudo: true
    end
  end
  
  verify do
    no_file '/etc/apache2/sites-enabled/000-default.conf'
  end
end
