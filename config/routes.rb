Rails.application.routes.draw do

  class ApiPrefixConstraint
    def initialize
      @allowed_subdomains = %w(api)
    end

    def matches?(request)
      request.path =~ /\A\/api\// || @allowed_subdomains.include?(request.subdomain)
    end
  end

  namespace :admin do
    devise_for :users

    resources :accounts, only: %i(new create edit update) do
      get :select, on: :member
      put :set_default, on: :member
    end
    resources :languages do
      put :set_default, on: :member
    end
    resources :questions do
      put :move, on: :member
    end
    resources :extra_questions do
      put :move, on: :member
    end
    resources :candidates
    resources :pages do
      put :move, on: :member
    end
    resources :parameters
    resources :submissions
    resources :customers
    resources :states do
      resources :districts
    end
    resources :users, only: %i(index create edit update destroy)

    root 'questions#index'
  end

  scope constraints: ApiPrefixConstraint.new do
    scope '(api)/:account/:language', as: :api, module: :api do
      resources :parameters, only: %i(index)
      resources :questions, only: %i(index)
      resources :extra_questions, only: %i(index)
      resources :candidates, only: %i(index)
      resources :customers, only: %i(create) do
        collection do
          get :current
          put :current, action: :update

          scope 'current', as: :customer do
            resources :submissions, only: %i(index update), controller: :customer_submissions
          end

          post :login
          delete :logout
          get :logout
        end
      end
      resources :passwords, only: %i(create update)
      resources :states, only: %i(index) do
        resources :districts, only: %i(index)
      end
      resources :districts, only: %i(index)
      resources :submissions, only: %i(index show update create) do
        put :left, on: :member
      end
    end
  end

  if Rails.application.config.app.oauth.present? && Rails.application.config.app.oauth.enabled.present?
    # Ominauth default prefix is "/auth"
    get '/auth/:provider/callback' => 'authentications#create', as: :oauth_callback, constraints: { provider: /#{Rails.application.config.app.oauth.enabled.join('|')}/ }
  end

  scope ':account/:language', as: :account do
    # Provide current_account_customer related methods
    devise_for :customers, only: [:passwords], controllers: {passwords: 'customers/passwords'}
  end

  get ':account/:publisher/:language/pages/:id' => 'pages#show', as: :page
  get ':comparsion_account/:account/:publisher/:language' => 'home#index', as: :compare_account
  get ':account/:publisher/:language' => 'home#index', as: :account_home
  post ':account/:language/submit' => 'api/submissions#create', as: :submission

  get 'ping-me' => 'home#ping'
  root 'home#root'
end
