require 'ostruct'

# Load configuration variable from ENV or from application.yml
def env_or_config(conf, key)
  ENV.fetch(key, conf[key])
end

Rails.application.configure do
  # Try to load application.yml file if not exists then use ENV only
  app_config = File.exists?(Rails.root.join('config', 'application.yml')) ? Rails.application.config_for(:application) : {}
  # Application general configuration
  config.app = OpenStruct.new

  config.app.aws = OpenStruct.new(
    access_key_id: env_or_config(app_config, 'SOCIETLY_AWS_ACCESS_KEY_ID'),
    secret_access_key: env_or_config(app_config, 'SOCIETLY_AWS_SECRET_ACCESS_KEY'),
    region: env_or_config(app_config, 'SOCIETLY_AWS_REGION'),
    assets_bucket: env_or_config(app_config, 'SOCIETLY_AWS_ASSETS_BUCKET'),
    media_public_host: env_or_config(app_config, 'SOCIETLY_MEDIA_PUBLIC_HOST')
  )
  config.app.aws.enabled = config.app.aws.access_key_id.present? && config.app.aws.secret_access_key.present? && config.app.aws.region.present?

  # Set CDN host
  config.app.cdn_host = env_or_config(app_config, 'SOCIETLY_CDN_HOST')

  # Set default from and sender for emails
  config.app.mailer = OpenStruct.new(
    default_from_email: env_or_config(app_config, 'SOCIETLY_MAILER_DEFAULT_FROM_EMAIL'),
    default_from_name: env_or_config(app_config, 'SOCIETLY_MAILER_DEFAULT_FROM_NAME')
  )

  config.app.oauth = OpenStruct.new(enabled: [])
  config.app.oauth.facebook = OpenStruct.new(
    key: env_or_config(app_config, 'SOCIETLY_FB_KEY'),
    secret: env_or_config(app_config, 'SOCIETLY_FB_SECRET')
  )
  config.app.oauth.enabled << :facebook if config.app.oauth.facebook.key.present? && config.app.oauth.facebook.secret.present?

  config.app.oauth.google = OpenStruct.new(
    provider: 'google_oauth2',
    key: env_or_config(app_config, 'SOCIETLY_GOOGLE_KEY'),
    secret: env_or_config(app_config, 'SOCIETLY_GOOGLE_SECRET'),
    options: {
      name: 'google',
      access_type: 'online',
      provider_ignores_state: true
    },
    keys: [env_or_config(app_config, 'SOCIETLY_GOOGLE_KEY')]
  )
  if config.app.oauth.google.key.present? && config.app.oauth.google.secret.present?
    config.app.oauth.enabled << :google
    config.app.oauth.google.keys += env_or_config(app_config, 'SOCIETLY_GOOGLE_SUPPORTED_KEYS').to_s.split(',')
  end

  # Setup email delivering
  config.action_mailer.default_url_options = {host: env_or_config(app_config, 'SOCIETLY_DEFAULT_HOST'), port: env_or_config(app_config, 'SOCIETLY_DEFAULT_HOST_PORT')}
  config.action_mailer.delivery_method = env_or_config(app_config, 'SOCIETLY_MAILER_DELIVERY_METHOD').downcase.to_sym if env_or_config(app_config, 'SOCIETLY_MAILER_DELIVERY_METHOD').present?

  # Setup SMTP settings
  if config.action_mailer.delivery_method == :smtp
    config.action_mailer.smtp_settings = {
      address: (env_or_config(app_config, 'SOCIETLY_SMTP_SERVER').presence || 'localhost'),
      port: (env_or_config(app_config, 'SOCIETLY_SMTP_SERVER_PORT').presence || 25).to_i,
      domain: (env_or_config(app_config, 'SOCIETLY_SMTP_DOMAIN').presence || 'example.com')
    }.tap do |h|
      h[:enable_starttls_auto] = env_or_config(app_config, 'SOCIETLY_SMTP_ENABLE_STARTTLS_AUTO').to_s.downcase == 'true' if env_or_config(app_config, 'SOCIETLY_SMTP_ENABLE_STARTTLS_AUTO').present?
      h[:authentication] = env_or_config(app_config, 'SOCIETLY_SMTP_AUTHENTICATION').to_sym if env_or_config(app_config, 'SOCIETLY_SMTP_AUTHENTICATION').present?
      h[:user_name] = env_or_config(app_config, 'SOCIETLY_SMTP_USER_NAME').presence
      h[:password] = env_or_config(app_config, 'SOCIETLY_SMTP_PASSWORD').presence
    end
  end
end
