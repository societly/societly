if Rails.application.config.app.aws.enabled
  Aws.config.update({
    region: Rails.application.config.app.aws.region,
    credentials: Aws::Credentials.new(Rails.application.config.app.aws.access_key_id, Rails.application.config.app.aws.secret_access_key),
  })
end
