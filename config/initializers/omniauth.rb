OmniAuth.config.logger = Rails.logger
Rails.application.config.middleware.use OmniAuth::Builder do
  # on_failure { |env| AuthenticationsController.action(:failure).call(env) }
  Rails.application.config.app.oauth.enabled.each do |key|
    conf = Rails.application.config.app.oauth.send(key)
    provider conf.provider || key, conf.key, conf.secret, conf.options || {}
  end
end
