# Subscribe to Societly exception notifications.
#
# Usage:
#   ActiveSupport::Notifications.instrument('internal_server.exception.societly', exception: exception, log_level: :fatal)
ActiveSupport::Notifications.subscribe(/\.exception.societly\z/) do |*args|
  event = ActiveSupport::Notifications::Event.new(*args)
  log_level = event.payload[:log_level] || :debug
  event.payload[:exception]

  Rails.logger.send log_level, "\n#{event.payload[:exception].class} (#{event.payload[:exception].message}): \n  #{event.payload[:exception].backtrace[0..8].join("\n  ")}\n"
end
