lock '3.4.0'

set :application, 'Societly'
set :repo_url, 'git@bitbucket.org:societly/societly.git'
set :deploy_to, '/var/rails/societly'

set :ssh_options, forward_agent: true
# set :branch, 'feature/eu2019-ui'
set :rbenv_type, :user
set :rbenv_ruby, '2.2.3'

set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system tmp/geoip}

set :linked_files, %w{config/database.yml}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  desc 'Download and setup GeoIP database'
  task :ensure_geoip_db do
    on roles(:app) do
      # Download GeoIP database only when not present (on initial deploy).
      unless test "[ -f #{shared_path.join('tmp/geoip/GeoLite2-City.mmdb')} ]"
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'geoip:update_geoip'
          end
        end
      end
    end
  end

  after :finishing, 'deploy:ensure_geoip_db'
  after :publishing, :restart
end
