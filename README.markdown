# Societly API

## API endpoints

* `http://societly.com/api/`
* `http://api.societly.com/`

## Request format

Request bodies should be in JSON format and request header have to include `Content-Type` header:

    Content-Type: application/json

## Pagination in API

Requests that return multiple items will be paginated to **50** items by default.

You can specify further pages with the `page` parameter. You can also set a custom page size up to **250** with the `per_page` parameter (default value is **50**). Example:

```
http://api.societly.com/default/en/candidates?page=1&per_page=50
```

Every response that includes paginated items are including also detailed info about pagination in response header. The pagination info is included in the [Link header](http://www.w3.org/wiki/LinkHeader).

Example value for `Link` header (NB! actual value does not include line brakes):

```
Link: <http://api.societly.com/default/en/candidates?page=1&level=country>; rel="first",
<http://api.societly.com/default/en/candidates?page=2&level=country>; rel="next",
<http://api.societly.com/default/en/candidates?page=2&level=country>; rel="last"
```

The possible `rel` values are:

* `next` - shows the URL of the immediate next page of results.
* `last` - shows the URL of the last page of results.
* `first` - shows the URL of the first page of results.
* `prev` - shows the URL of the immediate previous page of results.

The response includes also additional information about pagination like **total entries** and **total pages**.

Example headers:

```
X-Total-Entries: 60
X-Total-Pages: 2
```

## Parameters

Returns list of question parameter.

Request:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/parameters

Response:

    [
      {
        "id": 1,
        "name": "Parameter 1 name"
      },
      {
        "id": 2,
        "name": "Parameter 2 name"
      }
    ]

## Questions

Returns list of question:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/questions

Response:

    [
      {
        "id": 1,
        "name": "Question 1 name",
        "description": "Question 1 description",
        "params": {
          "xy-lr": "0",
          "xy-lc": "0",
          "1": 1
        },
        "position": 1
      }
    ]

## Extra questions

Returns list of account extra questions.

Request:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/extra_questions

Response:

    [
      {
        "id": 3,
        "position": 3,
        "name": "Are you registered to vote?",
        "description": null,
        "choices": [
          {
            "value": "1476276337196",
            "label": "No"
          },
          {
            "value": "1476276348285",
            "label": "Yes"
          }
        ]
      }
    ]

## Candidates

Returns list of candidate:

Request:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/candidates

Optional filter parameters:

* `level` - allowed values are `country`, `state`, `district`. It returns candidates on all levels by default when `level` is not present.
* `state_id` - state id
* `district_id` - district id

Response:

    [
      {
        "id": 2,
        "name": "Candidate 1 name",
        "description": "Candidate 1 description",
        "party": "Republican",
        "image": "<url to image>",
        "params": {"1": 1, "2": -1},
        "state_id": null,
        "district_id": null,
        "level": "country";
        "answers": [
          {
            "question_id": 1,
            "answer": 25,
            "source": "Source title",
            "source_desc": "Source description",
            "source_link": "<url to source>"
          }
        ]
      }
    ]

## States

Every account has his own set of states. By default it returns list of account state that are bounded to account candidates.

Filter parameters:

* `all` - if present then API returns full set of account states (e.g. `?all=true`).

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/states

Response:

    [
      {
        "id": 1,
        "code": "AK"
        "name": "Alaska"
      }
    ]

## Districts

Every account has his own set of districts. By default It returns list of account state districts that are bounded to account candidates.


Filter parameters:

* `all` - if present then API returns full set of state districts (e.g. `?all=true`).

Request:

1) Returns list of districts for one state of the account.

Request:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/states/1/districts

2) Returns list of account districts.

Request:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/states/districts

Response:

    [
      {
        "id": 10,
        "state_id": 1,
        "code": "AK00"
        "name": "Congressional District (at Large)"
      }
    ]

## Posting results

Additional endpoint: `http://societly.com/default/en/submit`

Request:

    curl -X POST \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"answers":[{"answer":100,"balance":1,"question_id":1},{"answer":100,"balance":1,"question_id":2}],"extra_questions":{"1476275970372":"2"},"seconds_to_result":226,"start_to_result":345,"open_page_at":"2015-09-21 14:35:21","publisher":"default","state_id":1,"district_id":1},"external_ref":"some-uniq-external-string"' \
    http://api.societly.com/default/en/submit

Result 1:

Result for submission when there has no authenticated customer.

    {
      "client_hash": "7C8F3A034F6FF5ADC9438285B080F2616CF52AFC583611F1F4D16D351B41BD65",
      "confirm_hash": "eyJpZCI6NDIsImV4cGlyZXMiOiIyMDE2LTEwLTIxIDEzOjQ1OjUxICswMzAwIn0=--aac83e1458f04459ea7199ff2a791d080d839559"
    }


`confirm_hash` value is returned only when submission has been submitted by anonymous visitor. Read more in "Claim submission ownership" section. `confirm_hash` expires after 24h.

Result 2:

Result for submission when there has authenticated customer and customer profile is not private.

    {
      "client_hash": "7C8F3A034F6FF5ADC9438285B080F2616CF52AFC583611F1F4D16D351B41BD65",
      "title": "My political profile on 20th Oct 2016",
      "customer": {
        full_name: "Test User",
        image: "http://files.societly.com.s3.amazonaws.com/0000/0000/0001/customers/0000/0000/0001/my-imaga-1443665157.jpg"
      }
    }

## Claim submission ownership

When result is posted by unauthenticated customer then it's possible to claim current customer (see "Current customer" section) ownership about recently posted submission using it's `confirm_hash` value. Customer is added to submission only when account id matches and submission customer value is not set.

Request:

    curl -X PUT \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"confirm_hash":"eyJpZCI6NDIsImV4cGlyZXMiOiIyMDE2LTEwLTIxIDEzOjQ1OjUxICswMzAwIn0=--aac83e1458f04459ea7199ff2a791d080d839559"}' \
    http://api.societly.com/default/en/submissions/7C8F3A034F6FF5ADC9438285B080F2616CF52AFC583611F1F4D16D351B41BD65

Result:

    {
      "title": "My political profile on 20th Oct 2016",
      "customer": {
        full_name: "Test User",
        image: "http://files.societly.com.s3.amazonaws.com/0000/0000/0001/customers/0000/0000/0001/my-imaga-1443665157.jpg"
      }
    }

Note: Submission `title` and `customer` attributes are returned only when submission has customer with public profile.

### Client left request

Request:

    curl -X PUT \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{}' \
    http://api.societly.com/default/en/submissions/7C8F3A034F6FF5ADC9438285B080F2616CF52AFC583611F1F4D16D351B41BD65/left

Result:

    {}

## Submissions

Get submission details by `client_hash`:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/submissions/7C8F3A034F6FF5ADC9438285B080F2616CF52AFC583611F1F4D16D351B41BD65

Response:

    {
      "state_id": 1,
      "district_id": 1,
      "answers":[
        {
          "answer": 100,
          "balance": 1,
          "question_id": 1
        },
        {
          "answer": 100,
          "balance": 1,
          "question_id": 2
        }
      ],
      "extra_questions": {
        "1476275970372": "2"
      },
      "title": "My political profile on 20th Oct 2016",
      "customer": {
        full_name: "Test User",
        image: "http://files.societly.com.s3.amazonaws.com/0000/0000/0001/customers/0000/0000/0001/my-imaga-1443665157.jpg"
      }
    }

Note: Submission `title` and `customer` attributes are returned only when submission has customer with public profile.

## Customer signup

Every account has his own set of customers.

**Signup by email and password**

Required fields:

* `email` - uniq email (in account scope)
* `password` - password (at least 4 charaters)

Create new customer and initialize session on success.

Request:

    curl -X POST \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"email":"testuser@gmail.com","full_name":"Test User","password":"mypassword"}' \
    http://api.societly.com/default/en/customers

**Signup by access token**

Required fields:

* `access_token` - access token returned by Facebook SDK or Google one-time-code flow.
* `provider` - acces token provider. Supported values: `facebook` and `google`.
* `redirect_uri` - pass the value "postmessage" in case of website login for Google. Leave it blank otherwise.

Create new customer and initialize session on success.

Request:

    curl -X POST \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"provider":"facebook","access_token":"sxZCEu6Ph4Q6DwhthuCqg8VH9Gs8tv64rRMSXcfw2qu4AUbHjuDoWvwhJ0okX6xjoZB1pFOWnbmDTMo4I3pc7ilqKvE0YH8IxoTNQLyWDS4chMP1qQZDZD""}' \
    http://api.societly.com/default/en/customers

Response for both methods:

    {
      "email": "testuser@gmail.com",
      "full_name": "Test User",
      "private": false,
      "zip": null,
      "gender": null,
      "year_of_birth": null,
      "ethnicity": null,
      "education": null,
      "image": null
    }

## Login existing customer

Log customer in. Customer session is stored in session (cookie `_societly_session`) that should be included in requests related to customer (customer and submission endpoints).

**Login by email and password**

Required fields:

* `email` - customer email.
* `password` - customer password.

Request:

    curl -X POST \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"email":"testuser@gmail.com","password":"mypassword"}' \
    http://api.societly.com/default/en/customers/login

**Login by access token**

If user is authenticated and does not exists in system then customer is created and logged in (like in signup section):

Required fields:

* `access_token` - access token returned by Facebook SDK or Google one-time-code flow.
* `provider` - acces token provider. Supported values: `facebook` and `google`.
* `redirect_uri` - pass the value "postmessage" in case of website login for Google. Leave it blank otherwise.

Create new customer and initialize session on success.

Request:

    curl -X POST \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"provider":"facebook","access_token":"sxZCEu6Ph4Q6DwhthuCqg8VH9Gs8tv64rRMSXcfw2qu4AUbHjuDoWvwhJ0okX6xjoZB1pFOWnbmDTMo4I3pc7ilqKvE0YH8IxoTNQLyWDS4chMP1qQZDZD"}' \
    http://api.societly.com/default/en/customers/login


Response for both requests:

    {
      "email": "testuser@gmail.com",
      "full_name": "Test User",
      "private": false,
      "zip": null,
      "gender": null,
      "year_of_birth": null,
      "ethnicity": null,
      "education": null,
      "image": null
    }

## Current customer

Get current logged in customer data:

Request:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/customers/current

Response:

    {
      "email": "testuser@gmail.com",
      "full_name": "Test User",
      "private": false,
      "zip": null,
      "gender": null,
      "year_of_birth": null,
      "ethnicity": null,
      "education": null,
      "image": null
    }

It returns status code `404` when there has no authenticated user in session with response:

    {
      "message": "Not found."
    }

## Update current customer

Update current customer.

Allowed attributes:

* `email` - user email.
* `full_name` - full name.
* `private` - turn of user related data in user shared results view (default is `false`).
* `zip` - zip code.
* `year_of_birth` - year.
* `gender` - expected values are `female` and `male`.
* `ethnicity` - expected values and meanings:
  * `I` - American Indian or Alaska Native
  * `A` - Asian
  * `B` - African American
  * `H` - Hispanic/Latino
  * `P` - Native Hawaiian or Other Pacific Islander
  * `M` - Two or More Races
  * `W` - Caucasian
* `education` - expected values are:
  * `primary`
  * `secondary`
  * `higher`
* `password` - allows to update customer password. When `password` is present in request then two additional parameters must be provided `password_confirmation` and `current_password`.

Request:

    curl -X PUT \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"full_name":"My Name",gender:}' \
    http://api.societly.com/default/en/customers/current

**User profile photo**

User image can be uploaded and updated only by `multipart/form-data` post where image field is named as `customer[image]`.

    curl -X PUT \
    -H 'Accept: application/json' \
    -F "customer[image]=@/Users/user1/Desktop/test.jpg"
    http://api.societly.com/default/en/customers/current

## Log out current customer

End current customer session (supports `GET` and `DELETE` requests):

Request:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/customers/logout

Response:

    {
      "success": true
    }


## Customer submissions

Get list of current customer submissions.

Request:

    curl -X GET \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    http://api.societly.com/default/en/customers/current/submissions

Response:

    [
      {
        "title": "My political profile on 20th Oct 2016",
        "client_hash": "7C8F3A034F6FF5ADC9438285B080F2616CF52AFC583611F1F4D16D351B41BD65"
      },
      {
        "title": "My political profile on 19th Oct 2016",
        "client_hash": "1C8F3A034H6FF5ADC9438285B080F2616CF52AFC583611F1F4D16D351B41BD66"
      }
    ]

## Customer submissions update

This endpoint allows customer to set new custom title to submission.

Request:

    curl -X PUT \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"title":"My view as Democrat"}' \
    http://api.societly.com/default/en/customers/current/submissions/7C8F3A034F6FF5ADC9438285B080F2616CF52AFC583611F1F4D16D351B41BD65

Response:

    {
      "title": "My political profile on 20th Oct 2016",
      "client_hash": "7C8F3A034F6FF5ADC9438285B080F2616CF52AFC583611F1F4D16D351B41BD65"
    }

## Customer password reset request

This endpoint allows customer to request password reset token (its send by email).

Request:

    curl -X POST \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"email":"my@email.com"}' \
    http://api.societly.com/default/en/passwords

Response:

    {
      "message": "Password reset link has been sent to my@email.com."
    }

Error response with status `422`:

    {
      "message": "An error occurred when requesting password reset. Please review the fields.",
      "errors": {
        "email": ["not found"]
      }
    }

## Customer password update by password reset token

This endpoint allows customer to reset password unsing token from email. It logs customer in after successfull request.

Request:

    curl -X PUT \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    -d'{"password":"NewPass","password_confirmation":"NewPass"}' \
    http://api.societly.com/default/en/passwords/Vx8QwtW9TaufHm8q1z4z

Response:

    {
      "email": "testuser@gmail.com",
      "full_name": "Test User",
      "private": false,
      "zip": null,
      "gender": null,
      "year_of_birth": null,
      "ethnicity": null,
      "education": null,
      "image": null
    }

Error response with status `422`:

    {
      "message": "Can't reset password. Please review the fields.",
      "errors": {
        "reset_password_token": ["is invalid"]
        "1": 1476275970372,
        "3": 1476276195288
      }
    }

# File storage

Files like candidate images are stored to S3. Set thes variables in your evironment or add them to `application.yml` file (in developlent):

    SOCIETLY_AWS_ASSETS_BUCKET: 'files.societly.com'
    SOCIETLY_MEDIA_PUBLIC_HOST: 'files.societly.com.s3.amazonaws.com'
    SOCIETLY_AWS_REGION: 'us-east-1'
    SOCIETLY_AWS_ACCESS_KEY_ID: 'access_key'
    SOCIETLY_AWS_SECRET_ACCESS_KEY: 'secret'

# Email setup

Email related environmental variables:

    SOCIETLY_DEFAULT_HOST: 'societly.local'
    SOCIETLY_DEFAULT_EMAIL: 'info@societly.com'
    SOCIETLY_MAILER_DEFAULT_FROM_EMAIL: "noreply@societly.com"
    SOCIETLY_MAILER_DEFAULT_FROM_NAME: "Societly"
    SOCIETLY_SMTP_DOMAIN: "societly.com"
    SOCIETLY_DEFAULT_HOST_PORT: "3000"
    SOCIETLY_MAILER_DELIVERY_METHOD: "smtp"
    SOCIETLY_SMTP_SERVER_PORT: "1025"

# Importers

## States importer

Import account related US states form CSV file.

Source for US: https://www.census.gov/geo/reference/ansi_statetables.html

* FIPS Codes for the States and the District of Columbia
* FIPS State Codes for the Outlying Areas of the United States and the Freely Associated States (with status 1 only)

Expected format is TAB separated CSV file with 3 columns:

```
Name  FIPS State Numeric Code Official USPS Code
Alabama 01  AL
```

Run importer via console `bin/rails c production`:

```
file = Rails.root.join('tmp', 'us_states_with_sovereignty.csv')
i = StatesImporter.new(account_id: 1, file: file)
i.import!
```

## Districts importer

Import account related US Congressional District Codes form CSV file.

Source for US 113th Congressional District Codes: https://www.census.gov/geo/reference/codes/cd113.html (should be converted to tab separated CSV).

Expected format is TAB separated CSV file with 4 columns:

```
STATE STATEFP CD113FP NAMELSAD
AL  01  1 Congressional District 1
```

Run importer via console `bin/rails c production`:

```
file = Rails.root.join('tmp', 'national_cd113.csv')
i = DistrictsImporter.new(account_id: 1, file: file)
i.import!
```

# Front end application

Backbone application for societly.com webpage.

## Building javascripts

Javascripts are currently minified by manually calling:

`make`

It expects uglifyjs command line tool to be installed on system. It can be installed npm:

`npm install -g uglify-js`

# Deployment

Application uses [Capistrano](https://capistranorb.com/) for deployment.

## Deployment flow

1. make sure that you have ran the `make` command and push the uglified JS file to the repo.
2. run
   `cap production deploy` (or `bundle exec cap production deploy`) on your local machine.

  Note that `production` here defines the target environment. Application is also deployed to https://euandi2019.eu/ in which case the environment is `euandi2019` and the deploy command should be `cap euandi2019 deploy`

## Extra tasks

All the commands that Capistrano provides can be listed by executing

`cap production -T`
