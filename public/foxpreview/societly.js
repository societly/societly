(function() {
  var style = document.createElement('style');
      style.id = "societly-id";

  style.innerHTML = ".societly-iframe { width: 100%; height: 600px; display: block; }";
  document.querySelector('head').appendChild(style); 

  var loc = encodeURIComponent(document.location.protocol + '//' + document.location.host + document.location.pathname),
      hash = document.location.hash,
      h = hash ? encodeURIComponent(document.location.hash.replace(/^#/,'')) : false,
      addr = document.getElementById('societly').getAttribute('data-address');

  var iframe = document.createElement('iframe');
  iframe.setAttribute('scrolling', 0);
  iframe.setAttribute('frameborder', 0);
  iframe.className = "societly-iframe";
  iframe.src= addr + "#iframe/" + loc + (h ? '/' + h : '');

  document.getElementById('societly').appendChild(iframe);

  if (window.addEventListener) {
    window.addEventListener("message", function(event) {
      if (event.data && (/^SocietlyHeight_\d+$/).test(event.data)) {
        var h = parseFloat(event.data.split('_')[1]);
        document.querySelector('.societly-iframe').style.height = (h + 20) + 'px';
      }
      if (event.data && (/^SocietlyScrollTop$/).test(event.data)) {
        var frameTop = document.getElementById('societly').getBoundingClientRect().top + window.pageYOffset - 40;
        window.scrollTo(0, frameTop);
      }
    }, false);
  }
})();
