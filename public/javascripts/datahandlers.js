(function($, _) {

    Politics.Answer = Backbone.Model.extend({});
    Politics.Answers = Backbone.Collection.extend({
        model: Politics.Answer
    });

    Politics.Question = Backbone.Model.extend({
        defaults: {
            "status":  "inactive",
            'my-balance': 1
        }
    });

    Politics.Questions = Backbone.Collection.extend({
        model: Politics.Question,

        getMyAnswers: function() {
            var answers = [];
            _.each(this.models, function(model) {
                answers.push({
                    answer: model.get('my-answer'),
                    balance: model.get('my-balance'),
                    question_id: model.get('id')
                });
            });
            return answers;
        },

        calculateModelParams: function(calcModel) {
            var params = {},
                coefficentSum = {},
                answers = calcModel.get('answers'),
                answer,
                value,
                p;

            _.each(this.models, function(model) {

                answer = _.findWhere(answers, {"question_id": model.get('id')});
                if (answer && typeof answer.answer !== undefined && answer.answer !== null) {
                    _.each(model.get('params'), function(p, key) {
                        if (p && p !== "0") {
                            param = parseInt(p, 10);
                            value = answer.answer;
                            if (!params[key]) { params[key] = 0; }
                            if (!coefficentSum[key]) { coefficentSum[key] = 0; }
                            if (key !== 'xy-lc' && key !== 'xy-lr') {
                                // Add a valid question to calculations (shift to negative by 50 so 50% is now neutral 0)
                                // and increase coefficient value
                                params[key] += Math.max(param * (value - 50), 0) * 2;
                                coefficentSum[key] += Math.abs(param);
                            } else {
                                params[key] += (param * (value - 50)) + 50;
                                coefficentSum[key] += 1;
                            }
                        }

                    }, this);
                }
            }, this);

            // Correct all parameters for cuefficent and shift by 50
            _.each(params, function(param, key) {
                if (typeof coefficentSum[key] != "undefined" && coefficentSum[key] !== 0) {
                    params[key] = param / coefficentSum[key];
                } else {
                    params[key] = null;
                }
            }, this);

            return params;
        }
    });

    Politics.Party = Backbone.Model.extend({
        defaults: {
            'my-preference': false
        },

        getAnswerById: function(qid) {
            var ret = null;
            _.each(this.get('answers'), function(a) {
                if (a.question_id == qid) {
                    ret = a;
                }
            }, this);
            return ret;
        }
    });

    Politics.Parties = Backbone.Collection.extend({
        model: Politics.Party,

        url: function() {
          return candidates_url + '?level=country';
        },

        calculateDistances: function(my_answers) {
            var my_answer;

            _.each(this.models, function(model) {
                var distanceSum = 0,
                    distanceSumWOBalance = 0,
                    count = 0,
                    balanceCount = 0;

                _.each(model.get('answers'), function(answer) {
                    var my_answer = _.findWhere(my_answers, {"question_id": answer.question_id});

                    if (typeof my_answer.answer !== "undefined" &&  my_answer.answer !== null) {
                        distanceSum += Math.abs(my_answer.answer - answer.answer) *  my_answer.balance;
                        balanceCount +=  my_answer.balance;
                        distanceSumWOBalance += Math.abs(my_answer.answer - answer.answer);
                        count++;
                    }
                }, this);

                model.set('distance', distanceSum / (count * (balanceCount/count)));
                model.set('distance_unbalanced', distanceSumWOBalance / count);

            }, this);

        },

        // Removes also district candidates
        removeStateCandidates: function() {
          var removables = [];

          _.each(this.models, function(model) {
            if (model.get('state_id')) {
              removables.push(model);
            }
          });

          if (removables.length > 0) {
            this.remove(removables);
          }
        },

        removeDistrictCandidates: function() {
          var removables = [];

          _.each(this.models, function(model) {
            if (model.get('district_id')) {
              removables.push(model);
            }
          });

          if (removables.length > 0) {
            this.remove(removables);
          }
        },

        appendComparisionCandidates: function(callback) {
          var items;
          if (!this.comparisionCache) {
            this.comparisionCache = {};
          }
          if (this.comparisionCache[comparsionAccountCode]) {
            this.add(this.comparisionCache[comparsionAccountCode]);
            callback();
          } else {
            $.getJSON(candidates_url, {
              'comparision': comparsionAccountCode,
            }).done(function(data) {
              this.comparisionCache[comparsionAccountCode] = data;
              this.add(this.comparisionCache[comparsionAccountCode]);
              callback();
            }.bind(this)).fail(function() {});
          }
        },

        removeComparisionCandidates: function() {
          var removables = [];

          _.each(this.models, function(model) {
            account = model.get('account');
            if (account.code !== subAccountCode) {
              removables.push(model);
            }
          });

          if (removables.length > 0) {
            this.remove(removables);
          }
        },

        appendStateCandidates: function(state_id) {
          $.getJSON(candidates_url, {
            'level': 'state',
            'state_id': state_id
          }).done(function(data) {
            this.add(data);
            this.appendDistrictCandidates(state_id);
          }.bind(this)).fail(function() {

          });
        },

        appendDistrictCandidates: function(state_id, district_id) {
          var q = {
            'level': 'district',
            'state_id': state_id,
          };

          if (district_id) {
            q.district_id = district_id;
          }

          $.getJSON(candidates_url, q)
            .done(function(data) {
              this.add(data);
            }.bind(this))
            .fail(function() {});
        },

        getMyPreferences: function () {
            return this.map(function(model) {
                return {
                    'party_id' : parseInt(model.get('id'), 10),
                    'preference': model.get('my-preference')
                };
            });
        }
    });

    Politics.QuestionParam = Backbone.Model.extend({});
    Politics.QuestionParams = Backbone.Collection.extend({
        model: Politics.QuestionParam
    });

    Politics.Client = Backbone.Model.extend({
        url: function() {
            if (this.get('client_hash')) {
                return results_url + '/' + this.get('client_hash');
            } else {
                return submission_url;
            }
        },

        parse: function(data) {
          return $.extend(true, {customer: null, title: null}, data);
        },

        claimOwnership: function() {
          if (this.has('confirm_hash') && this.get('client_hash')) {
            $.ajax({
              method: 'PUT',
              url: results_url + '/' + this.get('client_hash'),
              contentType: "application/json",
              dataType: "json",
              data: JSON.stringify({
                confirm_hash: this.get('confirm_hash')
              }),
              success: function(data) {
                this.set('title', data.title || null);
                this.set('customer', data.customer || null);
                this.unset('confirm_hash');
              }.bind(this)
            });
          }
        }
    });

    Politics.Customer = Backbone.Model.extend({
      defaults: {
        private: false
      },

      url: customer_url + '/current',

      idAttribute: "email", // Its is bit weird, but this way backbone knows to put not post

      initialize: function() {
        this.set('submissions', new Politics.CustomerSubmissions());
      },

      logout: function() {
        $.getJSON(customer_url + '/logout', function() {
          this.clear();
        }.bind(this));
      }
    });

    Politics.CustomerSubmission = Backbone.Model.extend({
      idAttribute: "client_hash"
    });

    Politics.CustomerSubmissions = Backbone.Collection.extend({
      url: customer_url + '/current/submissions',
      model: Politics.CustomerSubmission
    });

    Politics.ResultAccount = Backbone.Model.extend({
      defaults: {
        isOpen: false,
      },
      initialize: function(options) {
        this.isOpen = false;
        this.party = options.party;
        this.parties = options.parties;
      },
      idAttribute: 'party.id',
      distance: function() {
        return this.party.get('distance');
      },
      distance_unbalanced: function() {
        return this.party.get('distance_unbalanced');
      },
      name: function() {
        return this.party.get('account').name;
      },
      image: function() {
        return this.party.get('image');
      }
    });

    Politics.ResultAccounts = Backbone.Collection.extend({
      model: Politics.ResultAccount
    });

})(jQuery, _);
