var RadarChart = {
  draw: function(id, d, options){

    function wrap(text, width) {
      text.each(function() {
        var text = d3.select(this),
            words = text.text().split(/\s+/).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1, // ems
            y = text.attr("y"),
            x = text.attr("x"),
            tspan = text.text(null).append("tspan");

        while (word = words.pop()) {
          line.push(word);
          tspan.text(line.join(" "));
          if (tspan.node().getComputedTextLength() > width) {
            line.pop();
            tspan.text(line.join(" "));
            line = [word];
            tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + "em").text(word);
          }
        }
      });
    }

    var cfg = {
     radius: 5,
     w: 600,
     h: 600,
     factor: 1,
     factorLegend: 0.85,
     levels: 5,
     maxValue: 0,
     radians: 2 * Math.PI,
     opacityArea: 1,
     color: d3.scale.category10()
    };
    if('undefined' !== typeof options){
      for(var i in options){
        if('undefined' !== typeof options[i]){
          cfg[i] = options[i];
        }
      }
    }
    cfg.maxValue = Math.max(cfg.maxValue, d3.max(d, function(i){return d3.max(i.map(function(o){return o.value;}))}));
    var allAxis = (d[0].map(function(i, j){return i.axis}));
    var total = allAxis.length;
    var radius = cfg.factor*Math.min(cfg.w/2, cfg.h/2);
    var xspace = 100,
        yspace = 50;


    d3.select(id).select("svg").remove();
    var g = d3.select(id).append("svg").attr("width", cfg.w + (xspace*2)).style({
        "margin": "0px auto",
        "display": "block",
        "max-width": "100%"
    }).attr("height", cfg.h + (yspace*2)).attr("viewBox", "0 0 " + (cfg.w + (xspace*2)) + " " + (cfg.h + (yspace*2))).append("g")

    var tooltip;

    /*for(var j=0; j<cfg.levels; j++){
      var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
      g.selectAll(".levels").data(allAxis).enter().append("svg:line")
       .attr("x1", function(d, i){return levelFactor*(1-cfg.factor*Math.sin(i*cfg.radians/total));})
       .attr("y1", function(d, i){return levelFactor*(1-cfg.factor*Math.cos(i*cfg.radians/total));})
       .attr("x2", function(d, i){return levelFactor*(1-cfg.factor*Math.sin((i+1)*cfg.radians/total));})
       .attr("y2", function(d, i){return levelFactor*(1-cfg.factor*Math.cos((i+1)*cfg.radians/total));})
       .attr("class", "line").style("stroke", "rgba(0,0,0,0.1)").style("stroke-width", "0.5px").attr("transform", "translate(" + (cfg.w/2-levelFactor) + ", " + (cfg.h/2-levelFactor) + ")");
    }*/

    for(var j = cfg.levels; j--;){
      var levelFactor = cfg.factor*radius*((j+1)/cfg.levels);
      g.selectAll(".levels").data(allAxis).enter().append("svg:circle")
       .attr('r', levelFactor)
       .attr('cx', cfg.w/2 + xspace)
       .attr('cy', cfg.h/2 + yspace)
       .attr("class", "line").style("stroke", "rgba(0,0,0,0.1)").style('fill', 'rgba(0,0,0,0.01)').style("stroke-width", "0.5px");
    }

    for(var j = cfg.levels; j--;){
      var levelFactor2 = cfg.factor*radius*((j+1)/cfg.levels);
      g.append("text")
        .attr("class", "legend-txt")
        .text((j+1) *20).style("font-family", "arial").style({
          "font-size": "12px",
          "text-anchor": "middle",
          "font-weight": "300",
          "fill": "rgba(0,0,0,0.6)"
       })
      .attr("x", cfg.w/2 + xspace)
      .attr("y", cfg.h/2 - levelFactor2 + yspace);
    }

    series = 0;

    d.forEach(function(y, x){
      dataValues = [];
      g.selectAll(".nodes")
        .data(y, function(j, i){
          dataValues.push([
            cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
            cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
          ]);
        });
      dataValues.push(dataValues[0]);
      g.selectAll(".area")
                     .data([dataValues])
                     .enter()
                     .append("polygon")
                     .attr("class", "radar-chart-serie"+series)
                     .style("stroke-width", "2px")
                     .style("stroke", cfg.color(series))
                     .attr("points",function(d) {
                         var str="";
                         for(var pti=0;pti<d.length;pti++){
                           if (typeof d[pti] !== 'undefined') {
                             str=str + (d[pti][0] + xspace) + "," + (d[pti][1] + yspace) + " ";
                           }
                         }
                         return str;
                      })
                     .style("fill", "rgb(0,0,0)")
                     .style("fill-opacity", .1);/*
                     .on('mouseover', function (d){
                                        z = "polygon."+d3.select(this).attr("class");
                                        g.selectAll("polygon").transition(200).style("fill-opacity", 0.1);
                                        g.selectAll(z).transition(200).style("fill-opacity", 0.3);
                                      })
                     .on('mouseout', function(){
                                        g.selectAll("polygon").transition(200).style("fill-opacity", .6);
                     });*/
      series++;
    });
    series=0;


    d.forEach(function(y, x){
      g.selectAll(".nodes")
        .data(y).enter()
        .append("svg:circle").attr("class", "radar-chart-serie"+series)
        .attr('r', cfg.radius)
        .attr("alt", function(j){return Math.max(j.value, 0)})
        .attr("cx", function(j, i){
          dataValues.push([
            cfg.w/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)),
            cfg.h/2*(1-(parseFloat(Math.max(j.value, 0))/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total))
        ]);
        return cfg.w/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.sin(i*cfg.radians/total)) + xspace;
        })
        .attr("cy", function(j, i){
          return cfg.h/2*(1-(Math.max(j.value, 0)/cfg.maxValue)*cfg.factor*Math.cos(i*cfg.radians/total)) + yspace;
        })
        .attr("data-id", function(j){return j.axis})
        .style("fill", cfg.color(series)).style("fill-opacity", .9)
        .on('mouseover', function (d){
                    newX =  parseFloat(d3.select(this).attr('cx')) - 10;
                    newY =  parseFloat(d3.select(this).attr('cy')) - 5;
                    tooltip.attr('x', newX).attr('y', newY).text(d.value).transition(200).style('opacity', 1);
                    z = "polygon."+d3.select(this).attr("class");
                    g.selectAll("polygon").transition(200).style("fill-opacity", 0.5);
                    g.selectAll(z).transition(200).style("fill-opacity", .9);
                  })
        .on('mouseout', function(){
                    tooltip.transition(200).style('opacity', 0);
                    g.selectAll("polygon").transition(200).style("fill-opacity", cfg.opacityArea);
                  })
        .append("svg:title")
        .text(function(j){return Math.max(j.value, 0)});

      series++;
    });

    var axis = g.selectAll(".axis").data(allAxis).enter().append("g").attr("class", "axis");

    axis.append("line")
        .attr("x1", cfg.w/2 + xspace)
        .attr("y1", cfg.h/2 + yspace)
        .attr("x2", function(j, i){return cfg.w/2*(1-cfg.factor*Math.sin(i*cfg.radians/total)) + xspace;})
        .attr("y2", function(j, i){return cfg.h/2*(1-cfg.factor*Math.cos(i*cfg.radians/total)) + yspace;})
        .attr("class", "line").style("stroke", "rgba(0,0,0,0.1)").style("stroke-width", "1px");

    var t = axis.append("text").attr("class", "legend")
        .text(function(d){return d}).style("font-family", "arial").style({
            "font-size": "14px",
            "text-anchor": "middle",
            "font-weight": "300",
            "fill": "rgba(0,0,0,0.8)",
         }).attr("transform", function(d, i){return "translate(0,0)"})
        .attr("x", function(d, i){return (cfg.w/2*(1-cfg.factorLegend*Math.sin(i*cfg.radians/total))-20*Math.sin(i*cfg.radians/total))*1.5;})
        .attr("y", function(d, i){return (cfg.h/2*(1-Math.cos(i*cfg.radians/total))+20*Math.cos(i*cfg.radians/total))*1.25;});
    setTimeout(function() {
      t.call(wrap, 180);
    }, 0);

    //Tooltip
    tooltip = g.append('text').style('opacity', 0).style('font-family', 'sans-serif').style('font-size', '13px');
  }
};
