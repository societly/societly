Politics = Politics || {};

(function($, _) {

    function capitalize(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function stringFromErrors(data) {
      var str = '';

      if (data && data.errors) {
        for (var error in data.errors) {
          if (data.errors.hasOwnProperty(error) && data.errors[error]) {
            str += capitalize(error) + ' ' + data.errors[error] + '.\n';
          }
        }
      }

      return str;
    }

    var Workspace = Backbone.Router.extend({

      routes: {
        "": "start",
        "question/:id": "question",
        "balance": "balance",
        "userinfo": "userinfo",
        "extraquestions": "extraquestions",
        "save": "save",
        "results/list/:hash" : "resultslist",
        "results/list/partys/:hash" : "resultslistp",
        "results/party/:party/:hash" : "party",
        "results/radar/:hash": "radar",
        "results/xy/:hash": "xy",
        "results/comparision/:hash": "comparision",
        "results/radar/party/:id/:hash": "partyRadar",
        "iframe/:location/:hash": "iframe",
        "iframe/:location": "iframe"
      },

      question: function(id) {
        // checks if id is the one after answered question if not direct to first question (happens on reload to question url or lang change)
        if (id <= 1 || politics_site.questions.at(id-2).get('visited')) {
            politics_site.changeToQuestion(id);
        } else {
            app_router.navigate("start", {trigger: true});
        }
      },

      getParameterByName: function(name, url) {
        if (!url) {
          url = window.location.href;
        }

        name = name.replace(/[\[\]]/g, "\\$&");

        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);

        if (!results) {
          return null;
        }

        if (!results[2]) {
          return '';
        }

        return decodeURIComponent(results[2].replace(/\+/g, " "));
      },

      removeParameterByName: function (key, sourceURL) {
        var rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";

        if (queryString !== "") {
          params_arr = queryString.split("&");

          for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
              params_arr.splice(i, 1);
            }
          }

          if (params_arr.length > 0) {
            rtn = rtn + "?" + params_arr.join("&");
          }
        }
        return rtn;
      },

      start: function() {
        politics_site.changeToView(politics_site.getStartView(), undefined, 'start');
        if (this.getParameterByName('cref')) {
          // TODO: Global variable might not be the best solution.
          cref = this.getParameterByName('cref');

          window.history.replaceState({}, document.title, this.removeParameterByName('cref', document.location.href));
        }
      },

      save: function() {
        // checks if last question is answered (happens on reload to question url or lang change)
        if (politics_site.questions.last().get('visited')) {
            politics_site.sendScrollToTopOfFrame();
            politics_site.changeToView(politics_site.getSaveView());
        } else {
            app_router.navigate("start", {trigger: true});
        }
      },

      balance: function() {
        // checks if last question is answered (happens on reload to question url or lang change)
        if (politics_site.questions.last().get('visited')) {
            politics_site.sendScrollToTopOfFrame();
            politics_site.changeToView(new Politics.BalanceView({
                'collection': politics_site.questions
            }));
        } else {
            app_router.navigate("start", {trigger: true});
        }
      },

      userinfo: function() {
        // checks if last question is answered (happens on reload to question url or lang change)
        if (politics_site.questions.last().get('visited')) {
            politics_site.sendScrollToTopOfFrame();
            politics_site.changeToView(new Politics.UserInfoView({
                'client': politics_site.client
            }));
        } else {
            app_router.navigate("start", {trigger: true});
        }
      },

      extraquestions: function() {
        // checks if last question is answered (happens on reload to question url or lang change)
        if (politics_site.questions.last().get('visited')) {
          if (document.getElementById('extra-questions-template')) {
            politics_site.sendScrollToTopOfFrame();
            politics_site.changeToView(new Politics.ExtraQuestionsView({
                'client': politics_site.client
            }));
          } else {
            app_router.navigate("save", {trigger: true});
          }
        } else {
            app_router.navigate("start", {trigger: true});
        }
      },

      resultslist: function(hash) {
        politics_site.changeToView(politics_site.getResultsView(), undefined, undefined, true);
      },

      resultslistp: function(hash) {
        politics_site.changeToView(politics_site.getResultsView(true), undefined, undefined, true);
      },

      party: function(pid, hash) {
          var model = politics_site.parties.get(pid);
          politics_site.changeToView(new Politics.PartyView({
              'model': model,
              'questionsCollection': politics_site.questions
          }), undefined, undefined, true);
      },

      partyRadar: function(pid, hash) {
          if (pid !== -1) {
              var model = politics_site.parties.get(pid);
              politics_site.changeToView(politics_site.getRadarView(model, "party"), undefined, undefined, true);
          } else {
              politics_site.changeToView(politics_site.getRadarView(null, "party"), undefined, undefined, true);
          }
      },

      radar: function(pid, hash) {
          politics_site.changeToView(politics_site.getRadarView(), undefined, undefined, true);
      },

      xy: function(pid, hash) {
          politics_site.changeToView(politics_site.getXYView(), undefined, undefined, true);
      },

      comparision: function(pid, hash) {
          politics_site.changeToView(politics_site.getComparisionView(), undefined, undefined, true);
      },

      iframe: function(location, hash) {
        politics_site.iframeLocation = decodeURIComponent(location);
        if (hash && (/^results\//).test(hash)) {
            politics_site.iframeHash = hash;

            politics_site.client.set({
                'client_hash': _.last(hash.split('/'))
            });
            $.when(politics_site.client.fetch(), politics_site.parties.fetch()).then($.proxy(function() {
                politics_site.initClient();
                app_router.navigate(decodeURIComponent(hash), {trigger: true});
            }, this));
        }
      }

    });

    var app_router = new Workspace();
    window.app_router = app_router;

    Politics.MainView = Backbone.View.extend({
        events: {
            "click .menu-btn": "handleMenuBtnClick",
            "click .js-facebok": "facebook",
            "click .js-twitter": "twitter",
            "click .lang-menu a": "languageClick",
            "click .js-restart": "restartClick",
            "click .js-login": "loginClick",
            "click .js-signup": "signupClick",
            "click .js-customer-logout": "handleLogoutClick",
            "click .authenticated-buttons": "handleCustomerMenuClick",
            "click .js-profile": "handleProfileClick",
            "click .js-submissions": "handleSubmissionsClick",
        },

        el: "#main-view",

        initialize: function() {
            _.bindAll(this, 'handleMenuSideClick', 'initialRender');
            this.questions = new Politics.Questions(questions);
            this.parties = new Politics.Parties();
            this.questionParams = new Politics.QuestionParams(questionParams);
            this.client = new Politics.Client();
            this.customer = new Politics.Customer();
            this.loaded = false;

            this.customerMenuSideClickH = this.customerMenuSideClickHandler.bind(this);

            this.$content = this.$('#mainContent');
            this.$footer = this.$('.footer');
            this.$menu = this.$('.main-menu');

            this.curView = null;
            this.initTime = (new Date()).getTime();

            this.state = null;
            this.district = null;

            // Block clicking on unclickable menu element
            this.$('.main-menu .unclickable').click(function(event) { event.preventDefault(); });

            this.listenTo(this.customer, 'change:email', this.handleCustomerEmailChange.bind(this));
            this.listenTo(this.customer, 'change:image', this.handleCustomerImgChange.bind(this));
            this.listenTo(this.customer, 'change:full_name', this.handleCustomerNameChange.bind(this));

            this.listenTo(this.customer, 'change', this.handleCustomerChange.bind(this));

            this.listenTo(window.app_router, 'route', function(route) {
              if (route === 'start') {
                this.$('.frontpage-banner').show();
              } else {
                this.$('.frontpage-banner').hide();
              }
            });

            window.onbeforeunload = $.proxy(this.handleUnloadPage, this);
        },

        render: function() {
            if(!this.loaded) {
                Politics.openPageTime = new Date();
                this.initialRender();
                this.loaded = true;
            } else {
                this.renderFooter();
                this.renderMainMenu();
                this.$('.main-menu .unclickable').click(function(event) { event.preventDefault(); });
                this.afterFetchRender();
                setTimeout(function() {
                    Backbone.history.start();
                }, 0);
            }
            if (!('ontouchstart' in window)) {
                this.$el.addClass('canhover');
            }

            return this;
        },

        renderFooter: function() {
            var tpl = _.template($('#footer-template').html());
            this.$footer.html(tpl());
        },

        renderMainMenu: function() {
            var tpl = _.template($('#main-menu-template').html());


            this.$menu.html(tpl());
            this.$('.main-menu .unclickable').click(function(event) { event.preventDefault(); });

        },

        handleUnloadPage: function() {
            if (this.client.get('client_hash') && this.startTime) {
                $.ajax({
                      type: "put",
                      url: results_url + '/' + this.client.get('client_hash') + '/left',
                      dataType: "json",
                      async: false,
                      success: function () {}
                });
            }
        },

        // Render after translations
        initialRender: function() {
            var splashTemplate = _.template($('#splash-template').html()),
                startTime = (new Date()).getTime();

            this.$content.hide();
            this.renderFooter();
            this.renderMainMenu();
            this.$('.main-menu .unclickable').click(function(event) { event.preventDefault(); });

            if (typeof siteConfig !== 'undefined' && siteConfig.noLoader) {
                this.$content.show();
                this.afterFetchRender();
            } else {
                this.$content.html(splashTemplate());
                this.$('.moving-logo .arrow').addClass('animate');
                this.$content.fadeIn(500, $.proxy(function() {

                    // Start delaying animation only should be shown if no hash present
                    var ti = (document.location.hash && (/^#results\//).test(document.location.hash)) ? 0 : 2000,
                        passedTime = (new Date()).getTime() - startTime,
                        secs = Math.max(ti - passedTime, 0);
                    setTimeout($.proxy(function() {
                        this.afterFetchRender();
                    }, this), secs);

                } , this));
            }
        },

        getStateDistricts: function(callback) {
          $.getJSON(states_url + '/' + politics_site.state + '/districts').done(function(data) {
            callback(data);
          }.bind(this)).fail(function() {
            callback(null);
          });
        },

        afterFetchRender: function(f) {

            $('.splash-bg').addClass('hidden');

            if (document.location.hash && (/^#results\//).test(document.location.hash)) {
                // If hash then show results
                this.client.set({
                    'client_hash': _.last(document.location.hash.split('/'))
                });

                if (has_customers) {
                  $.when(this.customer.fetch()).always(function() {
                    $.when(this.client.fetch(), this.parties.fetch()).done($.proxy(function() {
                        this.initClient();
                        if (politics_site.state) {
                          this.getStateDistricts(function(d) {
                            politics_site.districts = d;
                            if (f) { f(); }
                            setTimeout(function() {
                                Backbone.history.start();
                            }, 0);
                          });
                        } else {
                          if (f) { f(); }
                          setTimeout(function() {
                              Backbone.history.start();
                          }, 0);
                        }
                    }, this));
                  }.bind(this));
                } else {
                  $.when(this.client.fetch(), this.parties.fetch()).done($.proxy(function() {
                      this.initClient();
                      if (politics_site.state) {
                        this.getStateDistricts(function(d) {
                          politics_site.districts = d;
                          if (f) { f(); }
                          setTimeout(function() {
                              Backbone.history.start();
                          }, 0);
                        });
                      } else {
                        if (f) { f(); }
                        setTimeout(function() {
                            Backbone.history.start();
                        }, 0);
                      }
                  }, this));
                }

            } else {
                // Else show startview
                this.changeToView(this.getStartView(), f, 'start');
                setTimeout(function() {
                    Backbone.history.start();
                    if (has_customers) {
                      this.customer.fetch();
                    }
                }.bind(this), 0);
            }
        },

        initClient: function() {
            this.initiatedWithHash = false;

            // Re-introduce answers and balances to questions collection
            _.each(this.client.get('answers'), function(answer) {
                if (this.questions.get(answer.question_id)) {
                    this.questions.get(answer.question_id).set('my-answer', answer.answer);
                    this.questions.get(answer.question_id).set('my-balance', answer.balance);
                }
            }, this);

            var my_answers = this.questions.getMyAnswers();
            this.parties.calculateDistances(my_answers);

            if (this.client.get('state_id')) {
              politics_site.state = this.client.get('state_id');
              this.parties.appendStateCandidates(politics_site.state);
            }

            if (this.client.get('district_id') && politics_site.state) {
              politics_site.district = this.client.get('district_id');
              this.parties.appendDistrictCandidates(politics_site.state, politics_site.district);
            }
        },

        getDocHeight: function() {
            return Math.max($('.modal').height() + 100, this.$('.global-wrapper').height()) + 40;
        },

        changeToView: function(view, f, name, fullwidth) {
            this.curView = view;
            if (view.$el) {
                view.$el.detach();
            }
            this.$content.html(view.render().el);
            if (f) {
                f();
            }

            this.sendHeightToParentWindow();
            if (name) {
              $('body').attr('data-view', name);
            } else {
              $('body').attr('data-view', 'default');
            }

            $('body').toggleClass('fullwidth', fullwidth || false);
        },

        // Detect iframe presence and notify parent of its content height changes
        sendHeightToParentWindow: function() {
            if (window !== window.top) {
                if (window.top.postMessage) {
                    window.top.postMessage("SocietlyHeight_" + this.getDocHeight(), "*");
                }
            }
        },

        sendScrollToTopOfFrame: function() {
          if (window !== window.top) {
              if (window.top.postMessage) {
                  window.top.postMessage("SocietlyScrollTop", "*");
              }
          }
        },

        changeToQuestion: function(nr) {
            var model = this.questions.at(nr - 1),
                maxQuestions = this.questions.length;

            this.changeToView(new Politics.QuestionView({"model": model, "nr": nr, "maxQuestions": maxQuestions}));
        },

        getCrefParameter: function() {
          var url = new URL(document.location.href);

          return url.searchParams.get("cref");
        },

        // Called when all questions answered
        // Saves the questions, calculates the distances and shows results
        allAnswered: function() {
            var endTime = (new Date()).getTime(),
                starttoresult = (this.startTime) ? (endTime - this.startTime) / 1000 : -1,
                secstoresult = (this.initTime) ? (endTime - this.initTime) / 1000 : -1;

            this.showSpinner();

            // Make new client(user) based on the answeres.
            // Will have a uniquie hash bound to it after save so it can be recalled

            this.client.unset('client_hash');
            this.client.set({
                'publisher': publisher,
                'answers': this.questions.getMyAnswers(),
                'start_to_result': starttoresult,
                'seconds_to_result': secstoresult,
                'open_page_at': Politics.openPageTime.toISOString(),
                'external_ref': window.cref,
                'comparision_account': comparsionAccountCode
            });

            $.when(this.client.save(), this.parties.fetch()).then($.proxy(function() {
                this.hideSpinner();

                // Calculate client results
                var my_answers = this.questions.getMyAnswers();
                this.parties.calculateDistances(my_answers);

                // Show results with appropriate client hashed url
                politics_site.sendScrollToTopOfFrame();

                if (politics_site.state) {
                  this.parties.appendStateCandidates(politics_site.state);
                }

                if (politics_site.district && politics_site.state) {
                  this.parties.appendDistrictCandidates(politics_site.state, politics_site.district);
                }

                app_router.navigate("results/list/" + this.client.get('client_hash'), {trigger: true});

            }, this));
        },

        getStartView: function() {
            return (new Politics.StartView());
        },

        getSaveView: function() {
          return (new Politics.SubmitView());
        },

        getResultsView: function(ptab) {
            return this.ResultsView = new Politics.ResultsView({
                "questionsCollection": this.questions,
                "partiesCollection": this.parties,
                "client": this.client
            });
        },

        getRadarView: function(model, type) {
            return this.RadarView = new Politics.RadarView({
                "questionsCollection": this.questions,
                "partiesCollection": this.parties,
                "client": this.client,
                "type": type || null,
                "model": model || null
            });
        },

        getXYView: function(model) {
            return this.XYView = new Politics.XYView({
                "questionsCollection": this.questions,
                "partiesCollection": this.parties,
                "client": this.client,
                "model": model || null
            });
        },

        getComparisionView: function() {
            return this.ComparisionView = new Politics.ComparisionView({
                "questionsCollection": this.questions,
                "partiesCollection": this.parties,
                "client": this.client
            });
        },

        handleMenuBtnClick: function(event) {
            event.preventDefault();
            var $menu = $('.main-menu');
            if ($menu.is('.open')) {
                $menu.removeClass('open');
                $(document.body).off('click.mainmenu');
            } else {
                $menu.addClass('open');
                $(document.body).on('click.mainmenu', this.handleMenuSideClick);
            }
        },

        handleMenuSideClick: function(event) {
            if ($(event.target).closest('.main-menu').length == 0) {
                this.$('.main-menu').removeClass('open');
                $(document.body).off('click.mainmenu');
            }
        },

        showSpinner: function($el) {
            var opts = {
              lines: 7, // The number of lines to draw
              length: 0, // The length of each line
              width: 5, // The line thickness
              radius: 10, // The radius of the inner circle
              corners: 0, // Corner roundness (0..1)
              rotate: 0, // The rotation offset
              direction: 1, // 1: clockwise, -1: counterclockwise
              color: '#222', // #rgb or #rrggbb
              speed: 1, // Rounds per second
              trail: 56, // Afterglow percentage
              shadow: false, // Whether to render a shadow
              hwaccel: true, // Whether to use hardware acceleration
              className: 'spinner', // The CSS class to assign to the spinner
              zIndex: 2e9, // The z-index (defaults to 2000000000)
              top: '50%', // Top position relative to parent in px
              left: '50%' // Left position relative to parent in px
            };

            if ($el) {
                $el.show();
            } else {
                this.$('.global-spinner').show();
            }

            if (!this.spinner) {
                if ($el) {
                    this.spinner = new Spinner(opts).spin($el.get(0));
                } else {
                    this.spinner = new Spinner(opts).spin(this.$('.global-spinner').get(0));
                }
            } else {
                if ($el) {
                    this.spinner.spin($el.get(0));
                } else {
                    this.spinner.spin(this.$('.global-spinner').get(0));
                }
            }
        },

        hideSpinner: function($el) {
            if (this.spinner) {
                this.spinner.stop();
                if ($el) {
                    $el.hide();
                } else {
                    this.$('.global-spinner').hide();
                }
            }
        },

        facebook: function(event) {
            event.preventDefault();
            var loc = this.iframeLocation || (window.location.protocol + '//' + window.location.host + '/');
            loc += window.location.hash;

            window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(loc), 'facebook-share-dialog', 'width=626,height=436');
        },

        twitter: function(event) {
          event.preventDefault();

          var loc = this.iframeLocation || (window.location.protocol + '//' + window.location.host + '/');
          loc += window.location.hash;

          window.open('https://twitter.com/intent/tweet?url=' + encodeURIComponent(loc), 'facebook-share-dialog', 'width=550,height=420');
        },

        languageClick: function(event) {
            event.preventDefault();
            var href = window.location.href,
                code = $(event.target).data('lang'),
                hsplit = href.split('#'),
                newHref = hsplit[0].replace(/\/[^\/]+$/, '/' + code);

            if (this.iframeLocation) {
              if (hsplit[1] && (/^iframe\//).test(hsplit[1])) {
                newHref += '#iframe/' + encodeURIComponent(this.iframeLocation);
              } else {
                newHref += '#iframe/' + encodeURIComponent(this.iframeLocation) + '/' +  encodeURIComponent(hsplit[1]);
              }
            } else {
              postfix = hsplit[1] ? "#" + hsplit[1] : '';
              if (window.cref) {
                newHref += '?cref=' + cref;
              }
              newHref += postfix;
            }

            document.location = newHref;
        },

        restartClick: function(event) {
            event.preventDefault();
            if (this.iframeLocation) {
                document.location = "#iframe/" + encodeURIComponent(this.iframeLocation);
                document.location.reload();
            } else {
                var href = window.location.href;
                if (window.cref) {
                  document.location.href = href.split('#')[0] + '?cref=' + cref;
                } else {
                  document.location.href = href.split('#')[0];
                }
            }
        },

        loginClick: function(event) {
          event.preventDefault();
          this.showSignupModal('login');
        },

        signupClick: function(event) {
          event.preventDefault();
          this.showSignupModal('signup');
        },

        showSignupModal:function(tab) {
          if (!this.signupModal) {
            this.signupModal = new Politics.SignupModal().render();
            this.signupModal.on('modal:hide', this.hideSignupModal.bind(this));
          }

          if (tab) {
            this.signupModal.setTab(tab);
          }

          document.body.appendChild(this.signupModal.el);
          $(document.body).addClass('modal-open');
          politics_site.sendHeightToParentWindow();
        },

        hideSignupModal: function() {
          if (this.signupModal) {
            document.body.removeChild(this.signupModal.el);
          }
          $(document.body).removeClass('modal-open');
          politics_site.sendHeightToParentWindow();
        },

        showProfileModal:function(tab) {
          if (!this.profileModal) {
            this.profileModal = new Politics.ProfileModal().render();
            this.profileModal.on('modal:hide', this.hideProfileModal.bind(this));
          }

          if (tab) {
            this.profileModal.setTab(tab);
          }

          document.body.appendChild(this.profileModal.el);
          $(document.body).addClass('modal-open');
          politics_site.sendHeightToParentWindow();
        },

        hideProfileModal: function() {
          if (this.profileModal) {
            document.body.removeChild(this.profileModal.el);
          }
          $(document.body).removeClass('modal-open');
          politics_site.sendHeightToParentWindow();
        },

        handleProfileClick: function(event) {
          event.preventDefault();
          this.showProfileModal('profile');
        },

        handleSubmissionsClick: function(event) {
          event.preventDefault();
          this.showProfileModal('submissions');
        },

        handleCustomerEmailChange: function() {
          if (this.customer.get('email')) {
            if (this.customer.get('full_name')) {
              $('.customer-menu-name').html(this.customer.get('full_name') || this.customer.get('email'));
            }
            $(document.body).addClass('authenticated');
          } else {
            $(document.body).removeClass('authenticated');
          }
        },

        handleCustomerImgChange: function() {
          if (this.customer.get('image')) {
            this.$('.customer-menu-avatar')
              .css('background-image', 'url(\'' + this.customer.get('image') + '\')')
              .removeClass('no-image');
          } else {
            this.$('.customer-menu-avatar')
              .css('background-image', '')
              .addClass('no-image');
          }
        },

        handleCustomerNameChange: function(event) {
          this.$('.customer-menu-name').html(this.customer.get('full_name') || this.customer.get('email'));
        },

        handleLogoutClick: function(event) {
          event.preventDefault();
          this.customer.logout();
        },

        handleCustomerChange: function(event) {
          if (this.client.has('client_hash')) {
            this.client.fetch();
          }
        },

        customerMenuSideClickHandler: function(event) {
          if ($(event.target).closest('.customer-dropdown').length == 0) {
            this.hideCustomerDropdown();
          }
        },

        handleCustomerMenuClick: function(event) {
          event.preventDefault();
          if ($(event.target).closest('.customer-dropdown').length === 0) {
            if ($('.customer-dropdown').hasClass('active')) {
              this.hideCustomerDropdown();
            } else {
              this.showCustomerDropdown();
            }
          }
        },

        showCustomerDropdown: function() {
          $('.customer-dropdown').addClass('active');
          document.body.addEventListener('click', this.customerMenuSideClickH);
        },

        hideCustomerDropdown: function() {
          $('.customer-dropdown').removeClass('active');
          document.body.removeEventListener('click', this.customerMenuSideClickH);
        }

    });

    Politics.StartView = Backbone.View.extend({
        events: {
            "click .js-start": "handleStart",
            "click .js-start-subaccount": "handleStartInSubAccount",
            "change #account_selector": "setAccount",
        },

        initialize: function() {
            this.template = _.template($('#start-template').html());
            this.accountCode = subAccountCode;
            politics_site.originalAccountCode = accountCode;
        },

        render: function() {
            this.$el.html(this.template());
            return this;
        },

        handleStart: function () {
            politics_site.startTime = (new Date()).getTime();
            app_router.navigate("question/1", {trigger: true});
        },

        handleStartInSubAccount: function () {
            lang = 'default';
            window.location.href = '/' + accountCode + '/' + this.accountCode + '/default/' + lang + '#question/1';
        },

        setAccount: function (event) {
          this.accountCode = event.target.value;
        }
    });


    // The view that shows the answers will be saved screen
    Politics.SubmitView = Backbone.View.extend({
        events: {
            "click .js-save": "handleSave"
        },

        initialize: function() {
            this.template = _.template($('#submit-template').html());
        },

        render: function() {
            $(window).scrollTop(0);
            this.$el.html(this.template());
            return this;
        },

        handleSave: function () {
            politics_site.allAnswered();
        }
    });

    // The view where client has to choose the relevance of questions to him
    Politics.BalanceView = Backbone.View.extend({
        events: {
            "click .js-save": "handleSave",
            "click .js-skip": "handleSkip"
        },

        className: "balances-view",

        initialize: function(options) {
            this.template = _.template($('#balance-template').html());
            this.rowTemplate = _.template($('#balance-row-template').html());
            this.collection = options.collection;
        },

        render: function() {
            $(window).scrollTop(0);
            this.$el.html(this.template());
            this.renderQuestions();
            return this;
        },

        renderQuestions: function() {
            var fragment = document.createDocumentFragment();
            _.each(this.collection.models, $.proxy(function(question, idx) {
                $(fragment).append(this.rowTemplate({
                  "nr": idx + 1,
                  "qid": question.get('id'),
                  "question": question.get('value'),
                  "balance": question.get('my-balance') || 1
                }));
            }, this));
            this.$el.find('.balance-questions').append(fragment);
        },

        updateQuestionBalances: function() {
          this.$('.balance-questions .balance-row').each($.proxy(function(idx, row) {
            var qmodel = this.collection.get($(row).data('question-id')),
                answer = $(row).find('input[type="radio"]:checked').val();
            qmodel.set('my-balance', parseFloat(answer));
          }, this));
        },

        handleSave: function () {
          this.updateQuestionBalances();

          if (document.getElementById('gather-user-info-template')) {
            app_router.navigate("userinfo", {trigger: true});
          } else if (document.getElementById('extra-questions-template')) {
            app_router.navigate("extraquestions", {trigger: true});
          } else {
            app_router.navigate("save", {trigger: true});
          }
        },

        handleSkip: function() {
          if (document.getElementById('gather-user-info-template')) {
            app_router.navigate("userinfo", {trigger: true});
          } else if (document.getElementById('extra-questions-template')) {
            app_router.navigate("extraquestions", {trigger: true});
          } else {
            app_router.navigate("save", {trigger: true});
          }
        }
    });

    // The view where client has to choose the relevance of questions to him
    Politics.UserInfoView = Backbone.View.extend({
        events: {
            "click .js-save": "handleSave",
            "click .js-skip": "handleSkip",
            'change .user-info-state': 'handleStateChange'
        },

        className: "userinfo-view",

        initialize: function(options) {
            this.template = _.template($('#gather-user-info-template').html());
            this.client = options.client;
        },

        render: function() {
            $(window).scrollTop(0);
            this.$el.html(this.template());
            return this;
        },

        handleStateChange: function(event) {
          var state = event.target.value || null;

          politics_site.state = state;
          politics_site.district = null;
        },

        updateClientInfo: function() {
            this.client.set({
                "gender": (this.$('.user-info-gender:checked').length > 0) ? this.$('.user-info-gender:checked').val() : '',
                "year_of_birth": this.$('.user-info-year-of-birth').val() || '',
                "ethnicity": this.$('.user-info-ethnicity').val() || '',
                "zip": this.$('.user-info-zipcode').val() || '',
                "education": (this.$('.user-info-education:checked').length > 0) ? this.$('.user-info-education:checked').val() :'',
                "email": this.$('.user-info-email').val() || '',
                'state_id': this.$('.user-info-state').val() ? parseInt(this.$('.user-info-state').val(), 10) : null
            });
        },

        handleSave: function () {
          this.updateClientInfo();
          if (document.getElementById('extra-questions-template')) {
            app_router.navigate("extraquestions", {trigger: true});
          } else {
            app_router.navigate("save", {trigger: true});
          }
        },

        handleSkip: function() {
          if (document.getElementById('extra-questions-template')) {
            app_router.navigate("extraquestions", {trigger: true});
          } else {
            app_router.navigate("save", {trigger: true});
          }
        }
    });

    // View that handles data from extra question fields inserted in admin
    // If no extra questions present then the view template is not present and the view should not be rendered
    Politics.ExtraQuestionsView = Backbone.View.extend({
      events: {
          "click .js-save": "handleSave",
          "click .js-skip": "handleSkip",
          "click .extra-question-options-slider .slider-option-wrap": "handleSliderOptClick"
      },

      className: "extraquestions-view",

      initialize: function(options) {
          this.template = _.template($('#extra-questions-template').html());
          this.client = options.client;
      },

      render: function() {

          $(window).scrollTop(0);
          this.$el.html(this.template());
          this.fillExistingAnswers();
          return this;
      },

      fillExistingAnswers: function() {
        var curAnswers = this.client.get('extra_questions');
        if (curAnswers) {
          for (var a in curAnswers) {
            if (curAnswers.hasOwnProperty(a)) {
              this.$('[data-equestion-id="' + a + '"] [data-answer-id="' + curAnswers[a] + '"]').prop("checked", true);
            }
          }
        }
      },

      updateClientInfo: function() {
        var data = {},
            questions = this.$el.find('[data-equestion-id]');

        questions.each(function(idx, q) {
          var qid = $(q).data('equestion-id'),
              answer = $(q).find('[data-answer-id]:checked');

          if (answer) {
            data[qid] = answer.data('answer-id');
          }
        });

        this.client.set({
          extra_questions: data
        });
      },

      handleSave: function () {
        this.updateClientInfo();
        app_router.navigate("save", {trigger: true});
      },

      handleSkip: function() {
        app_router.navigate("save", {trigger: true});
      },

      handleSliderOptClick: function(event) {
        $(event.target).closest('.slider-option-wrap').find('input').prop('checked', 'checked');
      }

    });

    Politics.QuestionView = Backbone.View.extend({
        events: {
            "click .js-answer": "handleAnswerClick",
            "click .question-nrs .js-nr": "handleQuestionNrClick"
        },
        initialize: function(options) {
            this.template = _.template($('#question-template').html());
            this.nr = options.nr;
            this.maxQuestions = options.maxQuestions;
        },

        render: function() {
            this.$el.html(this.template({
                "question": this.model.get('value'),
                "description": this.model.get('description'),
                "question_nrs": this.getQuestionNrs()
            }));
            if (typeof this.model.get('my-answer') != "undefined") {
                var val = this.model.get('my-answer');
                if (val == null) { val = ''; }
                this.$('.js-answer[data-val="' + val + '"]').addClass('active');
            }
            return this;
        },

        getQuestionNrs: function() {
            var html = "",
                visited;
            for (var i=1; i <= this.maxQuestions; i++) {
                visited = (politics_site.questions.at(i-1).get('visited')) ? " visited" : '';
                html += '<a href="#'+i+'" data-question="'+i+'" class="js-nr'+ ((this.nr == i) ? ' active' : '') + visited +'">'+i+'</a>';
            }
            return html;
        },

        // Handles the click on an answer button
        handleAnswerClick: function(event) {
            var $answer = $(event.target).closest('.js-answer'),
                val = ($answer.data('val') === "") ? null : parseFloat($answer.data('val'), 10);

            // Set the value to model, set model answered to true and add active class to selected answer btn
            this.model.set('my-answer', val);
            this.model.set('visited', true);
            $answer.addClass('active');

            // Navigate to next question or to save
            // TODO: Refactor to separate function
            if('ontouchstart' in window) {
                setTimeout($.proxy(function() {
                    if (this.nr < this.maxQuestions) {
                        app_router.navigate("question/" + (parseInt(this.nr, 10) + 1), {trigger: true});
                    } else {
                        app_router.navigate("balance", {trigger: true});
                    }
                }, this), 500);
            } else {
                if (this.nr < this.maxQuestions) {
                    app_router.navigate("question/" + (parseInt(this.nr, 10) + 1), {trigger: true});
                } else {
                    app_router.navigate("balance", {trigger: true});
                }
            }
        },

        // Hanadles clicl on numbers for navigation
        // Will not navigate past first unvisited answer
        handleQuestionNrClick: function(event) {
            event.preventDefault();
            var nr = $(event.target).data('question');
            if (politics_site.questions.at(nr -1).get('visited') === true || (nr > 1 && politics_site.questions.at(nr -2).get('visited') === true) || nr < this.nr) {
                app_router.navigate("question/" + (parseInt(nr, 10)), {trigger: true});
            }
        }
    });

    Politics.RegionPicker = Backbone.View.extend({
      events: {
        'change .state-picker': 'handleStateChange',
        'change .district-picker': 'handleDistrictChange'
      },

      initialize: function(options) {
        this.template = _.template($('#additional-regions-picker-template').html());
        this.state = options.state || null;
        this.district = options.district || null;
        this.districts = options.districts || null;
      },

      render: function() {
        this.$el.html(this.template());
        if (this.state) {
          this.$el.find('.state-picker option[value="' + this.state + '"]').attr('selected', 'selected');
          this.updateDistricts();
        }
        return this;
      },

      handleStateChange: function(event) {
        this.state = event.target.value || null;
        this.district = null;
        this.districts = null;
        politics_site.districts = null;
        this.trigger('pick:state', this.state, this.district);
        politics_site.showSpinner();
        this.updateDistricts(function() {
          politics_site.hideSpinner();
          /*if (this.districts && this.districts.length === 1) {
            this.district = this.districts[0].id;
            this.$el.find('.district-picker').val(this.district);
            this.trigger('pick:district', this.district);
          }*/
        }.bind(this));

      },

      handleDistrictChange: function(event) {
        this.district = parseInt(event.target.value, 10) || null;
        this.trigger('pick:district', this.district);
      },

      updateDistricts: function(callback) {
        var f = function(districts) {
          this.districts = districts;
          politics_site.districts = districts;
          var districtOptionsHtml = this.renderDistrictOptions(districts);
          this.$el.find('.district-picker').html(districtOptionsHtml);
          this.$el.find('.district-selection').removeClass('pol-hidden');
          if (callback) {
            callback();
          }
        }.bind(this);

        if (this.state) {
          this.$el.find('.district-selection').addClass('pol-hidden');
          if (this.districts) {
            f(this.districts);
          } else {
            this.getStateDistricts(this.state, f);
          }
        } else {
          this.districts = null;
          politics_site.districts = null;
          this.$el.find('.district-picker').html('');
          this.$el.find('.district-selection').addClass('pol-hidden');
          if (callback) {
            callback();
          }
        }
      },

      getStateDistricts: function(state, callback) {
        $.getJSON(states_url + '/' + this.state + '/districts').done(function(data) {
          callback(data);
        }.bind(this)).fail(function() {
          callback(null);
        });
      },

      renderDistrictOptions: function(districts) {
        var html = '<option value="">---</option>';
        _.each(districts, function(d) {
          if (parseInt(this.district, 10) === parseInt(d.id, 10)) {
            html += '<option value="' + d.id + '" selected>' + d.name + '</option>';
          } else {
            html += '<option value="' + d.id + '">' + d.name + '</option>';
          }
        }.bind(this));
        return html;
      }


    });

    Politics.ShareBoxView = Backbone.View.extend({

      className: "share-box-view",

      initialize: function(options) {
        this.template = _.template($('#share-template').html());
        this.listenTo(politics_site.client, 'change:customer', this.render.bind(this));
      },

      render: function() {
        var customer = politics_site.client.get('customer'),
            confirmHash = politics_site.client.get('confirm_hash');

        this.el.innerHTML = this.template();

        $('body').removeClass('share-shows-customer').removeClass('share-shows-upsell');

        if (customer) {
          this.$('.sharer-info-box').addClass('shows-customer');
          $('body').addClass('share-shows-customer');
          this.$('.sharer-avatar')
            .css('background-image', customer.image ? 'url(\'' + customer.image  + '\')' : '')
            .toggleClass('no-image', !customer.image);

          this.$('.sharer-name').html(customer.full_name);
          this.$('.share-title').html(politics_site.client.get('title'));
        } else if (confirmHash) {
          this.$('.sharer-info-box').addClass('shows-upsell');
          $('body').addClass('share-shows-upsell');
        }
        return this;
      }

    });

    Politics.ResultsView = Backbone.View.extend({
        events: {
            "click .js-party-row": "handlePartyClick",
            "click .radar-list-toggle .list-btn": "handleListToggleClick",
            "click .radar-list-toggle .radar-btn": "handleRadarToggleClick",
            "click .radar-list-toggle .xy-btn": "handleXYToggleClick",
            "click .radar-list-toggle .comparision-btn": "handleComparisionToggleClick",
            "click .js-toggle-balances": "toggleBalances"
        },

        initialize: function(options) {
            this.questionsCollection = options.questionsCollection;
            this.partiesCollection = options.partiesCollection;

            this.template = _.template($('#result-tables-template').html());
            this.client = options.client;

            this.balancesOn = true;
            this.partiesCollection.on('update', this.handlePartiesUpdate.bind(this));
            this.partiesCollection.removeComparisionCandidates();
        },

        render: function() {
            this.$el.html(this.template({
                "showBeginAgainButton": politics_site.initiatedWithHash || false
            }));

            this.renderPartyRows();
            $(window).scrollTop(0);

            setTimeout(function() {
                if (window.FB) {
                    FB.XFBML.parse(this.el);
                }
                if (window.twttr) {
                    twttr.widgets.load();
                }
            }.bind(this),0);

            if (has_states && has_districts) {
              this.regionsPicker = new Politics.RegionPicker({
                el: this.el.querySelector('.additional-regions-picker'),
                state: politics_site.state,
                district: politics_site.district,
                districts: politics_site.districts
              }).render();

              this.regionsPicker.on('pick:state', this.handleStatePick.bind(this));
              this.regionsPicker.on('pick:district', this.handleDistrictPick.bind(this));
            }

            this.$('.social-media').html('').append(new Politics.ShareBoxView().render().el);

            return this;
        },

        handlePartiesUpdate: function() {
          var my_answers = this.questionsCollection.getMyAnswers();
          this.partiesCollection.calculateDistances(my_answers);
          this.renderPartyRows();
        },

        renderPartyRows: function() {
            var distances = _.groupBy(_.sortBy(this.partiesCollection.models, function(model) {
                    return model.get('distance');
                }), function(model) {
                  return model.get('level');
                }),
                districtGroups = (distances.district) ? _.groupBy(distances.district, function(model) {
                  return model.get('district_id');
                }) : null;

            this.$('.js-partys-matches').html(this.renderRowContents(distances.country));

            if (distances.state) {
              this.$('.js-partys-state-matches').html(this.renderRowContents(distances.state));
              this.$el.find('.list-table-state').removeClass('pol-hidden');
            } else {
              this.$el.find('.list-table-state').addClass('pol-hidden');
            }

            this.$('.js-district-tables').html('');

            if (distances.district && districtGroups) {

              _.each(districtGroups, function(g, gid) {
                this.$('.js-district-tables').append(this.renderDistricttableContents(g, gid));
              }.bind(this));

              this.$el.find('.list-table-district').removeClass('pol-hidden');
            } else {
              this.$el.find('.list-table-district').addClass('pol-hidden');
            }
        },

        renderRowContents: function(list) {
          var html = '',
              template = _.template($('#party-table-row-template').html()),
              c = 0;

          _.each(list, function(party) {
              c++;
              var distance = this.balancesOn ? Math.round(100 - parseFloat(party.get('distance'), 10)) : Math.round(100 - parseFloat(party.get('distance_unbalanced'), 10));
              html += template({
                  "id": party.get('id'),
                  "nr": c,
                  "name": party.get('name'),
                  "image": party.get('image') || '',
                  "percent": distance
              });
          }, this);
          return html;
        },

        renderDistricttableContents: function(districtList, districtId) {
          return _.template($('#district-table-template').html())({
            districtName: _.find(politics_site.districts, function(d) { return d.id == districtId; }).name,
            list: this.renderRowContents(districtList),
            threecol: false
          });
        },

        handleStatePick: function(state, district) {
          politics_site.state = state;
          politics_site.district = district;

          this.partiesCollection.removeStateCandidates();

          if (politics_site.state) {
            this.partiesCollection.appendStateCandidates(politics_site.state);
          }
        },

        handleDistrictPick: function(district, dist) {
          politics_site.district = district;

          this.partiesCollection.removeDistrictCandidates();

          if (politics_site.state && politics_site.district) {
            this.partiesCollection.appendDistrictCandidates(politics_site.state, politics_site.district);
          } else if (politics_site.state) {
            this.partiesCollection.removeStateCandidates();
            this.partiesCollection.appendStateCandidates(politics_site.state);
          }
        },

        handlePartyClick: function(event) {
            event.preventDefault();
            var $r = $(event.target).closest('.js-party-row'),
                pid = $r.data('party-id'),
                h = this.client.get('client_hash');

             app_router.navigate("results/party/" + pid + "/" + h, {trigger: true});
        },

        toggleBalances: function(event) {
            event.preventDefault();
            var $btn = this.$('.js-toggle-balances');
            $btn.toggleClass('active');
            this.balancesOn = $btn.is('.active');
            this.renderPartyRows();
        },

        handleListToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/list/" + h, {trigger: true});
        },

        handleRadarToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/radar/" + h, {trigger: true});
        },

        handleXYToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/xy/" + h, {trigger: true});
        },

        handleComparisionToggleClick: function(event) {
          event.preventDefault();
          var h = politics_site.client.get('client_hash');
          app_router.navigate("results/comparision/" + h, {trigger: true});
        },
    });

    Politics.ComparisionView = Backbone.View.extend({
      events: {
          "click .js-party-row": "handlePartyClick",
          "click .js-account-row": "handleAccountClick",
          "click .radar-list-toggle .list-btn": "handleListToggleClick",
          "click .radar-list-toggle .radar-btn": "handleRadarToggleClick",
          "click .radar-list-toggle .xy-btn": "handleXYToggleClick",
          "click .radar-list-toggle .comparision-btn": "handleComparisionToggleClick",
          "click .js-toggle-balances": "toggleBalances",
          "click .js-toggle-grouping": "toggleAccountGrouping",
      },

      initialize: function(options) {
          politics_site.showSpinner();
          this.questionsCollection = options.questionsCollection;
          this.partiesCollection = options.partiesCollection;

          this.template = _.template($('#result-comparision-template').html());
          this.client = options.client;
          this.partiesCollection.appendComparisionCandidates(function() {
              politics_site.hideSpinner();
          });
          this.balancesOn = true;
          this.groupByAccount = true;
          this.partiesCollection.on('update', this.handlePartiesUpdate.bind(this));
      },

      render: function() {
          this.$el.html(this.template({
              'showBeginAgainButton': politics_site.initiatedWithHash || false,
              'groupByAccount': this.groupByAccount,
          }));

          this.renderPartyRows();
          $(window).scrollTop(0);

          setTimeout(function() {
              if (window.FB) {
                  FB.XFBML.parse(this.el);
              }
              if (window.twttr) {
                  twttr.widgets.load();
              }
          }.bind(this),0);

          this.$('.social-media').html('').append(new Politics.ShareBoxView().render().el);

          return this;
      },

      handlePartiesUpdate: function() {
        var my_answers = this.questionsCollection.getMyAnswers();
        this.partiesCollection.calculateDistances(my_answers);
        this.renderPartyRows();
      },

      renderPartyRows: function() {
          var parties = this.partiesCollection.models;
          var sortedByDistance = _.sortBy(parties, function(model) {
              return model.get('distance');
          });

          var distances = _.groupBy(sortedByDistance, function(model) {
              return model.get('level');
          });


          var accountGroups = _.groupBy(sortedByDistance, function(model) {
              return model.get('account').name;
          });

          var accounts = new Politics.ResultAccounts();
          accounts.comparator = 'distance';
          accounts.add(_.map(accountGroups, function(group) {
              var parties = _.map(group, function(party) {
                var distance = this.balancesOn ? party.get('distance') : party.get('distance_unbalanced');
                party.set('percentage', Math.round(100 - parseFloat(distance, 10)));
                return party;
              })
              return new Politics.ResultAccount({
                party: _.first(group),
                parties: parties,
              });
            })
          );

          if (this.groupByAccount) {
            this.$('.js-partys-matches').html(this.renderAccountRows(accounts.models));
          } else {
            this.$('.js-partys-matches').html(this.renderRowContents(distances.country));
          }

          this.$el.find('.list-table-state').addClass('pol-hidden');
          this.$('.js-district-tables').html('');
          this.$el.find('.list-table-district').addClass('pol-hidden');
      },

      renderAccountRows: function(list) {
        var html = '',
            template = _.template($('#account-table-row-template').html()),
            c = 0;

        _.each(list, function(account) {
            c++;
            var distance = this.balancesOn ? Math.round(100 - parseFloat(account.distance(), 10)) : Math.round(100 - parseFloat(account.distance_unbalanced(), 10));
            html += template({
                "id": account.cid,
                "nr": c,
                "name": account.name(),
                "image": account.image() || '',
                "percent": distance,
                "isOpen": account.isOpen,
                "parties": account.parties,
            });
        }, this);
        return html;
      },

      renderRowContents: function(list) {
        var html = '',
            template = _.template($('#party-table-row-template').html()),
            c = 0;

        _.each(list, function(party) {
            c++;
            var distance = this.balancesOn ? Math.round(100 - parseFloat(party.get('distance'), 10)) : Math.round(100 - parseFloat(party.get('distance_unbalanced'), 10));
            html += template({
                "id": party.get('id'),
                "nr": c,
                "name": party.get('name') + ' (' + (party.get('account') || {}).code + ')',
                "image": party.get('image') || '',
                "percent": distance,
                "parties": party.parties,
            });
        }, this);
        return html;
      },

      handlePartyClick: function(event) {
          event.preventDefault();
          var $r = $(event.target).closest('.js-party-row'),
              pid = $r.data('party-id'),
              h = this.client.get('client_hash');

           app_router.navigate("results/party/" + pid + "/" + h, {trigger: true});
      },

      handleAccountClick: function(event) {
        event.preventDefault();
        var pRow = $(event.target).closest('.js-account-row');
        var pid = pRow.data('account-id');
        var row = $('#account-parties-table-' + pid);
        row.toggle();
        pRow.children('td:last-child').children('div:first-child').toggleClass('ico-arrow-up');
      },

      toggleBalances: function(event) {
          event.preventDefault();
          var $btn = this.$('.js-toggle-balances');
          $btn.toggleClass('active');
          this.balancesOn = $btn.is('.active');
          this.renderPartyRows();
      },

      toggleAccountGrouping: function(event) {
        event.preventDefault();
        var $btn = this.$('.js-toggle-grouping');
        $btn.toggleClass('active');
        this.groupByAccount = $btn.is('.active');
        this.renderPartyRows();
        $(window).scrollTop(0);
    },

      handleListToggleClick: function(event) {
          event.preventDefault();
          var h = politics_site.client.get('client_hash');
          app_router.navigate("results/list/" + h, {trigger: true});
      },

      handleRadarToggleClick: function(event) {
          event.preventDefault();
          var h = politics_site.client.get('client_hash');
          app_router.navigate("results/radar/" + h, {trigger: true});
      },

      handleXYToggleClick: function(event) {
          event.preventDefault();
          var h = politics_site.client.get('client_hash');
          app_router.navigate("results/xy/" + h, {trigger: true});
      },

      handleComparisionToggleClick: function(event) {
        event.preventDefault();
        var h = politics_site.client.get('client_hash');
        app_router.navigate("results/comparision/" + h, {trigger: true});
      },
  });

    Politics.PartyView = Backbone.View.extend({
        events: {
            "click .js-radar": "handleRadarClick",
            "click .radar-list-toggle .list-btn": "handleListToggleClick",
            "click .radar-list-toggle .radar-btn": "handleRadarToggleClick",
            "click .radar-list-toggle .xy-btn": "handleXYToggleClick",
            "click .radar-list-toggle .comparision-btn": "handleComparisionToggleClick",
            'click .answer_source_link': 'handleSourceToggle'
        },

        className: "candidate-view",

        initialize: function(options) {
            this.questionsCollection = options.questionsCollection;
            this.my_answers = this.questionsCollection.getMyAnswers();

            this.template = _.template($('#party-view-template').html());
        },

        render: function() {

            this.$el.html(this.template({
                "showBeginAgainButton": politics_site.initiatedWithHash || false,
                "name": this.model.get('name'),
                "description": this.model.get('description'),
                "party": this.model.get('party'),
                "image": this.model.get('image') || ''
            }));

            this.renderAnswers();
            $(window).scrollTop(0);

            setTimeout(function() {
                if (window.FB) {
                    FB.XFBML.parse(this.el);
                }
                if (window.twttr) {
                    twttr.widgets.load();
                }
            }.bind(this),0);

            this.$('.social-media').html('').append(new Politics.ShareBoxView().render().el);

            return this;
        },

        handleSourceToggle: function(event) {
            event.preventDefault();
            $(event.target).closest('.answer_source').find('.answer_source_content').toggleClass('open');
            politics_site.sendHeightToParentWindow();
        },

        renderAnswers: function() {
            var template = _.template($('#question-comparison-template').html());

            _.each(this.questionsCollection.models, function(question) {
                var your_answer = question.get('my-answer'),
                    party_answer = this.model.getAnswerById(question.get('id'));
                if (party_answer) {
                    this.$('.js-question-comparison').append(template({
                        "question": question.get('value'),
                         "other": "party",
                         "your_answer": your_answer,
                         "other_answer": party_answer.answer,
                         "color": (your_answer !== null && party_answer.answer !== null) ? ((your_answer === party_answer.answer) ? "green-box": (Math.abs(your_answer - party_answer.answer) > 25) ? "red-box" : "yellow-box") : "",
                         "source": (party_answer.source && party_answer.source) ? party_answer.source : '',
                         "source_desc": (party_answer.source_desc && party_answer.source_desc) ?  party_answer.source_desc : '',
                         "source_links": (party_answer.source_link && party_answer.source_link) ?  party_answer.source_link.split(',') : []
                    }));
                }

            }, this);
        },

        handleRadarClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/radar/party/" + this.model.get('id') + "/" + h, {trigger: true});
        },

        handleListToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/list/" + h, {trigger: true});
        },

        handleRadarToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/radar/" + h, {trigger: true});
        },

        handleXYToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/xy/" + h, {trigger: true});
        },

        handleComparisionToggleClick: function(event) {
          event.preventDefault();
          var h = politics_site.client.get('client_hash');
          app_router.navigate("results/comparision/" + h, {trigger: true});
        }
    });

    Politics.XYView = Backbone.View.extend({
        events: {
            "click .radar-list-toggle .list-btn": "handleListToggleClick",
            "click .radar-list-toggle .radar-btn": "handleRadarToggleClick",
            "click .radar-list-toggle .xy-btn": "handleXYToggleClick",
            "click .radar-list-toggle .comparision-btn": "handleComparisionToggleClick",
            "click .xy-point[data-party-id]": "handlePartyPointClick"
        },

        initialize: function(options) {
            this.questionsCollection = options.questionsCollection;
            this.partiesCollection = options.partiesCollection;
            this.client = options.client;

            this.template = _.template($('#xy-template').html());
            this.my_params = this.questionsCollection.calculateModelParams(this.client);
            this.partiesCollection.on('update', this.handlePartiesUpdate.bind(this));
            this.partiesCollection.removeComparisionCandidates();
        },

        render: function() {
            $(window).scrollTop(0);
            this.$el.html(this.template({
                "showBeginAgainButton": politics_site.initiatedWithHash || false
            }));

            this.renderGraphPoints();

            setTimeout(function() {
                if (window.FB) {
                    FB.XFBML.parse(this.el);
                }
                if (window.twttr) {
                    twttr.widgets.load();
                }
            }.bind(this),0);

            if (has_states && has_districts) {
              this.regionsPicker = new Politics.RegionPicker({
                el: this.el.querySelector('.additional-regions-picker'),
                state: politics_site.state,
                district: politics_site.district,
                districts: politics_site.districts
              }).render();

              this.regionsPicker.on('pick:state', this.handleStatePick.bind(this));
              this.regionsPicker.on('pick:district', this.handleDistrictPick.bind(this));
            }

            this.$('.social-media').html('').append(new Politics.ShareBoxView().render().el);

            return this;
        },

        renderGraphPoints: function() {
          this.$plist = this.$('.xy-points');
          this.$plist.html('');

          this.partiesCollection.each(function(party) {
              this.$plist.append(this.renderPartyPointElement(party));
          }, this);

          this.$plist.append($('<div/>').addClass('xy-point my-xy-point').css({
              'top':  (100 - this.my_params["xy-lc"]) + '%',
              'left': this.my_params["xy-lr"] + '%'
          }).html(translations.you));
        },

        renderPartyPointElement: function(party) {

          var partyParams = this.questionsCollection.calculateModelParams(party),
              picDiv = $('<div/>').addClass('xy-point-image').css({
                'background-image': party.get('image') ? 'url("' + party.get('image') + '")' : 'none'
              }),
              xy = $('<div/>').addClass('xy-point').attr({
                  'data-title': party.get('name'),
                  'data-party-id': party.get('id')
              }).css({
                  'top':  (100 - partyParams["xy-lc"]) + '%',
                  'left': partyParams["xy-lr"] + '%'
              });

          if (has_states && has_districts) {
            xy.addClass('level-' + party.get('level'));
          }

          xy.append(picDiv);
          return xy;
        },

        handlePartyPointClick: function(event) {
            event.preventDefault();
            var pid = $(event.target).closest('[data-party-id]').data('party-id'),
                h = this.client.get('client_hash');

             app_router.navigate("results/party/" + pid + "/" + h, {trigger: true});
        },

        handlePartiesUpdate: function() {
          this.renderGraphPoints();
        },

        handleListToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/list/" + h, {trigger: true});
        },

        handleRadarToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/radar/" + h, {trigger: true});
        },

        handleXYToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/xy/" + h, {trigger: true});
        },

        handleComparisionToggleClick: function(event) {
          event.preventDefault();
          var h = politics_site.client.get('client_hash');
          app_router.navigate("results/comparision/" + h, {trigger: true});
        },

        handleStatePick: function(state, district) {
          politics_site.state = state;
          politics_site.district = district;

          this.partiesCollection.removeStateCandidates();

          if (politics_site.state) {
            this.partiesCollection.appendStateCandidates(politics_site.state);
          }
        },

        handleDistrictPick: function(district) {
          politics_site.district = district;

          this.partiesCollection.removeDistrictCandidates();

          if (politics_site.state && politics_site.district) {
            this.partiesCollection.appendDistrictCandidates(politics_site.state, politics_site.district);
          } else if (politics_site.state) {
            this.partiesCollection.removeStateCandidates();
            this.partiesCollection.appendStateCandidates(politics_site.state);
          }
        },

    });

    Politics.SignupModal = Backbone.View.extend({
      events: {
        'click .modal-overlay': 'hideModal',
        'click .modal-close': 'hideModal',
        'click .modal-tabs .tab': 'handletabClick',
        'click .js-login-fb': 'handleFbLogin',
        'click .js-signup-fb': 'handleFbLogin',
        'click .google-login-btn': 'handleGoogleLogin',
        'click .google-signup-btn': 'handleGoogleLogin',
        'submit .js-signup-form': 'handleSignupSubmit',
        'click .js-signup-submit': 'handleSignupSubmitClick',
        'submit .js-login-form': 'handleLoginSubmit',
        'click .js-login-submit': 'handleLoginSubmitClick',
        'click .js-forgot-password': 'handleForgotPassword'
      },

      className: 'modal-wrapper',

      initialize: function(options) {
        this.template = _.template($('#signup-modal-template').html());
      },

      render: function() {
        this.el.innerHTML = this.template();
        return this;
      },

      setTab: function(tabKey) {
        this.$('[data-tab]').each(function(idx, c) {
          $(c).toggleClass('active', $(c).attr('data-tab') == tabKey);
        });
      },

      hideModal: function(event) {
        if (event && event.preventDefault) {
          event.preventDefault();
        }
        this.trigger('modal:hide');
      },

      handletabClick: function(event) {
        event.preventDefault();
        var tabKey = $(event.target).closest('[data-tab]').attr('data-tab');
        this.setTab(tabKey);
      },

      handleFbLogin: function(event) {
        event.preventDefault();
        politics_site.showSpinner();
        FB.login(function(response) {
          if (response.authResponse) {
            $.getJSON('/auth/facebook/callback?account=' + accountCode)
            .success(
              function(json) {
                politics_site.customer.set(json);
                if (politics_site.client.has('client_hash') && politics_site.client.has('confirm_hash')) {
                  politics_site.client.claimOwnership();
                }
                this.trigger('modal:hide');
                politics_site.hideSpinner();
              }.bind(this)
            ).error(
              function(data) {
                var errorInfo = JSON.parse(data.responseText);
                alert(stringFromErrors(errorInfo))
                politics_site.hideSpinner();
              }.bind(this)
            );
          }
        }.bind(this), {scope: 'email'}); // if you want custom scopes, pass them as an extra, final argument to FB.login
      },

      handleGoogleLogin: function(event) {
        event.preventDefault();
        politics_site.showSpinner();
        auth2.grantOfflineAccess({'redirect_uri': 'postmessage'}).then(function(authResult) {
          if (authResult['code']) {
            $.getJSON('/auth/google/callback?account=' + accountCode + '&code=' + authResult['code'])
             .success(
               function(json) {
                politics_site.customer.set(json);
                if (politics_site.client.has('client_hash') && politics_site.client.has('confirm_hash')) {
                  politics_site.client.claimOwnership();
                }
                this.trigger('modal:hide');
                politics_site.hideSpinner();
              }.bind(this)
            ).error(
              function(data) {
                var errorInfo = JSON.parse(data.responseText);
                alert(stringFromErrors(errorInfo))
                politics_site.hideSpinner();
              }.bind(this)
            );
          } else {
            politics_site.hideSpinner();
          }
        }.bind(this));
      },

      handleSignupSubmit: function(event) {
        event.preventDefault();
        var email = this.$('.js-signup-email').val(),
            password = this.$('.js-signup-password').val();

        $.ajax({
          type: "POST",
          contentType: "application/json",
          url: customer_url,
          data: JSON.stringify({
            'email': email,
            'password': password
          }),
          dataType: "json",
          success: function(data) {
            politics_site.customer.set(data);
            if (politics_site.client.has('client_hash') && politics_site.client.has('confirm_hash')) {
              politics_site.client.claimOwnership();
            }
            this.hideModal();
          }.bind(this),

          error: function(d) {
            var str = 'An error occurred!\n',
                data = JSON.parse(d.responseText);

            if (data && data.errors) {
              for (var error in data.errors) {
                if (data.errors.hasOwnProperty(error) && data.errors[error]) {
                  str += capitalize(error) + ' ' + data.errors[error] + '.\n';
                }
              }
            }
            alert(str);
          }
       });

     },

     handleSignupSubmitClick: function(event) {
       event.preventDefault();
       this.$('.js-signup-form').submit();
     },

     handleLoginSubmitClick: function(event) {
       event.preventDefault();
       this.$('.js-login-form').submit();
     },

     handleLoginSubmit: function(event) {
       event.preventDefault();
       var email = this.$('.js-login-email').val(),
           password = this.$('.js-login-password').val();

       $.ajax({
         type: "POST",
         contentType: "application/json",
         url: customer_url + '/login',
         data: JSON.stringify({
           'email': email,
           'password': password
         }),
         dataType: "json",
         success: function(data) {
           politics_site.customer.set(data);
           if (politics_site.client.has('client_hash') && politics_site.client.has('confirm_hash')) {
             politics_site.client.claimOwnership();
           }
           this.hideModal();
         }.bind(this),

         error: function(d) {
           var str = 'Incorrect email or password!',
               data = JSON.parse(d.responseText);

           if (data && data.errors) {
             for (var error in data.errors) {
               if (data.errors.hasOwnProperty(error) && data.errors[error]) {
                 str += capitalize(error) + ' ' + data.errors[error] + '.\n';
               }
             }
           }
           alert(str);
         }
       });
     },

     handleForgotPassword: function(event) {
       event.preventDefault();
       var email = this.$('.js-login-email').val();

       if (!email) {
         alert(translations.fill_email_error);
         return;
       }

       $.ajax({
         type: "POST",
         contentType: "application/json",
         url: password_reset_url,
         data: JSON.stringify({
           'email': email
         }),
         dataType: "json",
         success: function(data) {
           alert(translations.key_forgot_reminder_sent);
         }.bind(this),

         error: function(d) {
           var data = JSON.parse(d.responseText);
           alert(data.message || 'Error!');
         }
       });
     }
    });

    Politics.ProfileModal = Backbone.View.extend({
      events: {
        'click .modal-overlay': 'hideModal',
        'click .modal-close': 'hideModal',
        'click .modal-tabs .tab': 'handletabClick',
        'click .js-change-profile-image': 'handleUploadImgClick',
        'change .file-inp': 'handleUploadImgChange',
        'click .js-save': 'handleProfileSave',
        'click .toggle-private': 'handlePrivacyClick'
      },

      className: 'modal-wrapper',

      initialize: function(options) {
        this.template = _.template($('#profile-template').html());
        this.formTpl = _.template($('#profile-form-template').html());
        this.listenTo(politics_site.customer, 'change:image', this.handleImgChange.bind(this));
      },

      render: function() {
        this.el.innerHTML = this.template();
        this.renderProfileForm();

        $.when(politics_site.customer.get('submissions').fetch())
         .then(this.renderSubmissions.bind(this));

        this.hasChanges = false;

        return this;
      },

      renderProfileForm: function() {
        this.$('.tab-content[data-tab="profile"]').html(this.formTpl(politics_site.customer.attributes));
        if (politics_site.customer.get('image')) {
          this.$('.profile-photo-big').removeClass('no-image');
        } else {
          this.$('.profile-photo-big').addClass('no-image');
        }
      },

      handleImgChange: function() {
        if (politics_site.customer.get('image')) {
          this.$('.profile-photo-big')
            .css('background-image', 'url(\'' + politics_site.customer.get('image') + '\')')
            .removeClass('no-image');
        } else {
          this.$('.profile-photo-big')
            .css('background-image', '')
            .addClass('no-image');
        }
      },

      handlePrivacyClick: function(event) {
        event.preventDefault();
        var $btn = $(event.target).closest('.toggle-private');

        if ($btn.is('.is-private')) {
          $btn.removeClass('is-private');
          this.$('[name="customer[private]"]').val('false');
        } else {
          $btn.addClass('is-private');
          this.$('[name="customer[private]"]').val('true');
        }
      },

      setTab: function(tabKey) {
        if (tabKey === 'profile' && !this.$('.tab-content[data-tab="profile"]').is('active') ) {
          this.renderProfileForm();
        }
        this.$('[data-tab]').each(function(idx, c) {
          $(c).toggleClass('active', $(c).attr('data-tab') == tabKey);
        });
        politics_site.sendHeightToParentWindow();
      },

      hideModal: function(event) {
        if (event && event.preventDefault) {
          event.preventDefault();
        }
        this.trigger('modal:hide');
      },

      handletabClick: function(event) {
        event.preventDefault();
        var tabKey = $(event.target).closest('[data-tab]').attr('data-tab');
        this.setTab(tabKey);
      },

      renderSubmissions: function() {
        this.$('.submissions-list').html();
        this.$('.submissions-list').append((new Politics.SubmissionsListView({
          collection: politics_site.customer.get('submissions')
        })).render().el);
        politics_site.sendHeightToParentWindow();
      },

      handleUploadImgClick: function(event) {
        event.preventDefault();
        this.$('.file-inp').click();
      },

      handleUploadImgChange: function(event) {
        var files = event.target.files,
            file = files[0] ? URL.createObjectURL(files[0]) : false;

        if (file) {
          this.$('.profile-photo-big')
            .css('background-image', 'url(\'' + file + '\')')
            .removeClass('no-image');
        } else {
          this.$('.profile-photo-big')
            .css('background-image', '')
            .addClass('no-image');
        }

        this.hasChanges = true;
      },

      handleProfileSave: function(event) {
        event.preventDefault();
        this.save();
      },

      save: function() {
        if ('FormData' in window) {
          var form = this.$('.customer-profile-form').get(0),
              fdata = new FormData(form);

          politics_site.showSpinner();

          $.ajax({
            url: form.getAttribute('action'),
            data: fdata,
            type: 'PUT',
            // THIS MUST BE DONE FOR FILE UPLOADING
            contentType: false,
            processData: false,
            success: function(data) {
              politics_site.hideSpinner();
              politics_site.customer.set(data);
              this.hideModal()
            }.bind(this),
            error: function() {
              politics_site.hideSpinner();
              alert('Saving failed! Try again later!');
            }
          });
        } else {
          alert('Sorry! You need to update your browser to support uploading images.');
        }
      }
    });

    Politics.SubmissionsListView = Backbone.View.extend({
      initialize: function(options) {

      },

      render: function() {
        this.el.innerHTML = '';
        this.collection.each(function(submission) {
          this.$el.append((new Politics.SubmissionView({
            model: submission
          })).render().el);
        }.bind(this));
        return this;
      }
    });

    Politics.SubmissionView = Backbone.View.extend({
      events: {
        'click .submission-link': 'handleSubmissionClick'
      },

      className: 'submission-row',

      initialize: function() {
        this.template = _.template($('#submission-row-template').html());
      },

      render: function() {
        this.$el.html(this.template({
          title: this.model.get('title'),
          url: document.location.pathname + '#results/list/' + this.model.get('client_hash')
        }));

        return this;
      },

      handleSubmissionClick: function(event) {
        event.preventDefault();
        var loc = $(event.target.closest('a')).attr('href');
        document.location.href = loc;
        document.location.reload();
      }
    });

    Politics.RadarView = Backbone.View.extend({
        events: {
            "click .js-partys-matches tr": "handlePartyClick",
            "click .radar-list-toggle .list-btn": "handleListToggleClick",
            "click .radar-list-toggle .radar-btn": "handleRadarToggleClick",
            "click .radar-list-toggle .xy-btn": "handleXYToggleClick",
            "click .radar-list-toggle .comparision-btn": "handleComparisionToggleClick"
        },

        initialize: function(options) {
            this.questionsCollection = options.questionsCollection;
            this.partiesCollection = options.partiesCollection;
            this.client = options.client;

            var my_answers = this.questionsCollection.getMyAnswers();

            this.partiesCollection.calculateDistances(my_answers);
            this.partyDistances  = _.sortBy(this.partiesCollection.models, function(model) {
                return model.get('distance');
            });

            this.template = _.template($('#radar-template').html());
            this.type = options.type;

            this.pol_colors = ["blue", "green", "purple", "red", "yellow"];
            this.party_colors = ["blue", "yellow", "green", "red", "purple", "pink"];

            this.color_table = {
                "blue": "#789fd0",
                "green": "#84be6c",
                "purple": "purple",
                "red": "#e36b7a",
                "yellow":"#fea367",
                "pink": "#f54eff"
            };

            this.partiesCollection.on('update', this.handlePartiesUpdate.bind(this));
            this.partiesCollection.removeComparisionCandidates();
        },

        render: function() {
            this.$el.html(this.template({
                "showBeginAgainButton": politics_site.initiatedWithHash || false
            }));

            this.renderPartyRows();
            $(window).scrollTop(0);
            if (this.cannotShowMe) {
                this.$('.radar-legends .alerts').html(
                        _.template($('#alert-row-template').html())()
                );
            }

            setTimeout(function() {
                if (window.FB) {
                    FB.XFBML.parse(this.el);
                }
                if (window.twttr) {
                    twttr.widgets.load();
                }
            }.bind(this),0);

            if (has_states && has_districts) {
              this.regionsPicker = new Politics.RegionPicker({
                el: this.el.querySelector('.additional-regions-picker'),
                state: politics_site.state,
                district: politics_site.district,
                districts: politics_site.districts
              }).render();

              this.regionsPicker.on('pick:state', this.handleStatePick.bind(this));
              this.regionsPicker.on('pick:district', this.handleDistrictPick.bind(this));
            }

            this.$('.social-media').html('').append(new Politics.ShareBoxView().render().el);

            return this;
        },

        handleStatePick: function(state, district) {
          politics_site.state = state;
          politics_site.district = district;

          this.partiesCollection.removeStateCandidates();

          if (politics_site.state) {
            this.partiesCollection.appendStateCandidates(politics_site.state);
          }
        },

        handleDistrictPick: function(district) {
          politics_site.district = district;

          this.partiesCollection.removeDistrictCandidates();

          if (politics_site.state && politics_site.district) {
            this.partiesCollection.appendDistrictCandidates(politics_site.state, politics_site.district);
          } else if (politics_site.state) {
            this.partiesCollection.removeStateCandidates();
            this.partiesCollection.appendStateCandidates(politics_site.state);
          }
        },

        drawPartyRadar: function(mod_col) {
            var my_d = this.getMyParamsData(),
                d = [my_d],
                c = ["white"],
                tmp;

            _.each(mod_col, function(mc){
                var p_data = this.getModelData(mc.party);
                d.push(p_data);
                c.push(this.color_table[mc.color]);
            }, this);

            // switch 2 points on radar
            for (var x = d.length; x--;) {
                if (d[x].length >= 2) {
                    tmp = d[x][0];
                    d[x][0] = d[x][1];
                    d[x][1] = tmp;
                }
            }

            RadarChart.draw(this.$(".js-party-radar").get(0), d, {
                color: function(m) {
                    return c[m];
                },
                radius: 0,
                w:400, h:400,
                opacityArea: 0.3,
                maxValue: 100,
                factorLegend:0.6
            });
        },

        handlePartiesUpdate: function() {
          var my_answers = this.questionsCollection.getMyAnswers();
          this.partiesCollection.calculateDistances(my_answers);
          this.renderPartyRows();
        },

        getClosestParty: function() {
          var closest = null;
          this.partiesCollection.each(function(model) {
            if (!closest || model.get('distance') < closest.get('distance')) {
              closest = model;
            }
          });
          return closest;
        },

        renderPartyRows: function() {
          var distances = _.groupBy(_.sortBy(this.partiesCollection.models, function(model) {
                return model.get('distance');
              }), function(model) {
                return model.get('level');
              }),
              districtGroups = (distances.district) ? _.groupBy(distances.district, function(model) {
                return model.get('district_id');
              }) : null;

          this.$('.js-partys-country-matches').html(this.renderRowContents(distances.country));

          if (distances.state) {
            this.$('.js-partys-state-matches').html(this.renderRowContents(distances.state));
            this.$el.find('.list-table-state').removeClass('pol-hidden');
          } else {
            this.$el.find('.list-table-state').addClass('pol-hidden');
          }

          this.$('.js-district-tables').html('');
          if (distances.district && districtGroups) {

            _.each(districtGroups, function(g, gid) {
              this.$('.js-district-tables').append(this.renderDistricttableContents(g, gid));
            }.bind(this));

            this.$el.find('.list-table-district').removeClass('pol-hidden');
          } else {
            this.$el.find('.list-table-district').addClass('pol-hidden');
          }

          if (this.model) {
              $check = this.$('.js-partys-matches [data-party-id="' + this.model.get('id') + '"] .js-check-party');
          } else {
            var closestParty = this.getClosestParty();
            if (closestParty) {
              $check = this.$('.js-partys-matches [data-party-id="' + closestParty.get('id') + '"] .js-check-party');
            } else {
              $check = this.$('.js-partys-matches .js-check-party').first();
            }
          }

          c = $check.data('color');
          $check.addClass('checked ' + c);
          $check.closest('tr').addClass(c);
          this.drawPartyRadar(this.getCheckedPartyArr());
          this.$('.js-partys-matches .js-check-party').attr('unselectable', 'on');
        },

        renderRowContents: function(list) {
          var html = '',
              template = _.template($('#party-table-row-template-check').html()),
              c = 0;

          _.each(list, function(party, idx) {
              c++;
              var distance = this.balancesOn ? Math.round(100 - parseFloat(party.get('distance'), 10)) : Math.round(100 - parseFloat(party.get('distance_unbalanced'), 10));
              html += template({
                "id": party.get('id'),
                "name": party.get('name'),
                "image": party.get('image') || '',
                "percent": 100 - parseInt(party.get('distance'), 10),
                "color": this.party_colors[idx % this.party_colors.length]
              });
          }, this);
          return html;
        },

        renderDistricttableContents: function(districtList, districtId) {
          return _.template($('#district-table-template').html())({
            districtName: _.find(politics_site.districts, function(d) { return d.id == districtId; }).name,
            list: this.renderRowContents(districtList),
            threecol: true
          });
        },

        getMyParamsData: function() {
            var my_p = this.questionsCollection.calculateModelParams(this.client),
                d = [];

            _.each(my_p, function(p, pid) {
                if (pid !== "xy-lr" && pid !== "xy-lc" && politics_site.questionParams.get(pid)) {
                    if (p  === null) { this.cannotShowMe = true; }
                    d.push({
                        "axis": politics_site.questionParams.get(pid).get('value'),
                        "value": p
                    });
                }
            }, this);
            return d;
        },

        getModelData: function(model) {
            var params = this.questionsCollection.calculateModelParams(model),
                d = [];

            _.each(params, function(p, pid) {
                if (pid !== "xy-lr" && pid !== "xy-lc" && politics_site.questionParams.get(pid)) {
                    d.push({
                      "axis": politics_site.questionParams.get(pid).get('value'),
                      "value": p
                    });
                }
            }, this);

            return d;
        },

        getCheckedPartyArr: function() {
            var r = [];
            _.each(this.$('.js-partys-matches .checked'), function(c) {
              if (this.partiesCollection.get($(c).closest('.js-party-row').data('party-id'))) {
                r.push({
                    "party": this.partiesCollection.get($(c).closest('.js-party-row').data('party-id')),
                    "color": $(c).data('color')
                });
              }
            }, this);
            return r;
        },

        handlePartyClick: function(event) {
            event.preventDefault();
            var $cboxes = this.$('.js-check-party.checked'),
                $target = $(event.target).closest('tr').find('.js-check-party');

            if ($target.hasClass('checked')) {
                $target.removeClass('checked').removeClass(this.party_colors.join(' '));
                $(event.target).closest('tr').removeClass(this.party_colors.join(' '));
                this.drawPartyRadar(this.getCheckedPartyArr());

            } else {
                $target.addClass('checked').addClass($target.data('color'));
                $(event.target).closest('tr').addClass($target.data('color'));
                this.drawPartyRadar(this.getCheckedPartyArr());
            }
        },

        handleListToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/list/" + h, {trigger: true});
        },

        handleRadarToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/radar/" + h, {trigger: true});
        },

        handleXYToggleClick: function(event) {
            event.preventDefault();
            var h = politics_site.client.get('client_hash');
            app_router.navigate("results/xy/" + h, {trigger: true});
        },

        handleComparisionToggleClick: function(event) {
          event.preventDefault();
          var h = politics_site.client.get('client_hash');
          app_router.navigate("results/comparision/" + h, {trigger: true});
        },
    });

})(jQuery, _);
