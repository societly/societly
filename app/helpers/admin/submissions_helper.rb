module Admin::SubmissionsHelper

  # Calculate distance between candidate and submitter
  #   submission - hash of submission answers where key is the question id
  def candidate_distance(candidate, answers)
    distance_sum = BigDecimal.new('0.0')
    count = 0
    balance_count = BigDecimal.new('0.0')

    candidate.candidate_question_answers.each do |candidate_answer|
      answer = answers[candidate_answer.question_id]

      if answer && answer['answer']
         distance_sum += (answer['answer'] - (candidate_answer.answer || 0)).abs * (answer['balance'] || 1)
         balance_count +=  answer['balance'] || 1
         count += 1
      end
    end

    if count == 0
      0
    else
      (100.0 - (distance_sum / (count * (balance_count / count)))).round
    end
  rescue => e
    logger.error "#{e.message}\n#{e.backtrace[0..7].join("\n  ")}"
    ''
  end
end
