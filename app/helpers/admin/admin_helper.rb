module Admin::AdminHelper

  # Localized time. Returns empty string when time is not present
  def lt(time)
    if time
      l(time, format: '%Y-%m-%d %H:%M')
    end
  end

  # Render "dt" based row
  def list_row(row_label, content, opt = {})
    "#{content_tag(:dt, row_label, opt.fetch(:label_html, {}))}#{content_tag(:dd, content.presence || '&nbsp;'.html_safe, opt.fetch(:content_html, {}))}".html_safe
  end

  def location_data(remote_ip)
    @location_data ||= {}
    # @location_data['remote_ip'] ||= begin
    #   location = GeoipLocation.find_by_ip(remote_ip)
    #   if location.try(:found?)
    #     {
    #       country_code: location.country.iso_code,
    #       country_name: location.country.name,
    #       subdivision_code: location.subdivisions.first.try(:iso_code),
    #       subdivision_name: location.subdivisions.first.try(:name),
    #       city: location.city.name,
    #       latitude: location.location.latitude,
    #       longitude: location.location.longitude
    #     }.reject { |_, v| v.blank?}
    #   end
    # end
  rescue => e
    logger.error "#{e.message}\n#{e.backtrace[0..7].join("\n  ")}"
    nil
  end
end
