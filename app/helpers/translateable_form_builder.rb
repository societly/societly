class TranslateableFormBuilder < ActionView::Helpers::FormBuilder
  
  def translatable_text_field(field, options = {})
    @template.account_languages.inject('') do |content, language|
      content += @template.content_tag :div, class: 'input-group' do
        placeholder = [options[:placeholder], language.name].join(' &ndash; ').html_safe if options.key?(:placeholder) && language.code.upcase == 'EN'
        
        @template.concat(@template.text_field_tag("#{@object_name}[#{field}][#{language.code}]", @object.translation_for(field, language), options.merge(placeholder: placeholder)))
        @template.concat(@template.content_tag(:div, language.code, title: language.name, class: 'input-group-addon'))
      end
    end.html_safe
  end

  def translatable_serialized_text_field(field, options = {})
    @template.account_languages.inject('') do |content, language|
      content += @template.content_tag :div, class: 'input-group' do
        placeholder = [options[:placeholder], language.name].join(' &ndash; ').html_safe if options.key?(:placeholder)
        prefix = "[#{options.delete(:for)}]" if options[:for].present?

        @template.concat(@template.text_field_tag("#{@object_name}#{prefix}[#{field}][#{language.code}]", @object.option_value_for(field, language), options.merge(placeholder: placeholder)))
        @template.concat(@template.content_tag(:div, language.code, title: language.name, class: 'input-group-addon'))
      end
    end.html_safe
  end
  
  def translatable_text_area(field, options = {})
    @template.account_languages.inject('') do |content, language|
      content += @template.content_tag :div, class: 'input-group' do
        placeholder = [options[:placeholder], language.name].join(' &ndash; ').html_safe if options.key?(:placeholder) && language.code.upcase == 'EN'
        
        @template.concat(@template.text_area_tag("#{@object_name}[#{field}][#{language.code}]", @object.translation_for_with_default(field, language, ''), options.merge(placeholder: placeholder)))
        @template.concat(@template.content_tag(:div, language.code, title: language.name, class: 'input-group-addon'))
      end
    end.html_safe
  end
end
