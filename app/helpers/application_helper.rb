module ApplicationHelper
  # Get translation for account
  def account_t(key, fallback=nil)
    t = account_translation_keys[key]
    t ||= fallback if fallback.present?
    t.to_s.html_safe
  end

  # Load account translation for current language
  def account_translation_keys
    @account_translation_keys ||= Account::TRANSLATABLE_KEYS.each_with_object(HashWithIndifferentAccess.new) do |key, h|
      h[key] = @account.translation_for_with_default(key, @language).to_s
    end
  end

  def menuitems
    @account.pages.where(hidden: false, language_id: @language.id).order(:position).select(:name, :path, :kind).map do |page|
      {
        name: page.name,
        url: (page.link? ? page.path : page_path(@account.code, (fetch_publisher_code.presence || :default), @language.code, page.path)),
        active: (page.path == params[:id])
      }
    end
  end
end
