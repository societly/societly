class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  after_filter :allow_iframe

  private

  def fetch_publisher_code
    params.fetch(:publisher, '')
  end
  helper_method :fetch_publisher_code

  def fetch_language_code
    params.fetch(:language)
  end

  def select_account
    @account = Account.find_by(code: params.fetch(:account, :default))
  end

  def allow_iframe
    response.headers.delete('X-Frame-Options')
  end

  def set_public_cache
    expires_in 15.minutes, public: true if Rails.env.production?
  end
end
