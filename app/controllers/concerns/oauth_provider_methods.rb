require 'ostruct'

# Adds methods for verifying and getting user data by access token (e.g. provided by FB SDK login).
module OauthProviderMethods

  extend ActiveSupport::Concern

  protected

  def authentication_by_token(provider, access_token)
    case provider
    when 'facebook'
      token = ::OAuth2::AccessToken.new(fb_client, access_token)
      user_info = ActiveSupport::JSON.decode(token.get('/me', params: {fields: 'name,email'}).body)

      OpenStruct.new(provider: provider, uid: user_info['id'], info: OpenStruct.new(user_info)) if user_info['id'].present?
    when 'google'
      token = if access_token.starts_with?('ya29')
        # Handle signed (by iOS) access_token
        if verify_google_token(access_token)
          ::OAuth2::AccessToken.from_hash(google_client, access_token: access_token)
        else
          nil
        end
      else
        options = params[:redirect_uri].present? ? {redirect_uri: params[:redirect_uri]} : {}
        google_client.auth_code.get_token(access_token, options)
      end

      if token.present?
        user_info = token.get('https://www.googleapis.com/plus/v1/people/me/openIdConnect').parsed

        uid = user_info['sub'].presence || (user_info['email'] if user_info['email_verified'])
        OpenStruct.new(provider: provider, uid: uid, info: OpenStruct.new(user_info)) if uid.present?
      end
    end
  end

  def fb_client
    ::OAuth2::Client.new(
      Rails.application.config.app.oauth.facebook.key,
      Rails.application.config.app.oauth.facebook.secret,
      site: 'https://graph.facebook.com/v2.8',
      authorize_url: 'https://www.facebook.com/v2.8/dialog/oauth',
      token_url: 'oauth/access_token'
    )
  end

  def google_client
    ::OAuth2::Client.new(
      Rails.application.config.app.oauth.google.key,
      Rails.application.config.app.oauth.google.secret,
      site: 'https://accounts.google.com',
      authorize_url: '/o/oauth2/auth',
      token_url: '/o/oauth2/token'
    )
  end

  def verify_google_token(access_token)
    raw_response = google_client.request(:get, 'https://www.googleapis.com/oauth2/v3/tokeninfo', params: {access_token: access_token}).parsed
    Rails.application.config.app.oauth.google.keys.include?(raw_response['aud'])
  end
end
