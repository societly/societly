module PaginateWithLinks

  extend ActiveSupport::Concern

  protected

  # Normalize pagination parameters. Default is 50 and maximum is 250 per page.
  def paginate_params
    {
      per_page: [1, [per_page, max_per_page].compact.min].max,
      page: [params[:page].presence.to_i, 1].max
    }
  end

  # Apply pagination to resource and add pagination links to Link header. Options:
  #  :total_entries - allow to override will_paginate default total_entries query
  def paginate_with_links(resource, options = {})
    resource = resource.paginate(paginate_params.merge(options.slice(:total_entries)))
    set_header_pagination(resource)
    resource
  end

  # Set header pagination for resource
  def set_header_pagination(resource)
    set_header_pagination_link(:first, 1)
    set_header_pagination_link(:prev, resource.previous_page) if resource.previous_page
    set_header_pagination_link(:next, resource.current_page + 1) if resource.next_page
    set_header_pagination_link(:last, resource.total_pages) if resource.total_pages

    # Provide metadata about number of pages available
    response.headers['X-Total-Pages'] = resource.total_pages.to_s if resource.total_pages
    response.headers['X-Total-Entries'] = resource.total_entries.to_s if resource.total_entries
  end

  # Set header web "Link" (http://tools.ietf.org/html/rfc5988) values e.g.:
  #   Link: <https://edicy.test/admin/api/pages?page=2>; rel="next"
  def set_header_pagination_link(rel, page)
    set_header_link(url_for(request.params.merge(page: page)), rel: rel)
  end

  def per_page_default
    @per_page_default ||= 50
  end

  def per_page
    @per_page ||= (params[:per_page].presence || per_page_default).to_i
  end

  def max_per_page
    @max_per_page ||= 250
  end

  private

  # Add a new Link header to response headers. Requires
  # a URL and a params hash. Does not escape or
  # sanitize anything. Manual added Link headers will be
  # overridden.
  # Idea from: https://github.com/jgraichen/rack-link_headers
  def set_header_link(url, otions = {})
    header_links << {url: url.to_s, otions: otions}

    response['Link'] = header_links.to_a.map do |link|
      "<#{link[:url]}>" + link[:otions].keys.sort.map do |k|
        "; #{k}=\"#{link[:otions][k]}\""
      end.join
    end.join(', ')
  end

  def header_links
    @_header_links ||= []
  end
end
