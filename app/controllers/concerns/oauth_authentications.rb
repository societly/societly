# Adds methods for creating and initializing user using omniauth information.
module OauthAuthentications

  extend ActiveSupport::Concern

  protected

  def find_or_initialize_authorization(auth)
    Authentication.find_or_initialize_by(account_id: @account.id, provider: auth.provider, uid: auth.uid)
  end

  def handle_oauth!(account, authentication, auth)
    if current_account_customer.present?
      if authentication.new_record? || authentication.customer.blank?
        # The identity is new and is not associated with the current_customer so lets associate the identity.
        authentication.customer = current_account_customer
        authentication.save
      else
        logger.debug 'Already linked that account!'
      end

      current_account_customer
    else
      if authentication.new_record? || authentication.customer.blank?
        # No customer associated with the identity so we need to create a new one
        begin
          Authentication.transaction do
            authentication.customer = account.customers.build(
              email: auth.info.email, full_name: auth.info.name, password: Devise.friendly_token[0, 20]
            )
            if authentication.customer.save! && authentication.save!
              sign_in(authentication.customer)
              authentication.customer
            end
          end
        rescue Exception => e
          ActiveSupport::Notifications.instrument('oauth.exception.societly', exception: e, log_level: :error)
          nil
        end
      else
        # The identity we found had a customer associated with it so let's just log them in here
        sign_in(authentication.customer)
        authentication.customer
      end
    end
  end
end
