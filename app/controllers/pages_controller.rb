class PagesController < ApplicationController

  before_action :set_public_cache
  before_action :select_account

  def show
    @language = @account.languages.where(code: fetch_language_code).first
    @page = @account.pages.joins(:language).where(languages: {code: params[:language]}).find_by!(path: params[:id])
    @languages = @account.languages
    @publisher = fetch_publisher_code
  end

  private

  def fetch_publisher_code
    params.fetch(:publisher, '')
  end
  helper_method :fetch_publisher_code

  def select_account
    @account = Account.find_by(code: params.fetch(:account, :default))
  end
end
