class AuthenticationsController < ApplicationController

  include OauthAuthentications

  before_action :select_account

  skip_before_action :verify_authenticity_token

  def create
    if @account
      auth = request.env['omniauth.auth']
      authentication = find_or_initialize_authorization(auth)
      handle_oauth!(@account, authentication, auth)

      if authentication.customer.try(:persisted?) && authentication.customer.errors.blank?
        render json: customer_json(authentication.customer)
      else
        render json: {errors: authentication.customer.try(:errors) }, status: :unprocessable_entity
      end
    else
      render json: {message: 'Account not found'}, status: :unprocessable_entity
    end
  end

  def failure
    render json: {error: failure_message}, status: :unprocessable_entity
  end

  private

  def failure_message
    exception = request.respond_to?(:get_header) ? request.get_header('omniauth.error') : request.env['omniauth.error']
    error = exception.error_reason if exception.respond_to?(:error_reason)
    error ||= exception.error if exception.respond_to?(:error)
    error ||= (request.respond_to?(:get_header) ? request.get_header('omniauth.error.type') : request.env['omniauth.error.type']).to_s
    error.to_s.humanize if error
  end

  def customer_json(customer)
    customer.serializable_hash(only: %i(email full_name zip gender year_of_birth ethnicity education private)).tap do |json|
      json[:image] = (customer.public_url if customer.image.present?)
    end
  end
end
