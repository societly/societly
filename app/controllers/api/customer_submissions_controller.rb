class Api::CustomerSubmissionsController < Api::ApiController
  respond_to :json

  before_filter :ensure_customer_and_account!

  def index
    submissions = current_account_customer.submissions.select(:title, :client_hash, :created_at).order('created_at DESC')
    render json: submissions.map { |submission| submission_json(submission) }
  end

  def update
    submission = current_account_customer.submissions.find_by!(client_hash: params[:id])

    if submission.update_attributes(submission_attributes)
      render json: submission_json(submission)
    else
      render_error_response 'An error occurred when updating this submission. Please review the fields.', submission.errors
    end
  end

  private

  def submission_attributes
    params.required(:submission).permit(:title)
  end

  def submission_json(submission)
    submission.serializable_hash(only: %i(title client_hash))
  end
end
