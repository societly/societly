class Api::PasswordsController < Api::ApiController

  wrap_parameters Customer, include: Customer.attribute_names + %i(email reset_password_token password password_confirmation)

  def create
    customer = Customer.send_reset_password_instructions(password_reset_params)

    if customer.errors.empty?
      render json: {message: "Password reset link has been sent to #{customer.email}." }
    else
      render_error_response 'An error occurred when requesting password reset. Please review the fields.', email: ['not found']
    end
  end

  def update
    customer = Customer.reset_password_by_token(password_update_params)

    if customer.errors.empty?
      sign_in(customer)
      render json: customer_json(customer)
    else
      render_error_response "Can't reset password. Please review the fields.", customer.errors
    end
  end

  private

  def password_reset_params
    params.required(:customer).permit(:email).tap do |h|
      h[:account_id] = @account.id
    end
  end

  def password_update_params
    params.required(:customer).permit(:email, :reset_password_token, :password, :password_confirmation).tap do |h|
      h[:reset_password_token] = params[:id]
      h[:account_id] = @account.id
    end
  end

  def customer_json(customer)
    customer.serializable_hash(only: %i(email full_name private zip gender year_of_birth ethnicity education)).tap do |json|
      json[:image] = (customer.public_url if customer.image.present?)
    end
  end
end
