class Api::StatesController < Api::ApiController

  before_action :set_public_cache

  def index
    states = @account.states.includes(:translations).sorted
    states = states.where(id: @account.used_state_ids) unless params.key?(:all)
    render json: paginate_with_links(states).map { |state| state_json(state) }
  end

  private

  def state_json(state)
    state.serializable_hash(only: %i(id code)).merge(
      name: state.translation_for_with_default(:name, @language)
    )
  end
end
