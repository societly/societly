class Api::CandidatesController < Api::ApiController

  before_action :set_public_cache

  def index
    if comparision_account.blank?
      candidates = candidates_scope(@account.id)
      candidates = candidates.where(state_id: params[:state_id]) if params[:state_id].present?
      candidates = candidates.where(district_id: params[:district_id]) if params[:district_id].present?
    else
      candidates = candidates_scope(comparable_account_ids)
    end
    render json: candidates.map { |candidate| candidate_json(candidate) }
  end

  private

  def candidate_json(candidate)
    candidate.serializable_hash(only: %i(id params state_id district_id account_id), methods: %i(level)).merge(
      name: candidate.translation_for_with_default(:name, @language),
      description: candidate.translation_for_with_default(:description, @language),
      party: candidate.translation_for_with_default(:party, @language),
      image: (candidate.public_url if candidate.image),
      account: {
        code: candidate.account.code,
        name: candidate.account.name
      }
    ).tap do |json|
      json[:answers] = candidate.candidate_question_answers.map { |answer| answer_json(answer) }
    end
  end

  def answer_json(answer)
    answer.serializable_hash(only: %i(question_id answer source source_desc source_link)).tap do |a|
      a[:question_id] = question_id_map[a['question_id'].to_s] if comparision_account.present?
    end
  end

  def candidates_scope(account_ids)
    scope = Candidate.where(account_id: account_ids).includes(
      {account: :translations}, :translations, :candidate_question_answers
    ).order(:id)
    scope = scope.by_level(params[:level]) if %w(country state district).include?(params[:level])
    scope
  end

  def comparision_account
    return false if params[:comparision].blank?
    @comparision_account ||=
      Account.includes(accounts: :questions)
             .where(is_comparative: true)
             .find_by(code: params[:comparision])
  end

  def comparable_account_ids
    @comparable_account_ids ||= comparision_account.accounts.map(&:id)
  end

  def question_id_map_by_position
    @question_id_map_by_position ||=
      @account.questions.each_with_object(HashWithIndifferentAccess.new) do |question, memo|
        memo[question.position] = question.id
      end
  end

  def question_id_map
    @question_id_map ||= begin
      questions = comparision_account.accounts.map(&:questions).flatten
      questions.each_with_object(HashWithIndifferentAccess.new) do |q, memo|
        memo[q.id.to_s] = question_id_map_by_position[q.position]
      end
    end
  end
end
