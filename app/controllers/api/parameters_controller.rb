class Api::ParametersController < Api::ApiController

  before_action :set_public_cache

  def index
    parameters = @account.parameters.order(:id)
    render json: paginate_with_links(parameters).map { |parameter| parameter_json(parameter) }
  end

  private

  def parameter_json(parameter)
    {
      id: parameter.id,
      name: parameter.translation_for_with_default(:name, @language)
    }
  end
end
