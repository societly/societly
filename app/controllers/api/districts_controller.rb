class Api::DistrictsController < Api::ApiController

  before_action :set_public_cache
  before_action :load_state

  def index
    districts = @state.present? ? @state.districts : @account.districts
    districts = districts.includes(:translations).order(:code)
    districts = districts.where(id: @account.used_district_ids) unless params.key?(:all)
    render json: paginate_with_links(districts).map { |district| district_json(district) }
  end

  private

  def district_json(district)
    district.serializable_hash(only: %i(id state_id code)).merge(
      name: district.translation_for_with_default(:name, @language)
    )
  end

  def load_state
    @state = @account.states.find(params[:state_id]) if params[:state_id].present?
  end
end
