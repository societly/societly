class Api::ApiController < ApplicationController
  include PaginateWithLinks

  rescue_from ActionController::ParameterMissing, with: :render_bad_request
  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found

  before_action :select_account
  before_action :load_language

  skip_before_action :verify_authenticity_token

  respond_to :json

  private

  def render_not_found
    render json: {message: message_for_not_found}, status: :not_found
  end

  def render_bad_request
    render json: {message: "It's not valid request."}, status: :bad_request
  end

  def render_error_response(message, errors = {}, status = :unprocessable_entity)
    render json: {message: message, errors: errors}, status: status
  end

  def select_account
    @account = Account.find_by!(code: params.fetch(:account, :default))
  end

  def load_language
    @language = @account.languages.find_by_code!(params[:language])
  end

  def message_for_not_found
    if @account
      if @language
        'Not found.'
      else
        'Language is not found.'
      end
    else
      'Account is not found.'
    end
  end

  def ensure_customer_and_account!
    render_not_found unless current_account_customer && current_account_customer.account_id == @account.id
  end
end
