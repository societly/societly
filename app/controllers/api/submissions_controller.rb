class Api::SubmissionsController < Api::ApiController
  respond_to :json
  skip_before_action :load_language, only: [:show, :update, :left]

  def show
    submission = @account.submissions.find_by!(client_hash: params[:id])
    render json: submission_json(submission)
  end

  def create
    submission = @account.submissions.build(submission_attributes)
    submission.customer = current_account_customer if current_account_customer && current_account_customer.account_id == @account.id

    if submission.save
      render json: submission_create_json(submission), status: :created
    else
      render_error_response 'An error occurred when creating this submission. Please review the fields.', submission.errors
    end
  end

  def update
    submission = @account.submissions.find_by!(client_hash: params[:id])

    # When current customer is present and confirm_hash is valid (and not expired) then add current_customer to current submission.
    if submission.customer_id.blank? && current_account_customer && current_account_customer.account_id == @account.id
      message = begin
        verifier.verify(params[:confirm_hash]) if params[:confirm_hash].present?
      rescue => e
        logger.debug e.inspect
        nil
      end

      if message && message['id'] == submission.id && message['expires'] >= Time.now
        submission.update_attribute(:customer_id, current_account_customer.id)
      end
    end

    render json: submission_customer_json(submission)
  end

  def left
    submission = @account.submissions.find_by(client_hash: params[:id])
    submission.touch(:left_page_at) if submission && submission.left_page_at.blank?

    render json: {}
  end

  private

  def submission_attributes
    params.required(:submission).permit(
      :publisher, :seconds_to_result, :start_to_result, :full_name, :email, :zip, :facebook, :twitter,
      :gender, :year_of_birth, :ethnicity, :education, :phone, :state_id, :district_id, :external_ref,
      :comparision_account
    ).tap do |whitelisted|
      whitelisted[:language_code] = params.fetch(:language)
      whitelisted[:answers] = params[:submission][:answers]
      whitelisted[:extra_questions] = params[:submission][:extra_questions]
      whitelisted[:open_page_at] = begin DateTime.parse(params[:submission][:open_page_at]) rescue nil; end
      whitelisted[:comparision_account] = Account.find_by(code: params.fetch(:comparision_account))
    end.merge(remote_ip: request.remote_ip, user_agent: request.env['HTTP_USER_AGENT'])
  end

  def submission_json(submission)
    submission.serializable_hash(
      only: %i(state_id district_id answers extra_questions external_ref account_id comparision_account_id)
    ).merge(
      submission_customer_json(submission)
    )
  end

  def submission_customer_json(submission)
    if submission.customer && !submission.customer.private?
      {
        title: submission.title,
        customer: {
          full_name: submission.customer.full_name.presence,
          image: (submission.customer.public_url if submission.customer.image.present?)
        }
      }
    else
      {}
    end
  end

  def submission_create_json(submission)
    {client_hash: submission.client_hash}.tap do |json|
      # Add confirm_hash if submission hasn't customer object
      json[:confirm_hash] = verifier.generate(id: submission.id, expires: Time.now + 1.day) if submission.customer.blank?
    end.merge(
      submission_customer_json(submission)
    )
  end

  # Message verifier.
  def verifier
    @verifier ||= ActiveSupport::MessageVerifier.new(Rails.application.secrets.secret_key_base, digest: 'SHA256', serializer: JSON)
  end
end
