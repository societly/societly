class Api::QuestionsController < Api::ApiController

  before_action :set_public_cache

  def index
    questions = @account.questions.sorted
    render json: paginate_with_links(questions).map { |question| question_json(question) }
  end

  private

  def question_json(question)
    question.serializable_hash(only: %i(id params position)).merge(
      name: question.translation_for_with_default(:name, @language),
      description: question.translation_for_with_default(:description, @language),
    )
  end
end
