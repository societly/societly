class Api::CustomersController < Api::ApiController

  include OauthAuthentications
  include OauthProviderMethods

  wrap_parameters Customer, include: Customer.attribute_names + %i(password password_confirmation current_password)

  def current
    if current_account_customer && current_account_customer.account_id == @account.id
      render json: customer_json(current_account_customer)
    else
      render_not_found
    end
  end

  def create
    if params[:access_token].present?
      token_login
    else
      customer = @account.customers.build(customer_attributes)

      if customer.save
        sign_in(customer)
        render json: customer_json(current_account_customer)
      else
        # Log in if user exists and correct password has been provided
        if customer.email.present? && customer.errors[:email].present? && customer.errors[:password].blank? && (existing_customer = @account.customers.find_by_email(customer.email)) && existing_customer.valid_password?(customer_attributes[:password])
          sign_in(existing_customer)
          render json: customer_json(current_account_customer)
        else
          render_error_response 'An error occurred when creating this customer. Please review the fields.', customer.errors
        end
      end
    end
  end

  def update
    if current_account_customer
      if current_account_customer.account_id == @account.id
        if %i(password password_confirmation current_password).any? { |k| customer_attributes[k].present? }
          current_account_customer.update_with_password(customer_attributes)
        else
          current_account_customer.update_without_password(customer_attributes)
        end

        if current_account_customer.errors.blank?
          add_uploaded_asset(current_account_customer)
          render json: customer_json(current_account_customer)
        else
          render_error_response 'An error occurred when updating this customer. Please review the fields.', current_account_customer.errors
        end
      else
        # TODO: unauthorized wrong account
        render_bad_request
      end
    else
      render_not_found
    end
  end

  def login
    if current_account_customer.blank? || current_account_customer.account_id != @account.id
      if params[:access_token].present?
        token_login
      else
        customer = Customer.find_for_database_authentication(email: customer_attributes[:email], account_id: @account.id)
        if customer
          if customer.valid_password?(login_attributes[:password])
            sign_in(customer)
            render json: customer_json(current_account_customer)
          else
            render_error_response 'Login failed.', customer.errors
          end
        else
          render_not_found
        end
      end
    else
      render json: customer_json(current_account_customer)
    end
  end

  def logout
    sign_out(current_account_customer) if current_account_customer
    render json: {success: true}
  end

  private

  def token_login
    if params[:provider].present? && Rails.application.config.app.oauth.enabled.include?(params[:provider].to_sym)
      errors = {}

      auth = begin
        authentication_by_token(params[:provider], params[:access_token])
      rescue OAuth2::Error => e
        errors[:oauth] = "#{[e.code, e.description].compact.join(' - ')}" if e.respond_to?(:code) && e.respond_to?(:description)
        ActiveSupport::Notifications.instrument('oauthtoken.exception.societly', exception: e, log_level: :error)
        nil
      rescue Exception => e
        ActiveSupport::Notifications.instrument('oauthtoken.exception.societly', exception: e, log_level: :error)
        nil
      end

      if auth.present?
        authentication = find_or_initialize_authorization(auth)
        handle_oauth!(@account, authentication, auth)
        if authentication.customer.try(:persisted?) && authentication.customer.errors.blank?
          render json: customer_json(authentication.customer)
        else
          render json: {errors: authentication.customer.try(:errors) }, status: :unprocessable_entity
        end
      else
        render_error_response 'An error occurred when creating/login this customer by token. Authentication failed.', errors
      end

    else
      render_error_response 'An error occurred when creating/login this customer by token. Please review the fields.', {provider: "Provider '#{params[:provider]} is unknown.'"}
    end
  end

  def customer_json(customer)
    customer.serializable_hash(only: %i(email full_name private zip gender year_of_birth ethnicity education)).tap do |json|
      json[:image] = (customer.public_url if customer.image.present?)
    end
  end

  def customer_attributes
    params.required(:customer).permit(
      :email, :full_name, :private, :zip, :gender, :year_of_birth, :ethnicity, :education, :password, :password_confirmation, :current_password
    )
  end

  def login_attributes
    params.required(:customer).permit(:email, :password)
  end

  # Upload and save new image if provided
  def add_uploaded_asset(customer)
    if params[:customer][:image] && params[:customer][:image].is_a?(ActionDispatch::Http::UploadedFile)
      customer.upload_image!(params[:customer][:image])
    end
  end
end
