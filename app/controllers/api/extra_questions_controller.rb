class Api::ExtraQuestionsController < Api::ApiController

  before_action :set_public_cache

  def index
    questions = @account.extra_questions.sorted
    render json: paginate_with_links(questions).map { |question| extra_questions_json(question) }
  end

  private

  def extra_questions_json(extra_question)
    extra_question.serializable_hash(only: %i(id params position)).merge(
      name: extra_question.translation_for_with_default(:name, @language),
      description: extra_question.translation_for_with_default(:description, @language),
    ).tap do |h|
      h[:choices] = extra_question.options.map do |k, v|
        {
          value: k,
          label: extra_question.option_value_for(k, @language)
        }
      end
    end
  end
end
