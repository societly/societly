class HomeController < ApplicationController

  before_action :set_public_cache, except: [:ping]
  before_action :select_account, except: [:root, :ping]

  skip_before_action :verify_authenticity_token, only: :submission

  def root
    account = Account.default.first || Account.first

    if account
      language = account.languages.first
      if language
        redirect_params = {account: account.code, publisher: :default, language: language.code}
        # Workaround for CloudFront 301 redirect
        redirect_params[:host] = cdn_host if deployed_to_cloudfront?
        redirect_to account_home_url(redirect_params)
      else
        render nothing: true
      end
    else
      render nothing: true
    end
  end

  def index
    @language = @account.languages.where(code: fetch_language_code).first || @account.default_language
    @languages = @account.languages
    @questions = @account.questions.includes(:translations).sorted
    @extra_questions = @account.extra_questions.includes(:translations).sorted.to_a
    @parameters = @account.parameters.includes(:translations)
    @candidates = @account.candidates.includes(:translations, :candidate_question_answers)
    @publisher = fetch_publisher_code
    @all_states = @account.states.includes(:translations).sorted.to_a
    @states = @account.states.where(id: @account.used_state_ids).includes(:translations).sorted.to_a if @all_states.present?
    @comparision_account = comparision_account
    @sub_account = @account.default_subaccount
    @partners_url = [
      'https://www.politico.eu/',
      'https://www.eunews.it/',
      'https://www.ilsole24ore.com/',
      'https://www.agerpres.ro/',
      'http://activenews.gr/',
      'https://www.bruxinfo.hu/',
      'https://www.postimees.ee/',
      'https://www.goethe.de/ins/ee/de/index.html',
      'https://www.publico.pt/',
      'https://www.timesofmalta.com/',
      'https://www.btv.bg/',
      'http://new.aej-bulgaria.org/',
      'https://www.irozhlas.cz/',
      'https://ihned.cz/',
      'https://www.tsf.pt'
    ]
  end

  def ping
    Account.first
    render plain: 'OK'
  end

  private

  def comparision_account
    code = params.fetch(:comparsion_account, nil)
    code.present? ? Account.find_by(code: code) : nil
  end

  def deployed_to_cloudfront?
    Rails.application.config.app.cdn_host.present? && request.headers['HTTP_USER_AGENT'] == 'Amazon CloudFront'
  end

  def cdn_host
    Rails.application.config.app.cdn_host
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end
end
