class Customers::PasswordsController < Devise::PasswordsController
  before_action :select_account
  before_action :load_language

  def new
    super
    resource.account_id = @account.id
  end

  protected

  def load_language
    @language = Language.where(account_id: @account.try(:id)).find_by_code!(params[:language])
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    account_home_path(@account.code, 'default', @language.code) if is_navigational_format?
  end
end
