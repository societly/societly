class Admin::ParametersController < Admin::AdminController

  before_action :require_language

  def index
    @parameters = current_account.parameters
  end
  
  def new
    @parameter = current_account.parameters.build
  end
  
  def create
    @parameter = current_account.parameters.build
    @parameter.attributes = parameter_params
    
    if @parameter.save
      redirect_to admin_parameters_path, notice: 'Parameter has been created'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'new'
    end
  end
  
  def edit
    @parameter = current_account.parameters.find(params[:id])
  end
  
  def update
    @parameter = current_account.parameters.find(params[:id])
    
    if @parameter.update_attributes(parameter_params)
      redirect_to admin_parameters_path, notice: 'Parameter has been updated'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end
  
  def destroy
    current_account.parameters.find(params[:id]).destroy
    redirect_to admin_parameters_path, notice: 'Parameter has been deleted'
  end
  
  private
  
  def parameter_params
    params.required(:parameter).permit(:name).tap do |whitelist|
      whitelist[:name] = params[:parameter][:name]
    end
  end
end
