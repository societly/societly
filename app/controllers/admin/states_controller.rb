class Admin::StatesController < Admin::AdminController

  before_action :require_language

  def index
    @states = current_account.states.order(:name)
  end
  
  def new
    @state = current_account.states.build
  end
  
  def create
    @state = current_account.states.build
    @state.attributes = state_params
    
    if @state.save
      redirect_to admin_states_path, notice: 'State has been created'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'new'
    end
  end
  
  def edit
    @state = current_account.states.find(params[:id])
  end
  
  def update
    @state = current_account.states.find(params[:id])
    
    if @state.update_attributes(state_params)
      redirect_to admin_states_path, notice: 'State has been updated'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end
  
  def destroy
    current_account.states.find(params[:id]).destroy
    redirect_to admin_states_path, notice: 'State has been deleted'
  end
  
  private
  
  def state_params
    params.required(:state).permit(:code, :name).tap do |whitelist|
      whitelist[:name] = params[:state][:name]
    end
  end
end
