require 'csv'

class Admin::SubmissionsController < Admin::AdminController

  before_action :require_language

  def index
    @submissions = current_account.submissions.includes(:state, :district).order('id DESC')

    respond_to do |format|
      format.html do
        @submissions = @submissions.paginate(page: params[:page])
      end
      format.csv do
        # TODO: Improve performance
        @include_candidates = params.key?(:include_candidates)
        @questions = current_account.questions.sorted
        @extra_questions = current_account.extra_questions.sorted
        @candidates = current_account.candidates.by_level('country').includes(:candidate_question_answers).order(:name) if @include_candidates
        @language = current_account.languages.default_language.first
        headers['Content-Disposition'] = "attachment; filename=\"societly-#{current_account.name.downcase}-submissions-#{Time.now.strftime("%Y%m%d%H%M")}.csv\""
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end

  def show
    @submission = current_account.submissions.find(params[:id])
    @questions_hash = current_account.questions.each_with_object({}) { |q, h| h[q.id] = q }
  end
end
