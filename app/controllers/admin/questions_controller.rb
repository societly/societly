class Admin::QuestionsController < Admin::AdminController
  
  # TODO: Question ordering by position

  before_action :require_language
  
  def index
    @questions = current_account.questions.sorted
  end
  
  def new
    @question = current_account.questions.build
    @parameters = current_account.parameters
  end
  
  def create
    @question = current_account.questions.build
    @question.attributes = question_params
    @parameters = current_account.parameters
    
    if @question.save
      redirect_to admin_questions_path, notice: 'Question has been created'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'new'
    end
  end
  
  def edit
    @question = current_account.questions.find(params[:id])
    @parameters = current_account.parameters
  end
  
  def update
    @question = current_account.questions.find(params[:id])
    @parameters = current_account.parameters
    
    if @question.update_attributes(question_params)
      redirect_to admin_questions_path, notice: 'Question has been updated'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end
  
  def destroy
    current_account.questions.find(params[:id]).destroy
    redirect_to admin_questions_path, notice: 'Question has been deleted'
  end

  def move
    @question = current_account.questions.find(params[:id])
    params[:direction] == 'up' ? @question.move_higher : @question.move_lower

    @old_position, @new_position = @question.previous_changes['position']
  end

  private
  
  def question_params
    params.required(:question).permit(:name, :description, :params).tap do |w|
      w[:name] = params[:question][:name]
      w[:description] = params[:question][:description]
      w[:params] = params[:question][:params]
    end
  end
end
