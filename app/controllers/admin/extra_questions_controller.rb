class Admin::ExtraQuestionsController < Admin::AdminController

  before_action :require_language

  def index
    @extra_questions = current_account.extra_questions.sorted
  end

  def new
    @extra_question = current_account.extra_questions.build
  end

  def create
    @extra_question = current_account.extra_questions.build
    @extra_question.attributes = extra_question_params

    if @extra_question.save
      redirect_to admin_extra_questions_path, notice: 'Extra question has been created'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'new'
    end
  end

  def edit
    @extra_question = current_account.extra_questions.find(params[:id])
  end

  def update
    @extra_question = current_account.extra_questions.find(params[:id])

    if @extra_question.update_attributes(extra_question_params)
      redirect_to admin_extra_questions_path, notice: 'Extra question has been updated'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end

  def destroy
    current_account.extra_questions.find(params[:id]).destroy
    redirect_to admin_extra_questions_path, notice: 'Extra question has been deleted'
  end

  def move
    @extra_question = current_account.extra_questions.find(params[:id])
    params[:direction] == 'up' ? @extra_question.move_higher : @extra_question.move_lower

    @old_position, @new_position = @extra_question.previous_changes['position']
  end

  private

  def extra_question_params
    params.required(:extra_question).permit(:name, :options).tap do |w|
      w[:name] = params[:extra_question][:name]
      w[:options] = params[:extra_question][:options]
    end
  end
end
