class Admin::LanguagesController < Admin::AdminController
  
  def index
    @languages = current_account.languages
  end
  
  def new
    @language = current_account.languages.build
  end
  
  def create
    @language = current_account.languages.create(language_params)
    
    if @language.valid?
      redirect_to admin_languages_path, notice: 'Language has been created'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'new'
    end
  end
  
  def edit
    @language = current_account.languages.find(params[:id])
  end
  
  def update
    @language = current_account.languages.find(params[:id])
    
    if @language.update_attributes(language_params)
      redirect_to admin_languages_path, notice: 'Language has been updated'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end
  
  def set_default
    @language = current_account.languages.find(params[:id])
    @language.update_attribute(:default_language, true)
    
    redirect_to admin_languages_path, notice: "Language #{@language.name} has been set as default language"
  end
  
  def destroy
    current_account.languages.find(params[:id]).destroy
    redirect_to admin_languages_path, notice: 'Language has been deleted'
  end
  
  private
  
  def language_params
    params.required(:language).permit(:name, :code)
  end
end
