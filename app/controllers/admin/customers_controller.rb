require 'csv'

class Admin::CustomersController < Admin::AdminController

  before_action :require_language
  before_action :load_customer, only: [:show, :destroy]

  def index
    @customers = current_account.customers.order('id DESC')
    respond_to do |format|
      format.html do
        @customers = @customers.paginate(page: params[:page])
      end
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"societly-#{current_account.name.downcase}-customers-#{Time.now.strftime("%Y%m%d%H%M")}.csv\""
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end
  
  def new
    @customer = current_account.customers.build
  end
  
  def show
    @submissions = @customer.submissions.order('id DESC').to_a
  end
  
  def destroy
    @customer.destroy
    redirect_to admin_customers_path, notice: 'Customer has been deleted'
  end
  
  private

  def load_customer
    @customer = current_account.customers.find(params[:id])
  end
end
