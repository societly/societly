class Admin::UsersController < Admin::AdminController
  
  def index
    @users = User.all
  end

  def create
    @user = User.create(email: params.required(:user).fetch(:email), password: Devise.friendly_token.first(8))

    if @user.persisted?
      # Send password reset link to user
      @user.password_reset_by_admin = true
      @user.send_reset_password_instructions
      flash.notice = 'User has been created'
    else
      flash.alert = "Cant't add tis email"
    end

    redirect_to admin_users_path
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(user_params)
      redirect_to admin_users_path, notice: 'User has been updated'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    
    if @user.id == current_admin_user.id
      redirect_to admin_users_path, notice: 'Current user cannot be deleted'
    else
      @user.destroy
      redirect_to admin_users_path, notice: 'User has been deleted'
    end
  end

  private

  def user_params
    params.required(:user).permit(:password, :password_confirmation)
  end
end
