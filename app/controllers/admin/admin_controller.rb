class Admin::AdminController < ApplicationController

  before_action :authenticate_admin_user!, :load_accounts

  protected

  def current_account
    @current_account ||= load_account_from_session
  rescue ActiveRecord::RecordNotFound
    @current_account = nil
  end
  helper_method :current_account

  def account_languages
    @account_languages ||= current_account.languages
  end
  helper_method :account_languages

  private

  def load_accounts
    @accounts = Account.all.load
  end

  def load_account_from_session
    account = Account.find_by_id(session[:account_id]) || @accounts.detect { |a| a.default } || @accounts.first
    session[:account_id] = account.id if account && session[:account_id].to_i != account.id
    account
  end

  def require_language
    redirect_to new_admin_language_path, alert: 'Add at least one langue to current account to continue' if current_account && account_languages.blank?
  end
end
