class Admin::CandidatesController < Admin::AdminController

  # TODO: Calculate candidate params values based on answers

  before_action :require_language
  
  def index
    @candidates = current_account.candidates.order(:name, :id).paginate(page: params[:page], per_page: params.fetch(:per_page, 50))
    @candidates = @candidates.by_level(params[:level]) if %w(country state district).include?(params[:level])
    @questions = current_account.questions.sorted
    @parameters = current_account.parameters
  end
  
  def new
    @candidate = current_account.candidates.build
    @questions = current_account.questions.sorted
  end
  
  def create
    @candidate = current_account.candidates.build
    @candidate.attributes = candidate_params
    @questions = current_account.questions.sorted
    
    if @candidate.save
      add_uploaded_asset(@candidate)
      redirect_to admin_candidates_path, notice: 'Candidate has been created'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'new'
    end
  end
  
  def edit
    @candidate = current_account.candidates.find(params[:id])
    @questions = current_account.questions.sorted
  end
  
  def update
    @candidate = current_account.candidates.find(params[:id])
    @questions = current_account.questions.sorted
    
    if @candidate.update_attributes(candidate_params)
      add_uploaded_asset(@candidate)
      redirect_to admin_candidates_path, notice: 'Candidate has been updated'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end
  
  def destroy
    current_account.candidates.find(params[:id]).destroy
    redirect_to admin_candidates_path, notice: 'Candidate has been deleted'
  end
  
  private
  
  def candidate_params
    params.required(:candidate).permit(:name, :code, :description, :party, :question_answers, :state_id, :district_id).tap do |w|
      w[:name] = params[:candidate][:name]
      w[:description] = params[:candidate][:description]
      w[:party] = params[:candidate][:party]
      w[:question_answers] = params[:candidate][:question_answers]
    end
  end

  # Upload and save new image if provided
  def add_uploaded_asset(candidate)
    if params[:candidate][:image] && params[:candidate][:image].is_a?(ActionDispatch::Http::UploadedFile)
      candidate.upload_image!(params[:candidate][:image])
    end
  end
end
