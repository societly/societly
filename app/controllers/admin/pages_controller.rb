class Admin::PagesController < Admin::AdminController

  before_action :require_language

  def index
    load_languages
    @pages = current_account.pages.where(language_id: @language.try(:id)).order(:position)
  end

  def new
    @page = current_account.pages.build
    @page.language = current_account.languages.find_by_code(params[:language]) if params[:language].present?
  end

  def create
    @page = current_account.pages.create(page_params)

    if @page.valid?
      redirect_to admin_pages_path(language: @page.language.try(:code)), notice: 'Page has been created'
    else
      load_languages
      flash.now.alert = 'Please fill in required fields'
      render action: 'new'
    end
  end

  def edit
    @page = current_account.pages.find(params[:id])
  end

  def update
    @page = current_account.pages.find(params[:id])

    if @page.update_attributes(page_params)
      redirect_to admin_pages_path(language: @page.language.try(:code)), notice: 'Page has been updated'
    else
      load_languages
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end

  def destroy
    current_account.pages.find(params[:id]).destroy
    redirect_to admin_pages_path, notice: 'Page has been deleted'
  end

  def move
    @page = current_account.pages.joins(:language).where(languages: {code: params[:language]}).find(params[:id])
    params[:direction] == 'up' ? @page.move_higher : @page.move_lower

    @old_position, @new_position = @page.previous_changes['position']
  end

  private

  def page_params
    params.required(:page).permit(:language_id, :publisher, :kind, :name, :title, :description, :path, :content, :hidden)
  end

  def load_languages
    @languages = current_account.languages.order(:code).all
    @language = @languages.detect { |e| e.code == params[:language] } || @languages.first
  end
end
