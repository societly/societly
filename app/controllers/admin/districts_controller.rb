class Admin::DistrictsController < Admin::AdminController

  before_action :load_state
  
  def index
    @districts = @state.districts
  end
  
  def new
    @district = @state.districts.build
  end
  
  def create
    @district = @state.districts.build(account: current_account)
    @district.attributes = district_params
    
    if @district.save
      redirect_to admin_state_districts_path(@state), notice: 'District has been created'
    else
      logger.debug @district.errors.full_message.to_s
      flash.now.alert = 'Please fill in required fields'
      render action: 'new'
    end
  end
  
  def edit
    @district = @state.districts.find(params[:id])
  end
  
  def update
    @district = @state.districts.find(params[:id])
    
    if @district.update_attributes(district_params)
      redirect_to admin_state_districts_path(@state), notice: 'District has been updated'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end
  
  def destroy
    @state.districts.find(params[:id]).destroy
    redirect_to admin_state_districts_path(@state), notice: 'District has been deleted'
  end
  
  private
  
  def district_params
    params.required(:district).permit(:code, :name).tap do |whitelist|
      whitelist[:name] = params[:district][:name]
    end
  end

  def load_state
    @state = current_account.states.find(params[:state_id])
  end
end
