class Admin::AccountsController < Admin::AdminController

  # TODO: Account settings (greeting text team / methodology page contents / logo and background picture)

  def select
    session[:account_id] = Account.find(params[:id]).id
    redirect_to admin_root_path
  end

  def new
    @account = Account.new
  end

  def create
    @account = Account.create(account_params)

    if @account.valid?
      session[:account_id] = @account.id
      redirect_to admin_root_path, notice: 'Account has been created'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'new'
    end
  end

  def edit
    @account = current_account
  end

  def update
    @account = current_account

    if @account.update(account_params)
      redirect_to admin_root_path, notice: 'Account has been updated'
    else
      flash.now.alert = 'Please fill in required fields'
      render action: 'edit'
    end
  end

  def set_default
    @account = current_account
    @account.set_default!

    redirect_to admin_root_path, notice: "Account #{@account.name} has been set as default language"
  end

  private

  def account_params
    params.required(:account).permit(:name, :code, :requires_unique_ref, :is_comparative, { account_ids: [] }).tap do |e|
      Account::TRANSLATABLE_KEYS.each do |key|
        e[key] = params[:account][key]
      end
    end
  end
end
