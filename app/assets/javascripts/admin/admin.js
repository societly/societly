//= require jquery
//= require jquery_ujs
//= require bootstrap

document.addEventListener('DOMContentLoaded', function() {
  
  $('[data-behavior="toggle-question-details"]').on('click', function(event) {
    event.preventDefault();
    
    var questionId = $(event.target).closest('[data-question-id]').attr('data-question-id');
    
    $('[data-question-details][data-question-id="' + questionId + '"]').toggleClass('details-hidden');
  });
  
  $('[data-behavior="toggle-all-question-details"]').on('click', function(event) {
    event.preventDefault();
    
    $('[data-question-id]').removeClass('details-hidden');
  });
});
