class Page < ActiveRecord::Base

  include SoftDeleteable

  module Kind
    Page = 'page'
    Link = 'link'

    def self.values
      [Page, Link]
    end
  end

  belongs_to :account
  belongs_to :language

  acts_as_list scope: [:account_id, :language_id]

  before_validation :set_defaults

  validates :name, length: {maximum: 255}, presence: true
  validates :title, :description, length: {maximum: 255}
  validates :path, presence: true, uniqueness: {scope: [:account_id, :language_id, :deleted_at]}, format: {with: /\A[\w\d-]+\z/}, unless: :link?
  validates :path, format: {with: /\A((((https?\:\/\/))(([a-z0-9]([a-z0-9\-]{0,61}[a-z0-9])?\.)+([a-z]{2,61}|(xn--[a-z0-9\-]{0,61})))+)|(\/\/?))+([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp:\[\]|;=%\$#_])*\z/i}, if: :link?
  validates :kind, inclusion: {in: Kind.values}

  def link?
    kind == Kind::Link
  end

  private

  def set_defaults
    self.title = name if title.blank?
    unless link?
      self.path = path.blank? ? name.to_s.parameterize : path.downcase
    end
  end
end
