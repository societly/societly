class CandidateQuestionAnswer < ActiveRecord::Base
  
  include SoftDeleteable
  
  belongs_to :account
  belongs_to :candidate
  belongs_to :question

end
