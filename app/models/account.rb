class Account < ActiveRecord::Base
  # Available translatable keys
  TRANSLATABLE_KEYS = I18n.t('account.keys', locale: 'en').keys.freeze

  include SoftDeleteable

  validates :name, :code, presence: true

  has_many :customers, dependent: :destroy
  has_many :languages, dependent: :destroy
  has_many :pages, dependent: :destroy
  has_many :parameters, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :extra_questions, dependent: :destroy
  has_many :candidates, dependent: :destroy
  has_many :states, dependent: :destroy
  has_many :districts, dependent: :destroy
  has_many :translations, dependent: :destroy
  has_many :submissions
  has_many :translation_keys, as: :model, class_name: 'Translation'

  has_and_belongs_to_many(:accounts,
                          join_table: 'account_connections',
                          foreign_key: 'account_id',
                          association_foreign_key: 'connected_account_id')

  scope :default, -> { where(default: true) }

  # Setup setter methods for translatable keys
  TRANSLATABLE_KEYS.each do |key|
    define_method("#{key}=") { |translated_names| set_translations_for(key, translated_names, languages) }
  end

  def translation_for_with_default(field, language, default_value = nil)
    translation_keys.detect { |t| t.language_id == language.id && t.model_field == field.to_s }.try(:content).presence || default_value ||
      I18n.t(field, scope: 'account.keys', default: '')
  end
  alias_method :translation_for, :translation_for_with_default

  def set_translations_for(field, values, languages)
    return if languages.blank?
    values.each do |language_code, value|
      language = languages.find { |l| l.code == language_code }
      translation = translation_keys.find_or_initialize_by(account_id: id, model_field: field.to_s, language_id: language.id)
      translation.content = value
      translation.save if translation.persisted?
    end
  end

  def set_default!
    transaction do
      Account.where(default: true).where('id != ?', id).update_all(default: false)
      update_attribute(:default, true)
    end
  end

  def used_state_ids
    @used_state_ids ||= candidates.where.not(state_id: nil).pluck(:state_id)
  end

  def used_district_ids
    @used_district_ids ||= candidates.where.not(district_id: nil).pluck(:district_id)
  end

  # TODO: store it to account.
  def feature_customers?
    %w(us2016congress testing).include?(code)
  end

  # TODO: store it to account.
  def feature_userinfo_tab?
    code != 'nyc2017'
  end

  # TODO: make this dependant of GeoIP
  def default_subaccount
    is_comparative? ? accounts.order(:name).first : self
  end

  def default_language
    @default_language ||= languages.where(default_language: true).first
  end
end
