class Candidate < ActiveRecord::Base

  include Translateable
  include SoftDeleteable
  include Uploadable

  serialize :params, JSON

  belongs_to :account
  belongs_to :state
  belongs_to :district
  has_many :candidate_question_answers, dependent: :destroy

  validates :name, :account_id, presence: true

  after_validation :verify_district_value

  scope :by_level, ->(level) {
    case level
    when 'country'
      where(state_id: nil, district_id: nil)
    when 'state'
      where.not(state_id: nil).where(district_id: nil)
    when 'district'
      where.not(district_id: nil)
    end
  }

  def name=(translated_names)
    set_translations_for(:name, translated_names, account.languages)
  end

  def description=(translated_descriptions)
    set_translations_for(:description, translated_descriptions, account.languages)
  end

  def party=(translated_parties)
    set_translations_for(:party, translated_parties, account.languages)
  end

  def question_answers=(answers)
    answers.each do |question_id, answer|
      a = candidate_question_answers.find_or_initialize_by(question_id: question_id, account_id: self[:account_id])
      a.attributes = answer.permit(:answer, :source, :source_link, :source_desc)
      a.save
    end
  end

  def answer_for_question(question_id)
    candidate_question_answers.detect { |e| e.question_id == question_id } || candidate_question_answers.build(question_id: question_id, account_id: self[:account_id])
  end

  def resize_params
    {size: '600>', sharpen: '0x0.5', quality: 85}
  end

  # Upload file to S3
  def upload_image!(file)
    if file.respond_to?(:original_filename)
      old_name = image
      self.image = ActiveSupport::Multibyte::Unicode.normalize(filename_with_timestamp(file.original_filename))

      if file_is_image?
        store_file(file)
        remove_remote_file(old_name) if save!
      else
        errors.add(:image, :invalid)
      end
    end
  rescue Exception => e
    Rails.logger.error "Upload failed for Candidate##{id}: #{e.message.inspect}: \n#{e.backtrace[0..8].join("\n  ")}"
  end

  def remove_remote_file(file_name)
    delete_file!(file_name) if file_name.present?
  end

  def filename
    image
  end

  def folder
    'candidates'
  end

  def level_country?
    state_id.blank? && district_id.blank?
  end

  def level_state?
    state_id.present? && district_id.blank?
  end

  def level_district?
    state_id.present? && district_id.present?
  end

  # Returns the level of the candidate (country, state or district).
  def level
    if level_country?
      'country'
    elsif level_state?
      'state'
    else
      'district'
    end
  end

  private

  # Add timestamp to filename.
  def filename_with_timestamp(val)
    ext = File.extname(val)
    "#{File.basename(val, ext)}-#{Time.now.to_i}#{ext}".downcase
  end

  def verify_district_value
    self.district_id = nil if district_id.blank? || state_id.blank? || state.blank? || !state.districts.where(id: district_id).exists?
  end
end
