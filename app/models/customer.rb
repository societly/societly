class Customer < ActiveRecord::Base

  EMAIL_REGEX_PATTERN = /\A([^@\s]+)@([a-z0-9]([a-z0-9\-]{0,61}[a-z0-9])?\.)+([a-z]{2,61}|(xn--[a-z0-9\-]{0,61}))\z/i

  include SoftDeleteable
  include Uploadable

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, :rememberable
  devise :database_authenticatable, :registerable, :recoverable, :trackable#, :validatable

  belongs_to :account
  has_many :submissions
  has_many :authentications, dependent: :destroy

  validates :account_id, presence: true
  validates :full_name, :zip, :gender, :year_of_birth, :ethnicity, :education, length: {maximum: 255}, allow_blank: true
  validates :email, presence: true, if: :email_required?
  validates :email, uniqueness: {scope: [:account_id, :deleted_at]}, format: {with: EMAIL_REGEX_PATTERN}, allow_blank: true, if: :email_changed?

  validates :password, presence: true, confirmation: true, if: :password_required?
  validates :password, length: {within: 4..128}, allow_blank: true

  def self.authentication_keys
    [:account_id, :email]
  end

  def self.reset_password_keys
    [:account_id, :email]
  end

  def resize_params
    {size: '280x280>', sharpen: '0x0.5', quality: 85, thumbnail: true, auto_orient: true}
  end

  # Upload file to S3
  def upload_image!(file)
    if file.respond_to?(:original_filename)
      old_name = image
      self.image = ActiveSupport::Multibyte::Unicode.normalize(filename_with_timestamp(file.original_filename))

      if file_is_image?
        store_file(file)

        remove_remote_file(old_name) if save!
      else
        errors.add(:image, :invalid)
      end
    end
  rescue Exception => e
    Rails.logger.error "Upload failed for Customer##{id}: #{e.message.inspect}: \n#{e.backtrace[0..8].join("\n  ")}"
  end

  def remove_remote_file(file_name)
    delete_file!(file_name) if file_name.present?
  end

  def filename
    image
  end

  def folder
    'customers'
  end

  protected

  def password_required?
    !persisted? || !password.nil? || !password_confirmation.nil?
  end

  def email_required?
    true
  end

  private

  # Add timestamp to filename.
  def filename_with_timestamp(val)
    ext = File.extname(val)
    "#{File.basename(val, ext)}-#{Time.now.to_i}#{ext}".downcase
  end
end
