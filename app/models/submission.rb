class Submission < ActiveRecord::Base

  serialize :answers, JSON
  serialize :extra_questions, JSON
  serialize :location_data, JSON

  belongs_to :account
  belongs_to :state
  belongs_to :district
  belongs_to :customer
  belongs_to :comparision_account, class_name: 'Account'

  validates :account_id, presence: true
  validates :publisher, :remote_ip, :title, :full_name, :email, :zip, :facebook, :twitter,
    :gender, :year_of_birth, :ethnicity, :education, :phone, :external_ref, length: {maximum: 255}, allow_blank: true

  before_create :set_hash
  after_validation :set_location_data, on: :create

  def title
    read_attribute(:title) || (I18n.t('submissions.default_title', date: I18n.l(created_at.to_date, format: :long)) if created_at)
  end

  def db_title
    read_attribute(:title)
  end

  private

  def set_hash
    begin
      self[:client_hash] = SecureRandom.hex(32).upcase
    end while Submission.where(client_hash: self[:client_hash]).exists?
  end

  # Fill location information by GeoIp data.
  def set_location_data
    # if remote_ip.present?
    #   location = GeoipLocation.find_by_ip(remote_ip)

    #   if location.try(:found?)
    #     self.location_data = {
    #       country_code: location.country.iso_code,
    #       country_name: location.country.name,
    #       subdivision_code: location.subdivisions.first.try(:iso_code),
    #       subdivision_name: location.subdivisions.first.try(:name),
    #       city: location.city.name,
    #       latitude: location.location.latitude,
    #       longitude: location.location.longitude
    #     }.reject { |_, v| v.blank?}
    #   end
    # end
  rescue => e
    logger.error "#{e.message}\n#{e.backtrace[0..7].join("\n  ")}"
    true
  end
end
