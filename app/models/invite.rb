require 'securerandom'

class Invite < ActiveRecord::Base
  include SoftDeleteable

  belongs_to :account
  validates :token, :account_id, presence: true

  before_validation :generate_token
  def generate_token
    self.token ||= SecureRandom.uuid
  end
end
