class User < ActiveRecord::Base
  
  include SoftDeleteable
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable#, :validatable

  attr_accessor :password_reset_by_admin

  validates_presence_of :email
  validates_uniqueness_of :email, allow_blank: true, scope: [:deleted_at], if: :email_changed?
  validates_format_of :email, with: /\A[^@\s]+@([^@\s]+\.)+[^@\W]+\z/, allow_blank: true, if: :email_changed?

  validates_presence_of :password, if: :password_required?
  validates_confirmation_of :password, if: :password_required?
  validates_length_of :password, within: 4..72, allow_blank: true

  protected

  # Checks whether a password is needed or not. For validations only.
  # Passwords are always required if it's a new record, or if the password
  # or confirmation are being set somewhere.
  def password_required?
    !persisted? || !password.nil? || !password_confirmation.nil?
  end

  module ClassMethods
    Devise::Models.config(self, :email_regexp, :password_length)
  end
end
