class ExtraQuestion < ActiveRecord::Base

  include SoftDeleteable
  include Translateable

  serialize :options, JSON

  acts_as_list scope: [:account_id]

  belongs_to :account

  validates :account_id, presence: true
  validates :name, length: {maximum: 255}, presence: true

  scope :sorted, -> { order(position: :asc, id: :asc) }

  def name=(translated_names)
    set_translations_for(:name, translated_names, account.languages)
  end

  # Get option value by option key. Expected structure:
  #   {"1476275970372" => {"en" => "Yes"}}
  def option_value_for(option_id, language = nil)
    if options.is_a?(Hash) && options.key?(option_id.to_s)
      h = options[option_id.to_s]
      if h.is_a?(Hash)
        language ||= account.languages.default_language.first

        if h.key?(language.code)
          h[language.code]
        else
          h.first.try(:last)
        end
      end
    end
  end
end
