class Language < ActiveRecord::Base
  
  include SoftDeleteable
  
  belongs_to :account
  
  has_many :translations, dependent: :destroy
  has_many :pages, dependent: :destroy
  
  validates :name, :code, presence: true
  validates_uniqueness_of :code, scope: :account_id
  
  before_create :set_initial_language_default
  after_create :set_default_language
  after_update :set_default_language
  
  scope :default_language, -> { where(default_language: true) }
  
  private
  
  def set_initial_language_default
    self[:default_language] = true if account.languages.empty?
  end

  def set_default_language
    if self.default_language_changed? && default_language
      account.languages.where(default_language: true).where('id != ?', id).update_all(default_language: false)
    end
  end
end
