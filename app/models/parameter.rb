class Parameter < ActiveRecord::Base
  
  include SoftDeleteable
  include Translateable
  
  belongs_to :account
  
  validates :name, :account_id, presence: true
  
  def name=(translated_names)
    set_translations_for(:name, translated_names, account.languages)
  end
end
