module Translateable
  
  extend ActiveSupport::Concern
  
  included do
    has_many :translations, as: :model
  end

  def translation_for(field, language)
    default_value = self[field] if language.default_language?
    
    translations.detect { |t| t.language_id == language.id && t.model_field == field.to_s }.try(:content).presence || default_value
  end
  
  def translation_for_with_default(field, language, default_value = nil)
    translations.detect { |t| t.language_id == language.id && t.model_field == field.to_s }.try(:content).presence || default_value || self[field]
  end
  
  def set_translations_for(field, values, languages)
    values.each do |language_code, value|
      language = languages.find { |l| l.code == language_code }
      
      self[field] = value if language.default_language? || languages.size == 1
      
      translation = translations.find_or_initialize_by(account_id: account.id, model_field: field.to_s, language_id: language.id)
      translation.content = value
      translation.save if translation.persisted?
    end
  end
end
