# Module is being used by Candidate class and it contains methods to upload its images to S3.
module Uploadable

  extend ActiveSupport::Concern

  IMAGE_EXTENSIONS = %w(jpg jpeg png gif)

  def public_url(scheme = 'http')
    "#{scheme}:#{schemeless_public_url}"
  end

  def schemeless_public_url
    "//#{media_public_host}/#{public_path}"
  end

  def store_file(file)
    if bucket.present?
      if resizable?
        file = MiniMagick::Image.open(file.tempfile.path)
        file = resize!(file, resize_params)
      end
      content_type = file.respond_to?(:content_type) ? file.content_type : file.mime_type
      bucket.object(upload_path).upload_file(file.path, acl: 'public-read', cache_control: 'max-age:3600', content_type: sanitized_content_type(content_type))
    else
      Rails.logger.warn "Can't upload file to S3 environment. S3 is not configured for application!"
    end
  end

  def upload_path
    upload_key(folder, filename)
  end

  def upload_key(folder, filename)
    [('dev' unless Rails.env.production?), sprintf('%012d', account.id).scan(/..../), folder, sprintf('%012d', id).scan(/..../), filename].flatten.compact.join('/')
  end

  def public_path
    upload_key(folder, URI.escape(ActiveSupport::Multibyte::Unicode.normalize(filename), /[^#{URI::REGEXP::PATTERN::UNRESERVED}\/]/)) if filename.present?
  end

  def delete_file!(file_name)
    bucket.object(upload_key(folder, file_name)).delete
  rescue
    Rails.logger.warn "Can't delete remote file with name #{file_name}."
  end

  def resizable?
    resize_params.present?
  end

  def resize_params
    {}
  end

  def file_is_image?
    IMAGE_EXTENSIONS.include?(File.extname(filename).gsub('.', ''))
  end

  private

  def bucket_name
    Rails.application.config.app.aws.assets_bucket
  end

  def media_public_host
    Rails.application.config.app.aws.media_public_host.presence || bucket_name
  end

  def bucket
    @bucket ||= Aws::S3::Resource.new.bucket(bucket_name) if Rails.application.config.app.aws.enabled
  end

  def sanitized_content_type(content_type)
    if content_type.blank? || content_type == 'application/octet-stream'
      MIME::Types.type_for(filename.to_s).first.to_s.presence || content_type
    else
      content_type
    end
  end

  def resize!(file, opts = {})
    file.combine_options do |c|
      c.auto_orient if opts.fetch(:auto_orient, false)
      if opts.key?(:size)
        if opts.fetch(:thumbnail, false)
          c.thumbnail opts.fetch(:size)
        else
          c.resize opts.fetch(:size)
        end
      end
      c.quality opts.fetch(:quality) if opts.key?(:quality)
      c.sharpen opts.fetch(:sharpen) if opts.key?(:sharpen)
    end
    file
  end
end
