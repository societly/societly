class Question < ActiveRecord::Base
  
  include Translateable
  include SoftDeleteable
  
  serialize :params, JSON

  acts_as_list scope: [:account_id]

  belongs_to :account
  has_many :candidate_question_answers, dependent: :destroy
  
  validates :name, :account_id, presence: true
  
  scope :sorted, -> { order(position: :asc, id: :asc) }

  def name=(translated_names)
    set_translations_for(:name, translated_names, account.languages)
  end
  
  def description=(translated_descriptions)
    set_translations_for(:description, translated_descriptions, account.languages)
  end
  
  def param_value_for(param_id)
    if params.respond_to?(:key) && params.respond_to?(:fetch)
      if params.key?(param_id.to_s)
        params.fetch(param_id.to_s).to_i
      else
        0
      end
    else
      0
    end
  end
end
