class State < ActiveRecord::Base

  include SoftDeleteable
  include Translateable

  belongs_to :account
  has_many :districts, dependent: :destroy

  validates :account_id, presence: true
  validates :name, length: {maximum: 255}, presence: true
  validates :code, presence: true, uniqueness: {scope: [:account_id, :deleted_at]}

  scope :sorted, -> { order(:name) }

  def name=(translated_names)
    set_translations_for(:name, translated_names, account.languages)
  end
end
