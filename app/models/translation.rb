class Translation < ActiveRecord::Base
  
  include SoftDeleteable
  
  belongs_to :account
  belongs_to :model, polymorphic: true
  
end
